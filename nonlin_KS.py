# -*- coding: utf-8 -*-
"""
Solving Kuramoto-Sivashinsky equation: u_t = (u^2)_x - v * u_xxxx - u_xx + f
Created on Sun Mar 12 22:41:20 2017

@author: Gengiro
"""

import numpy as np
import scipy.integrate as sp
import matplotlib.pyplot as plt
from rfft_norm import rfft_norm, irfft_norm


def nonlin_KS_ode(a, t, method, N, v):
    da = np.zeros(a.size)

    if method == 1:
        M = a.size
        for k in range(M):
            s = 0.
            for p in range(k - M + 1, M):
                if (p < 0):
                    a_p = - a[-p]
                else:
                    a_p = a[p]
                if ((k - p) < 0):
                    a_kp = - a[p - k]
                else:
                    a_kp = a[k - p]
                s += a_p * a_kp
            da[k] = (k ** 2 - v * (k ** 4)) * a[k] - k * s

    elif method == 2:
        u = irfft_norm(a * 1j, N)
        u2 = np.square(u)
        a2 = np.real(rfft_norm(u2, N))
        for k in range(a.size):
            da[k] = (k ** 2 - v * (k ** 4)) * a[k] + k * a2[k]

    else:
        assert(0)

    return da


def CL_nonlin_KS_ode(a, t, method, N, v, BK, a_eq, r):
    if (r is False) or (np.linalg.norm(a - a_eq) < r):
        da = nonlin_KS_ode(a, t, method, N, v) - np.dot(BK, a - a_eq)
    else:
        da = nonlin_KS_ode(a, t, method, N, v)
    return da


def sim_nonlin_KS(x, t, u0, param):
    atol = 1e-12
    N = param['N']
    method = param['method']
    v = param['v']
    x_step = x[1] - x[0]
    u = np.zeros((len(t), N))
    u[0, :] = u0

    # Find the amplitudes of the Fourier decomposition
    a0 = np.imag(rfft_norm(u[0, :], N))
    a0[0] = 0
    freq = np.fft.rfftfreq(N, x_step)

    a_t_k = np.zeros((t.size, freq.size))
    a_t_k = sp.odeint(nonlin_KS_ode, a0, t, args=(method, N, v), atol=atol)

    assert(a_t_k.shape == (t.size, freq.size))

    for i in range(1, t.size):
        u[i, :] = irfft_norm(a_t_k[i, :] * 1j, N)

    return a_t_k, u


def sim_CL_nonlin_KS(x, t, u0, param, BK, a_eq, r=False):
    atol = 1e-12
    N = param['N']
    method = param['method']
    v = param['v']
    x_step = x[1] - x[0]
    u = np.zeros((len(t), N))
    u[0, :] = u0

    # Find the amplitudes of the Fourier decomposition
    a0 = np.imag(rfft_norm(u[0, :], N))
    a0[0] = 0
    freq = np.fft.rfftfreq(N, x_step)

    a_t_k = np.zeros((t.size, freq.size))
    a_t_k, info = sp.odeint(CL_nonlin_KS_ode, a0, t,
                            args=(method, N, v, BK, a_eq, r), atol=atol,
                            full_output=1)
    print('  Attention: ', info['message'])
    assert(a_t_k.shape == (t.size, freq.size))

    for i in range(1, t.size):
        u[i, :] = irfft_norm(a_t_k[i, :] * 1j, N)
    return a_t_k, u


def plot_sol_KS(u, x, t):
    font_size = 13
    r = (x.size // 2) + 1
    x_r = x[:r]
    u_r = u[:, :r]

    plt.figure(figsize=(15, 5))
    X, T = np.meshgrid(x_r, t)
    plt.contourf(T, X, u_r, cmap='jet')
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=font_size)
    plt.xlabel("time [s]", fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.ylabel("x [rad]", fontsize=font_size)
    plt.yticks(np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.]),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$'], fontsize=font_size)
    # plt.title("u(x, t)")
    plt.tight_layout()
    # plt.savefig('figure1.png', dpi=600, transparent=True)
    plt.show()


def plot_coeff_KS(a_t_k, x, t):
    font_size = 13
    cf = (1, 3)

    """plt.figure(figsize=(15, 4))
    m = a_t_k[1, :].size
    dt = t[1] - t[0]
    T = t[-1] - t[0]
    PSD_k = np.zeros(m)
    for k in range(m):
        PSD_k[k] = np.sum((a_t_k[:, k] * dt) ** 2) / T
    # plt.plot(np.log10(PSD_k))
    plt.semilogy(PSD_k)
    plt.ylabel(r'$log_{10}(PSD(a_k))$', fontsize=font_size)
    plt.xlabel("Fourier coefficient", fontsize=font_size)
    plt.show()"""

    plt.figure(figsize=(13, 4))
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        plt.plot(a_t_k[:, odd_cf], a_t_k[:, even_cf])

        y_str = r'$a_{' + str(even_cf) + '}$'
        x_str = r'$a_{' + str(odd_cf) + '}$'
        plt.ylabel(y_str, fontsize=font_size)
        plt.xlabel(x_str, fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.grid()

    plt.tight_layout()
    # plt.savefig('figure3.png', dpi=600, transparent=True)
    plt.show()
