# -*- coding: utf-8 -*-
"""
Script to solve/simulate the open loop system originated by the
Kuramoto-Sivashinsky equation
Created on Mon Apr  3 08:35:24 2017

@author: Jorge Vidal-Ribas
University of Southampton
"""
import numpy as np
from nonlin_KS import sim_nonlin_KS, plot_sol_KS, plot_coeff_KS
from simulations import create_simulation, load_simulation
from rfft_norm import a2u
from time import time

v = (2. * np.pi / 39.) ** 2.
sim_T = 30
dt = 0.01
seed = 1
ovrwrt = False
rand_u0 = False


N_lst = [64, 96, 128]
N_lst = [64, 96]
N_lst = [96]
method_lst = [1, 2]
method_lst = [2]
t = np.arange(0, sim_T, dt)

for N_idx in range(len(N_lst)):
    for method_idx in range(len(method_lst)):
        N = N_lst[N_idx]
        method = method_lst[method_idx]
        param = {'v': v, 'N': N, 'method': method}

        x = np.linspace(0, 2 * np.pi, num=N, endpoint=False)
        if rand_u0:
            u0 = np.zeros(x.size)
            np.random.seed(seed)
            u0[1:(x.size-1)] = (np.random.random_sample(x.size-2) -
                                0.5) * 1e-06
        else:
            sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'
            a_sim, t_sim = load_simulation(sim_file)
            u_sim = a2u(a_sim, N)
            u0 = u_sim[-1, :]

        print('Running nonlinear simulation ...')
        t_0 = time()
        a, u = sim_nonlin_KS(x, t, u0, param)
        t_end = time()
        plot_sol_KS(u, x, t)
        plot_coeff_KS(a, x, t)
        print('Elapsed time: %.1fs' % (t_end - t_0))
        print()
        sim_filename = ('sim_N' + str(N) + '_method' + str(method) +
                        '_nonlin_OL_no_rand_T' + str(sim_T))
#        create_simulation(sim_filename, a, dt, overwrite=True)
