# -*- coding: utf-8 -*-
"""
Python script to execute all the plots used in my dissertation
Stabilisation of the Kuramoto-Sivashinsky Equation
Created on Wed Aug 30 16:04:45 2017

@author: Jorge Vidal-Ribas
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from equilibria_KS import (load_equilibria, equilibria_selection, plot_circle,
                           plot_arrow)
from simulations import load_simulation, interpolate_sim, cm2inch, compare_sim
from actuators_KS import load_actuators
from stabilisation_KS import (load_stabilisation, edge_detection,
                              min_distance)
from rfft_norm import a2u, interpolate
from lqr import lqr_matrices_KS, sim_actuators
from nonlin_KS import sim_CL_nonlin_KS
from force_KS import B_KS
from lin_KS import lin_KS, eig_lin_KS, real_eig, disturbance

cf = (1, 3)
ax_k = 0.05
font_size = 6
lw = 0.8
ms = 2.3
alpha = 0.8
dpi = 600
transp = False

width = 14
pad = 0.2
w_pad = 0.5
h_pad = 1.0

# Colours
dark_blue = '#00009bff'
blue = '#0062ffff'
light_blue = '#00d4ffff'
yellow = '#f0ff0eff'
red = '#f00000ff'
orange = '#ff5400ff'
green = '#66ff66'
magenta = '#9b009b'
grey = '#A9A9A9'
white = 'w'

col2 = [dark_blue, red]
col3 = [magenta, orange, dark_blue]
col7 = [white, dark_blue, blue, light_blue, yellow, orange, red]
col6 = col7[1:]

"""colours = [
           '#00009bff',  # dark blue
           # '#0062ffff',  # blue
           '#00d4ffff',  # light blue
           # '#f0ff0eff',  # yellow
           # '#f00000ff',  # red
           '#ff9900',  # orange
           '#66ff66',  # green
           ]

n_pos_c = ['w',
           '#00009bff',
           '#0062ffff',
           '#00d4ffff',
           # '#54ffa9ff',
           '#f0ff0eff',
           '#ff5400ff',
           '#f00000ff']
"""

plt.rcParams.update({'font.size': font_size})
plt.rcParams.update({'legend.fancybox': False})
plt.rcParams.update({'legend.edgecolor': 'k'})  # '0.8'
plt.rcParams.update({'legend.frameon': False})
plt.rcParams.update({'lines.markeredgewidth': 0.5})  # 1.0
plt.rcParams.update({'lines.linewidth': 1.0})  # 1.5

# %% SOLVING KSE
# Simulations done with the script exe_sim_nonlin_KS.py
# Load simulations
a_N64_m1, t_N64_m1 = load_simulation('sim_N64_method1_nonlin_OL')
a_N96_m1, t_N96_m1 = load_simulation('sim_N96_method1_nonlin_OL')
a_N128_m1, t_N128_m1 = load_simulation('sim_N128_method1_nonlin_OL')

a_m1 = [a_N64_m1, a_N96_m1, a_N128_m1]
t_m1 = [t_N64_m1, t_N96_m1, t_N128_m1]

a_N64_m2, t_N64_m2 = load_simulation('sim_N64_method2_nonlin_OL')
a_N96_m2, t_N96_m2 = load_simulation('sim_N96_method2_nonlin_OL')
a_N128_m2, t_N128_m2 = load_simulation('sim_N128_method2_nonlin_OL')

a_m2 = [a_N64_m2, a_N96_m2, a_N128_m2]
t_m2 = [t_N64_m2, t_N96_m2, t_N128_m2]

a_diff_m1, t_diff_m1 = load_simulation('sim_N96_method1_nonlin_OL_no_rand_T30')
a_diff_m2, t_diff_m2 = load_simulation('sim_N96_method2_nonlin_OL_no_rand_T30')

N_lst = [64, 96, 128]
method_lst = [1, 2, 3]
method_lst = [1, 2]
# method_lst = [3]

# Open loop simulations plots
dt = 0.01
N_intp = 512
t_range = [13, 27]
if method_lst == [3]:
    t_range = [0, 12]
    a_m1 = [a_diff_m1]
    t_m1 = [t_diff_m1]
    a_m2 = [a_diff_m2]
    t_m2 = [t_diff_m2]
    N_lst = [96]

t_intp = np.arange(t_range[0], t_range[1] + dt, dt)
a_m1_intp = []
u_m1_intp = []
for idx in range(len(a_m1)):
    x_item = np.linspace(0, 2 * np.pi, N_lst[idx], endpoint=False)
    x_intp = np.linspace(0, 2 * np.pi, N_intp, endpoint=False)
    a_item = a_m1[idx]
    t_item = t_m1[idx]
    u_item = a2u(a_item, N_lst[idx])
    a_item_intp, u_item_intp = interpolate_sim(a_item, u_item, x_item,
                                               t_item, new_x=x_intp,
                                               new_t=t_intp)
    a_m1_intp.append(a_item_intp)
    u_m1_intp.append(u_item_intp)

a_m2_intp = []
u_m2_intp = []
for idx in range(len(a_m2)):
    x_item = np.linspace(0, 2 * np.pi, N_lst[idx], endpoint=False)
    x_intp = np.linspace(0, 2 * np.pi, N_intp, endpoint=False)
    a_item = a_m2[idx]
    t_item = t_m2[idx]
    u_item = a2u(a_item, N_lst[idx])
    a_item_intp, u_item_intp = interpolate_sim(a_item, u_item, x_item,
                                               t_item, new_x=x_intp,
                                               new_t=t_intp)
    a_m2_intp.append(a_item_intp)
    u_m2_intp.append(u_item_intp)

for N_idx in range(len(N_lst)):
    N = N_lst[N_idx]
    for method in method_lst:
        if method == 1:
            a_intp = a_m1_intp[N_idx]
            u_intp = u_m1_intp[N_idx]
        elif method == 2:
            a_intp = a_m2_intp[N_idx]
            u_intp = u_m2_intp[N_idx]
        elif method == 3:
            a_intp = a_m2_intp[N_idx] - a_m1_intp[N_idx]
            u_intp = u_m2_intp[N_idx] - u_m1_intp[N_idx]
            method = '_diff'
        fig_name = '1_sim_N' + str(N) + '_method' + str(method) + '_nonlin_OL'
        print('N = %d, method: %s' % (N, str(method)))

        # Spatio-temporal solution
        plt.figure(figsize=cm2inch(width, 6))
        r = (x_intp.size // 2) + 1
        x_r = x_intp[:r]
        u_r = u_intp[:, :r]
        t_r = t_intp - t_intp[0]

        X, T = np.meshgrid(x_r, t_r)
        c_map = 'jet'
        # c_map = 'Blues'
        # c_map = 'bone'
        # c_map = 'gray'
        plt.contourf(T, X, u_r, cmap=c_map)
        cb = plt.colorbar()
        plt.xlabel(r'$time$')
        plt.ylabel(r'$x$')
        plt.yticks(np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.]),
                   ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                    r'$5\pi/6$', r'$\pi$'])
        plt.tight_layout()
        # plt.subplots_adjust(left=0.08, right=1.05, top=0.95, bottom=0.17)

        plt.savefig('Dissertation/' + fig_name + '_u', dpi=dpi,
                    transparent=transp, bbox_inches='tight', pad_inches=0.1)
        plt.show()

        plt.figure(figsize=cm2inch(width, 4))
        for k in range(cf[0] * cf[1]):
            plt.subplot(cf[0], cf[1], k + 1)
            odd_k = 2 * k + 1
            even_k = 2 * k + 2

            y_str = r'$a_{' + str(even_k) + '}$'
            x_str = r'$a_{' + str(odd_k) + '}$'
            plt.ylabel(y_str)
            plt.xlabel(x_str)
            plt.axis('equal')
            plt.plot(a_intp[:, odd_k], a_intp[:, even_k], c=dark_blue,
                     linewidth=lw, alpha=alpha)

        plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
        for p in range(cf[0] * cf[1]):
            plt.subplot(cf[0], cf[1], p + 1)
            # plt.grid()
            # plt.axis('equal')
            ax = np.array(plt.axis())
            ax[0] -= ax_k * (ax[1] - ax[0])
            ax[1] += ax_k * (ax[1] - ax[0])
            ax[2] -= ax_k * (ax[3] - ax[2])
            ax[3] += ax_k * (ax[3] - ax[2])
            plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
            plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
            plt.axis(tuple(ax))

        plt.savefig('Dissertation/' + fig_name + '_a', dpi=dpi,
                    transparent=transp, bbox_inches='tight', pad_inches=0.1)
        plt.show()

# %% Fourier modes truncation plot
method_lst = [1, 2]

for method in method_lst:
    fig_name = '2_sim_method' + str(method) + '_nonlin_OL_spectrum'
    if method == 1:
        a_lst = a_m1
    elif method == 2:
        a_lst = a_m2

    PSD = []
    for a in a_lst:
        PSD_k = []
        for k in range(len(a[0, :])):
            PSD_k.append(np.square(a[:, k] * dt).sum() /
                         (t_intp[-1] - t_intp[0]))
        PSD.append(PSD_k)

    plt.figure(figsize=cm2inch(width, 6))
    for idx in range(len(N_lst) - 1, -1, -1):
        plt.semilogy(PSD[idx], color=col3[idx],
                     label='M = '+str(N_lst[idx] // 2), linewidth=lw,
                     alpha=alpha)

    plt.xlabel(r'Fourier mode, $k$')
    plt.ylabel(r'$\overline{a_k^2}$')
    plt.legend(loc='upper right')
    plt.tight_layout()
    # plt.subplots_adjust(left=0.08, right=1.10, top=0.95, bottom=0.17)

    plt.savefig('Dissertation/' + fig_name, dpi=dpi, transparent=transp,
                bbox_inches='tight', pad_inches=0.1)
    plt.show()

# %% EQUILIBRIA SEARCH
N = 96
tol = 1e-10

# Equilibria search files
eq_file = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
a0_file = eq_file + '_a0'
a_file = eq_file + '_sim'

fig_name = '3_' + eq_file + '_search'

roots = load_equilibria(eq_file)
a0 = load_equilibria(a0_file)
a, t = load_simulation(a_file)

# Coefficients
ax_k = 0.05

fig = plt.figure(figsize=cm2inch(width, 4))
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    odd_cf = 2 * p + 1
    even_cf = 2 * p + 2
    y_str = r'$a_{' + str(even_cf) + '}$'
    x_str = r'$a_{' + str(odd_cf) + '}$'
    plt.ylabel(y_str)
    plt.xlabel(x_str)
    plt.axis('equal')
    plt.plot(a[:, odd_cf], a[:, even_cf], color=grey, alpha=alpha,
             linewidth=lw/2)
    for m in range(len(a0)):
        a_ini = a0[m]
        plt.plot(a_ini[odd_cf], a_ini[even_cf], '.', color=blue,
                 alpha=alpha, markersize=ms/3)
    for eq in range(len(roots)):
        a_eq = roots[eq]
        plt.plot(a_eq[odd_cf], a_eq[even_cf], 'x', linewidth=lw, alpha=alpha,
                 color=red, markersize=ms*1.2, markeredgewidth=0.7)

artist = []
label = []
artist.append(plt.Line2D((.5, .5), (0, 0), color=grey, marker='',
                         linestyle='-', linewidth=lw))
label.append('simulation')
artist.append(plt.Line2D((.5, .5), (0, 0), color=blue, marker='.',
                         linestyle='', markersize=ms*1.2))
label.append('search start')
artist.append(plt.Line2D((.5, .5), (0, 0), color=red, marker='x',
                         linestyle='', markersize=ms*1.2))
label.append('equilibria')
fig.legend(artist, label, loc='upper center', ncol=3,
           bbox_to_anchor=(0.5, 1.15))

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    # plt.grid()
    # plt.axis('equal')
    ax = np.array(plt.axis())
    ax[0] -= ax_k * (ax[1] - ax[0])
    ax[1] += ax_k * (ax[1] - ax[0])
    ax[2] -= ax_k * (ax[3] - ax[2])
    ax[3] += ax_k * (ax[3] - ax[2])
    plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
    plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
    plt.axis(tuple(ax))

plt.savefig('Dissertation/' + fig_name + '_a', dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# %% Steady-States
N_intp = 512
N = 96
only = [3, 4, 16, 17, 18, 21, 22]
# 'single', 'red_paired', 'paired', None, [3, 4, 16, 17, 18, 21, 22]
fig_name = '4_' + eq_file + '_'

eq_filt, alpha_lst, add_save = equilibria_selection(roots, only=only)
x = np.linspace(0, 2 * np.pi, N, endpoint=False)
x_interp = np.linspace(0, np.pi, N_intp)

plt.figure(figsize=cm2inch(width, 6))
for eq in eq_filt:
    u = a2u(roots[eq], N)
    u_interp = interpolate(x_interp, x, u)
    plt.plot(x_interp, u_interp, label='eq ' + str(eq), linewidth=lw)

plt.ylabel('u')
plt.xlabel('x')
plt.xticks(np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.]),
           ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
            r'$5\pi/6$', r'$\pi$'])
plt.xlim([0, np.pi])
plt.legend(loc="upper right", bbox_to_anchor=(1.15, 1), ncol=1)
plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
# plt.grid()

plt.savefig('Dissertation/' + fig_name + add_save, dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# %% LINEARISATION
v = (2. * np.pi / 39.) ** 2.

only = 'paired'

# 'single', 'red_paired', 'paired', None, [3, 4, 16, 17, 18, 21, 22]
a_file = 'sim_N96_method1_nonlin_OL_no_rand_T30'
fig_name = '5_' + eq_file + '_'

text = False
dtxt = 0.05
ax_k = 0.10

# Equilibria
eq_filt, alpha_lst, add_save = equilibria_selection(roots, only=only)
a, t = load_simulation(a_file)

fig = plt.figure(figsize=cm2inch(width, 4))
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    odd_cf = 2 * p + 1
    even_cf = 2 * p + 2
    y_str = r'$a_{' + str(even_cf) + '}$'
    x_str = r'$a_{' + str(odd_cf) + '}$'
    plt.ylabel(y_str)
    plt.xlabel(x_str)
    plt.axis('equal')
    plt.plot(a[:, odd_cf], a[:, even_cf], c=grey, alpha=alpha,
             linewidth=lw/2, zorder=0)
for eq in eq_filt:
    a_eq = roots[eq]
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        plt.plot(a_eq[odd_cf], a_eq[even_cf], 'x', c=red, ms=ms)
        if text:
            if a_eq[odd_cf] >= 0:
                h_a = 'left'
                dx = dtxt
            else:
                h_a = 'right'
                dx = -dtxt
            if a_eq[even_cf] >= 0:
                v_a = 'bottom'
                dy = dtxt
            else:
                v_a = 'top'
                dy = -dtxt
            plt.text(a_eq[odd_cf] + dx, a_eq[even_cf] + dy, str(eq),
                     ha=h_a, va=v_a)

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
ax_lst = []
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    # plt.grid()
    # plt.axis('equal')
    ax = np.array(plt.axis())
    ax[0] -= ax_k * (ax[1] - ax[0])
    ax[1] += ax_k * (ax[1] - ax[0])
    ax[2] -= ax_k * (ax[3] - ax[2])
    ax[3] += ax_k * (ax[3] - ax[2])
    plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
    plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
    # ax = [-max_ax, max_ax, -max_ax, max_ax]
    ax_lst.append(ax)
    plt.axis(tuple(ax))

plt.savefig('Dissertation/' + fig_name + add_save + '_equilibria',
            dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# Equilibria eigenvalues
text = False
r_k = 4e-1
#    max_ax = 0

fig = plt.figure(figsize=cm2inch(width, 4))
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    odd_cf = 2 * p + 1
    even_cf = 2 * p + 2
    y_str = r'$a_{' + str(even_cf) + '}$'
    x_str = r'$a_{' + str(odd_cf) + '}$'
    plt.ylabel(y_str)
    plt.xlabel(x_str)
    plt.axis('equal')
    plt.plot(a[:, odd_cf], a[:, even_cf], color=grey, alpha=alpha,
             linewidth=lw/2, zorder=0)
for eq in eq_filt:
    a_eq = roots[eq]
    A = lin_KS(a_eq, v)
    eig, pos_eig = eig_lin_KS(A, plot=False)
    n_pos = pos_eig['n_pos']
    sum_pos = np.real(pos_eig['val']).sum()
    r = r_k * sum_pos ** 2
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        plt.scatter(a_eq[odd_cf], a_eq[even_cf], s=r, c=col7[n_pos],
                    label='P = ' + str(n_pos), edgecolors='none')
        # plt.plot(a_eq[odd_cf], a_eq[even_cf], '.r')
        if text:
            if a_eq[odd_cf] >= 0:
                h_a = 'left'
                dx = dtxt
            else:
                h_a = 'right'
                dx = -dtxt
            if a_eq[even_cf] >= 0:
                v_a = 'bottom'
                dy = dtxt
            else:
                v_a = 'top'
                dy = -dtxt
            plt.text(a_eq[odd_cf] + dx, a_eq[even_cf] + dy, str(eq),
                     ha=h_a, va=v_a)

for eq in alpha_lst:
    a_eq = roots[eq]
    A = lin_KS(a_eq, v)
    eig, pos_eig = eig_lin_KS(A, plot=False)
    n_pos = pos_eig['n_pos']
    sum_pos = np.real(pos_eig['val']).sum()
    r = r_k * sum_pos ** 2
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        plt.scatter(a_eq[odd_cf], a_eq[even_cf], s=r, c=col7[n_pos],
                    label='P = ' + str(n_pos), alpha=alpha/2,
                    edgecolors='none')

artist = []
label = []
for c in range(1, len(col7)):
    artist.append(plt.Line2D((.5, .5), (0, 0), color=col7[c],
                             marker='o', linestyle='',
                             markersize=ms*2))
    label.append('P = ' + str(c))
fig.legend(artist, label, loc='upper center', ncol=len(col7[1:]),
           fontsize=font_size, bbox_to_anchor=(0.5, 1.15))

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
ax_lst = []
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    # plt.grid()
    # plt.axis('equal')
    ax = np.array(plt.axis())
    ax[0] -= ax_k * (ax[1] - ax[0])
    ax[1] += ax_k * (ax[1] - ax[0])
    ax[2] -= ax_k * (ax[3] - ax[2])
    ax[3] += ax_k * (ax[3] - ax[2])
    plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
    plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
    # ax = [-max_ax, max_ax, -max_ax, max_ax]
    ax_lst.append(ax)
    plt.axis(tuple(ax))

plt.savefig('Dissertation/' + fig_name + add_save + '_eigenvalues',
            dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# Equilibria eigenvectors
radius = .35
text = False

fig = plt.figure(figsize=cm2inch(width, 4))
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    odd_cf = 2 * p + 1
    even_cf = 2 * p + 2
    y_str = r'$a_{' + str(even_cf) + '}$'
    x_str = r'$a_{' + str(odd_cf) + '}$'
    plt.ylabel(y_str)
    plt.xlabel(x_str)
    plt.axis('equal')
    plt.plot(a[:, odd_cf], a[:, even_cf], color=grey, alpha=alpha,
             linewidth=lw/2, zorder=0)
for eq in eq_filt:
    a_eq = roots[eq]
    A = lin_KS(a_eq, v)
    eig, pos_eig = eig_lin_KS(A, plot=False)
    r_val, r_vec = real_eig(pos_eig['val'], pos_eig['vec'])
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        plot_circle(a_eq[odd_cf], a_eq[even_cf], radius,
                    color=dark_blue, linewidth=lw/2)
        for m in range(len(r_vec)):
            plot_arrow(a_eq[odd_cf], a_eq[even_cf], r_vec[m][odd_cf],
                       r_vec[m][even_cf], radius, color=dark_blue)
        if text:
            if a_eq[odd_cf] >= 0:
                h_a = 'left'
                dx = dtxt
            else:
                h_a = 'right'
                dx = -dtxt
            if a_eq[even_cf] >= 0:
                v_a = 'bottom'
                dy = dtxt
            else:
                v_a = 'top'
                dy = -dtxt
            plt.text(a_eq[odd_cf] + dx, a_eq[even_cf] + dy, str(eq),
                     ha=h_a, va=v_a)

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    # plt.grid()
    # plt.axis('equal')
    ax = np.array(plt.axis())
    ax[0] -= ax_k * (ax[1] - ax[0])
    ax[1] += ax_k * (ax[1] - ax[0])
    ax[2] -= ax_k * (ax[3] - ax[2])
    ax[3] += ax_k * (ax[3] - ax[2])
    ax = ax_lst[p]
    plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
    plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
#    max_ax = 4
#    ax = [-max_ax, max_ax, -max_ax, max_ax]
    plt.axis(tuple(ax))

plt.savefig('Dissertation/' + fig_name + add_save + '_eigenvectors',
            dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# %% Eigenvalues and eigenvectors of the selection
eq_filt = [3, 16, 18, 21]
fig_name = '6_' + eq_file + '_eigen_eq'
log = False

if log:
    ylim = [1e-10, 4]
    fun = plt.semilogy
    base = ylim[0]
    label = [r'$|$Re $(v_i)|$', r'$|$Imag $(v_i)|$']
    y_eq = 5e-1
    leg_loc = 'upper right'
    add_log = '_log'
else:
    ylim = [-1, 1]
    fun = plt.plot
    base = 0
    label = [r'Re $(v_i)$', r'Imag $(v_i)$']
    y_eq = ylim[1] - 0.15
    leg_loc = 'lower right'
    add_log = ''

for eq in eq_filt:
    a_eq = roots[eq]
    A = lin_KS(a_eq, v)
    eig, pos_eig = eig_lin_KS(A, plot=False)
    n_pos = pos_eig['n_pos']

    fig = plt.figure(figsize=cm2inch(width, n_pos * 4))
    for n in range(n_pos):
        val = pos_eig['val'][n]
        real_val = np.real(val)
        imag_val = np.imag(val)
        vec = pos_eig['vec'][n]
        real_vec = np.real(vec)
        imag_vec = np.imag(vec)
        modes = np.arange(1, len(real_vec))
        xticks = np.arange(1, len(real_vec), 2)

        if log:
            real_vec = np.abs(real_vec)
            imag_vec = np.abs(imag_vec)

        plt.subplot(n_pos, 1, n + 1)
        if not log:
            plt.plot(modes, modes * base, 'k', linewidth=lw/1.5, alpha=alpha)
        for idx in xticks:
            fun([idx, idx], ylim, 'k', linewidth=lw/2, alpha=0.2)
        for idx in modes:
            fun([idx, idx], [base, imag_vec[idx]], linewidth=lw/1.5,
                color=red)
        fun(modes, imag_vec[1:], 'o', color=red, markersize=ms, label=label[1])

        for idx in modes:
            fun([idx, idx], [base, real_vec[idx]], linewidth=lw/1.5,
                color=dark_blue)
        fun(modes, real_vec[1:], 'o', color=dark_blue, markersize=ms,
            label=label[0])

        plt.xlim([0, len(real_vec)])
        plt.ylim(ylim)
        plt.xticks(xticks)
        plt.text(len(real_vec) - 1, y_eq,
                 r'$\lambda = %.2f + %.2f j$'
                 % (real_val, imag_val), va='top', ha='right')
        plt.ylabel(r'$v_i$')
        if n + 1 < n_pos:
            plt.xticks(xticks, [''] * len(xticks))
        else:
            plt.xlabel(r'Fourier mode, $i$')
            if log:
                plt.legend(loc=leg_loc, bbox_to_anchor=(1, 0.85))
            else:
                plt.legend(loc=leg_loc)

    plt.savefig('Dissertation/' + fig_name + str(eq) + add_log,
                dpi=dpi, transparent=transp,
                bbox_inches='tight', pad_inches=0.1)

    plt.show()

# %% STABILITY
# Actuators position
N = 96
tol = 1e-10
eq = 3
n_act = 1
search = 'unique'
ctrl_method = 'lqr_test'
fig_name = '7_actuators_example'
act_lst = [1, 2, 5]
# act_lst = [1]

dy = 0.04
N_intp = 512

xticks = np.pi / 6. * np.array(range(13))

x_intp = np.linspace(0, 2 * np.pi, N_intp, endpoint=True)

plt.figure(figsize=cm2inch(width, 4 * len(act_lst)))
for n_idx in range(len(act_lst)):
    n_act = act_lst[n_idx]
    act_file = ('actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol))) +
                '_' + search + '_' + ctrl_method + '_act' + str(n_act))
    actuators = load_actuators(act_file)
    act = actuators[eq]
    Mu = act['Mu']
    Sigma = act['Sigma']
    B, F, f = B_KS(x_intp, Mu, Sigma, N_intp)

    plt.subplot(len(act_lst), 1, n_idx + 1)
    n_act = len(F[0, :])
    plt.plot([x_intp[0], x_intp[-1]], [0, 0], 'k', linewidth=lw/2)
    for n in range(n_act):
        mu = Mu[n]
        mu_alt = 2 * np.pi - mu
        plt.plot([mu, mu], [-dy, 1], '--', c=col6[n], linewidth=lw/2)
        plt.plot([mu_alt, mu_alt], [-1, dy], '--', c=col6[n],
                 linewidth=lw/2)
        plt.plot(x_intp, F[:, n], c=col6[n], label='act ' + str(n + 1),
                 linewidth=lw)
        if n_act <= 1:
            str1 = r'$\mu$'
            str2 = r'$2\pi-\mu$'
        else:
            str1 = r'$\mu_{' + str(n + 1) + '}$'
            str2 = r'$2\pi-\mu_{' + str(n + 1) + '}$'
        plt.text(mu, -dy, str1, ha='center', va='top')
        plt.text(mu_alt, dy, str2, ha='center', va='bottom')

    plt.axis([0, 2 * np.pi, -1.1, 1.1])
    plt.ylabel(r'$f_{act}$')
    if n + 1 >= len(act_lst):
        plt.xticks(xticks,
                   ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                    r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$',
                    r'$3\pi/2$', r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'])
        plt.xlabel(r'$x$')
    else:
        plt.xticks(xticks, [''] * len(xticks))
    # plt.yticks([])
    if n_act > 1:
        plt.legend(loc='lower left', ncol=3)
        # plt.legend(loc="upper right", bbox_to_anchor=(1.15, 1), ncol=1)
    plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
    # plt.grid()

plt.savefig('Dissertation/' + fig_name + '_' + str(act_lst).replace(' ', ''),
            dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# %% Stability of the closed-loop system
# Eigenvalues of the closed-loop system
v = (2. * np.pi / 39.) ** 2.
N = 96
method = 2
tol = 1e-10
rho = 1e-1
search = 'unique'
ctrl_method = 'lqr_test'

eq_filt = [3, 16, 18, 21]
act_lst = [1, 2, 5]
loop = ['Open loop system', 'Closed-loop system']
matrix = ['Stability', 'Energy growth rate']

x = np.linspace(0, 2 * np.pi, N, endpoint=False)
param = {'v': v, 'N': N, 'method': method}
eq_file = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(eq_file)

for n_act in act_lst:
    act_file = ('actuators_N' + str(N) +
                '_tol' + str(-int(np.log10(tol))) +
                '_' + search + '_' + ctrl_method + '_act' + str(n_act))
    actuators = load_actuators(act_file)
    for eq in eq_filt:
        print('Equilibrium number %d' % (eq))
        print('Number of actuators: %d' % (n_act))
        a_eq = roots[eq]
        Mu = actuators[eq]['Mu']
        Sigma = actuators[eq]['Sigma']
        Q = np.diagflat(np.ones_like(a_eq))
        R = rho * np.diagflat(np.ones(len(Mu)))
        A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R)
        BK = np.dot(B, K)
        for l in loop:
            print(' ' + l)
            if l == loop[0]:
                H = A
            elif l == loop[1]:
                H = A - BK
            for m in matrix:
                print('  ' + m)
                if m == matrix[0]:
                    G = H
                elif m == matrix[1]:
                    G = H + H.T

                eig, pos_eig = eig_lin_KS(G, plot=False)
                pos_val = pos_eig['val']
                n_val = pos_eig['n_pos']

                if n_val == 0:
                    print('   No eigenvalues with positive real part')
                    val = eig['val'][-2]
                    print('   Max eigenvalue = (%.2f, %.2f)'
                          % (np.real(val), np.imag(val)))
                else:
                    for idx in range(n_val):
                        val = pos_val[idx]
                        print('   Eigenvalue %d = (%.2f, %.2f)'
                              % (idx + 1, np.real(val), np.imag(val)))
                print()
            print()

# %% Basin of attraction
# Depending on: Equilibria, Sim Time, Method or Num of actuators
N = 96
eq_lst = [3, 16, 21]
T_lst = [2, 8, 24]
method_lst = ['naive', 'search', 'E_dot']
titles = ['Naive', 'Search', r'Max $\dot{E}$ direction']
fig_name = '8_stabilisation_range_N' + str(N) + '_single_act'

sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'
r0_list = np.ones((len(eq_lst), len(method_lst))) * 10

fig = plt.figure(figsize=cm2inch(width, 5 * len(method_lst)))
for mth_idx in range(len(method_lst)):
    stab_method = method_lst[mth_idx]
    for eq_idx in range(len(eq_lst)):
        eq = eq_lst[eq_idx]
        a_eq = roots[eq]
        plt.subplot(len(method_lst), len(eq_lst),
                    mth_idx * len(eq_lst) + eq_idx + 1)

        for T_idx in range(len(T_lst)):
            sim_T = T_lst[T_idx]
            stab_name = ('stabilisation_range_N' + str(N) + '_eq' + str(eq) +
                         '_' + stab_method + '_T' + str(sim_T))
            r0, rT, a0, sim_T, dt = load_stabilisation(stab_name, a_eq)
            r0_max, rT_max, success = edge_detection(r0, rT)
            print('r0_max = %.3e' % (r0_max))
            print('rT_max = %.3e' % (rT_max))
            print()
            if T_idx == 0:
                success = False
            if success:
                r0_list[eq_idx, mth_idx] = np.min([r0_list[eq_idx, mth_idx],
                                                  r0_max])
                plt.loglog([r0_max, r0_max], [1e-12, 1e3], '--',
                           color=col3[T_idx], alpha=alpha,
                           linewidth=lw/1.5)
            plt.loglog(r0, rT, '.', color=col3[T_idx], alpha=alpha,
                       markersize=ms/1.7)
        _, d_min, _, d_max = min_distance(sim_file, a_eq, dist=True,
                                          max_dist=True)
        rect = mpatches.Rectangle([d_min, 1e-12], d_max - d_min, 1e3,
                                  ec="none", color=col3[-1], alpha=0.1)
        ax = plt.gca()
        ax.add_patch(rect)

        plt.ylim([1e-12, 1e3])
        plt.xlim([5e-7, 5e0])
        if eq_idx != 0:
            plt.yticks([])

for idx in range(len(eq_lst)):
    plt.subplot(len(method_lst), len(eq_lst),
                (len(method_lst) - 1) * len(eq_lst) + idx + 1)
    plt.xlabel(r'$\|\|\tilde{a}(0)\|\|$')
    plt.subplot(len(method_lst), len(eq_lst), idx + 1)
    plt.title(eq_lst[idx])
for idx in range(len(method_lst)):
    plt.subplot(len(method_lst), len(eq_lst), idx * len(eq_lst) + 1)
    plt.ylabel(r'$\|\|\tilde{a}(T)\|\|$')
    ax1 = plt.subplot(len(method_lst), len(eq_lst), (idx + 1) * len(eq_lst))
    ax2 = ax1.twinx()
    ax2.set_ylabel(titles[idx])
    ax2.set_yticks([])

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)

artist = []
label = []
for T_idx in range(len(T_lst)):
    artist.append(plt.Line2D((.5, .5), (0, 0), color=col3[T_idx],
                             marker='.', linestyle='', linewidth=lw,
                             markersize=ms*2))
    label.append('T = %d' % (T_lst[T_idx]))

fig.legend(artist, label, loc='upper center', ncol=3,
           bbox_to_anchor=(0.5, 1.04))

plt.savefig('Dissertation/' + fig_name + '_single_act', dpi=dpi,
            transparent=transp, bbox_inches='tight', pad_inches=0.1)
plt.show()

print('Max stable initial perturbation magnitude')
for idx in range(len(eq_lst)):
    print('Equilibrium %d' % (eq_lst[idx]))
    for mth in range(len(method_lst)):
        print('%s, max r_0 = %.2e' % (titles[mth], r0_list[idx, mth]))
    print()
print()

# %% Number of actuators effect
act_lst = [1, 2, 5]
eq_lst = [3, 16, 18, 21]
stab_method = 'search'
titles = ['1 actuator', '2 actuators', '5 actuators']

sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'
r0_list = np.ones((len(eq_lst), len(act_lst))) * 10

fig = plt.figure(figsize=cm2inch(width, 5 * len(act_lst)))
for act_idx in range(len(act_lst)):
    act = act_lst[act_idx]
    for eq_idx in range(len(eq_lst)):
        eq = eq_lst[eq_idx]
        a_eq = roots[eq]
        plt.subplot(len(act_lst), len(eq_lst),
                    act_idx * len(eq_lst) + eq_idx + 1)

        for T_idx in range(len(T_lst)):
            sim_T = T_lst[T_idx]
            orig_name = ('stabilisation_range_N' + str(N) + '_eq' + str(eq) +
                         '_' + stab_method + '_T' + str(sim_T))
            if act == 1:
                stab_name = orig_name
            else:
                stab_name = orig_name + '_act' + str(act)
            r0, rT, a0, sim_T, dt = load_stabilisation(stab_name, a_eq)
            r0_max, rT_max, success = edge_detection(r0, rT)
            print('r0_max = %.3e' % (r0_max))
            print('rT_max = %.3e' % (rT_max))
            print()
            if T_idx == 0:
                success = False
            if success:
                r0_list[eq_idx, act_idx] = np.min([r0_list[eq_idx, act_idx],
                                                  r0_max])
                plt.loglog([r0_max, r0_max], [1e-12, 1e3], '--',
                           color=col3[T_idx], alpha=alpha,
                           linewidth=lw/1.5)
            plt.loglog(r0, rT, '.', color=col3[T_idx], alpha=alpha,
                       markersize=ms/1.7)
        _, d_min, _, d_max = min_distance(sim_file, a_eq, dist=True,
                                          max_dist=True)
        rect = mpatches.Rectangle([d_min, 1e-12], d_max - d_min, 1e3,
                                  ec="none", color=col3[-1], alpha=0.1)
        ax = plt.gca()
        ax.add_patch(rect)

        plt.ylim([1e-12, 1e3])
        plt.xlim([5e-7, 5e0])
        if eq_idx != 0:
            plt.yticks([])

for idx in range(len(eq_lst)):
    plt.subplot(len(act_lst), len(eq_lst),
                (len(act_lst) - 1) * len(eq_lst) + idx + 1)
    plt.xlabel(r'$\|\|\tilde{a}(0)\|\|$')

    plt.subplot(len(act_lst), len(eq_lst), idx + 1)
    plt.title(eq_lst[idx])
for idx in range(len(act_lst)):
    plt.subplot(len(act_lst), len(eq_lst), idx * len(eq_lst) + 1)
    plt.ylabel(r'$\|\|\tilde{a}(T)\|\|$')
    ax1 = plt.subplot(len(method_lst), len(eq_lst), (idx + 1) * len(eq_lst))
    ax2 = ax1.twinx()
    ax2.set_ylabel(titles[idx])
    ax2.set_yticks([])

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)

artist = []
label = []
for T_idx in range(len(T_lst)):
    artist.append(plt.Line2D((.5, .5), (0, 0), color=col3[T_idx],
                             marker='.', linestyle='', linewidth=lw,
                             markersize=ms*2))
    label.append('T = %d' % (T_lst[T_idx]))

fig.legend(artist, label, loc='upper center', ncol=3,
           bbox_to_anchor=(0.5, 1.04))

plt.savefig('Dissertation/' + fig_name + '_mult_act', dpi=dpi,
            transparent=transp, bbox_inches='tight', pad_inches=0.1)
plt.show()

print('Max stable initial perturbation magnitude')
for idx in range(len(eq_lst)):
    print('Equilibrium %d' % (eq_lst[idx]))
    for act in range(len(act_lst)):
        print('%s, max r_0 = %.2e' % (titles[act], r0_list[idx, act]))
    print()
print()

# %% Stability range in the direction of the attractor
stab_method = 'min_d'
act = 2

sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'
eq_file = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(eq_file)
r0_list = np.ones((len(eq_lst),)) * 10
d_min_lst = np.zeros((len(eq_lst),))

fig = plt.figure(figsize=cm2inch(width, 5 * 1))
for eq_idx in range(len(eq_lst)):
    eq = eq_lst[eq_idx]
    a_eq = roots[eq]
    plt.subplot(1, len(eq_lst), eq_idx + 1)

    for T_idx in range(len(T_lst)):
        sim_T = T_lst[T_idx]
        orig_name = ('stabilisation_range_N' + str(N) + '_eq' + str(eq) +
                     '_' + stab_method + '_T' + str(sim_T))
        if act == 1:
            stab_name = orig_name
        else:
            stab_name = orig_name + '_act' + str(act)
        r0, rT, a0, sim_T, dt = load_stabilisation(stab_name, a_eq)
        r0_max, rT_max, success = edge_detection(r0, rT)
        print('r0_max = %.3e' % (r0_max))
        print('rT_max = %.3e' % (rT_max))
        print()
        if success:
            r0_list[eq_idx] = np.min([r0_list[eq_idx], r0_max])
        if T_idx == 0:
            success = False
        if success:
            plt.loglog([r0_max, r0_max], [1e-12, 1e3], '--',
                       color=col3[T_idx], alpha=alpha,
                       linewidth=lw/1.5)
        plt.loglog(r0, rT, '.', color=col3[T_idx], alpha=alpha,
                   markersize=ms/1.7)
    _, d_min, _, d_max = min_distance(sim_file, a_eq, dist=True, max_dist=True)
    d_min_lst[eq_idx] = d_min
    print('Equilibrium number %d' % (eq))
    print('Minimum distance = %.4f' % (d_min))
    print('Maximum distance = %.4f' % (d_max))
    print()
#    plt.loglog([d_min, d_min], [1e-12, 1e3], 'k--', alpha=alpha,
#               linewidth=lw/1.5)
    rect = mpatches.Rectangle([d_min, 1e-12], d_max - d_min, 1e3,
                              ec="none", color=col3[-1], alpha=0.15)
    ax = plt.gca()
    ax.add_patch(rect)

    plt.ylim([1e-12, 1e3])
    plt.xlim([5e-5, 1e1])
    if eq_idx != 0:
        plt.yticks([])

for idx in range(len(eq_lst)):
    plt.subplot(1, len(eq_lst), idx + 1)
    plt.xlabel(r'$\|\|\tilde{a}(0)\|\|$')
    plt.subplot(1, len(eq_lst), idx + 1)
    plt.title(eq_lst[idx])

for idx in range(1):
    plt.subplot(1, len(eq_lst), 1)
    plt.ylabel(r'$\|\|\tilde{a}(T)\|\|$')

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)

artist = []
label = []
for T_idx in range(len(T_lst)):
    artist.append(plt.Line2D((.5, .5), (0, 0), color=col3[T_idx],
                             marker='.', linestyle='', linewidth=lw,
                             markersize=ms*2))
    label.append('T = %d' % (T_lst[T_idx]))

fig.legend(artist, label, loc='upper center', ncol=3,
           bbox_to_anchor=(0.5, 1.10))

plt.savefig('Dissertation/' + fig_name + '_min_d', dpi=dpi,
            transparent=transp, bbox_inches='tight', pad_inches=0.1)
plt.show()

print('Max stable initial perturbation magnitude')
for idx in range(len(eq_lst)):
    print('Equilibrium %d' % (eq_lst[idx]))
    print('Attractor dist = %.2e' % (d_min_lst[idx]))
    print('max r_0 = %.2e' % (r0_list[idx]))
    print()
print()

# %% Stabilisation of the simulation
v = (2. * np.pi / 39.) ** 2
N = 96
method = 2
sim_T = 10
tol = 1e-10
n_act = 2
search = 'unique'
ctrl_method = 'lqr_test'
rho = 1e-1

eq_lst = [16, 18]
N_intp = 512
dt_intp = 0.002
sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'
ini_idx_lst = [-400, -400]
r_enable_lst = [1.3, 1.3]
stab_tol = 1e-3
enabled = False

fig_name = '9_CL_sim_N' + str(N)
if enabled:
    fig_name += '_feedback_always'
else:
    fig_name += '_enabling_feedback'

param = {'v': v, 'N': N, 'method': method}
x = np.linspace(0, 2 * np.pi, N, endpoint=False)
x_intp = np.linspace(0, np.pi, N_intp)

eq_file = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(eq_file)
actuators_name = ('actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol))) +
                  '_' + search + '_' + ctrl_method + '_act' + str(n_act))
actuators = load_actuators(actuators_name)

a_OL, t_OL = load_simulation(sim_file)
u_OL = a2u(a_OL, N)
t_OL_intp = np.arange(t_OL[1000], t_OL[-1], dt_intp)
a_OL_intp, u_sim_intp = interpolate_sim(a_OL, u_OL, x, t_OL, new_x=x_intp,
                                        new_t=t_OL_intp)

a_lst = []
u_lst = []
t_lst = []
enabled_lst = []
a_lst_intp = []
u_lst_intp = []
t_lst_intp = []
enabled_lst_intp = []

a_OL_lst = []
u_OL_lst = []

# d_lst = []

for eq_idx in range(len(eq_lst)):
    eq = eq_lst[eq_idx]
    ini_idx = ini_idx_lst[eq_idx]

    CL_sim_file = ('sim_N' + str(N) + '_method' + str(method) +
                   '_nonlin_CL_no_rand_T' + str(sim_T) + '_eq' + str(eq))
    if enabled:
        CL_sim_file += '_enabled'

    a, t = load_simulation(CL_sim_file)
    u = a2u(a, N)
    a_lst.append(a)
    u_lst.append(u)
    t_lst.append(t)

    if enabled:
        enabled_idx = 0
    else:
        enabled_idx = compare_sim(a, a_OL[ini_idx:, :])
    enabled_lst.append(enabled_idx)
    t_enabled = t[enabled_idx]

    idx0 = np.max([0, enabled_idx - 200])
    idx1 = np.min([len(t), idx0 + 400])

    t_CL = t[idx0:idx1]
    t_CL = np.arange(t[idx0], t[idx1], dt_intp)

    enabled_lst_intp.append(np.argmin(np.abs(t_CL - t_enabled)))

    a_CL, u_CL = interpolate_sim(a, u, x, t, new_x=x_intp, new_t=t_CL)
    a_lst_intp.append(a_CL)
    u_lst_intp.append(u_CL)
    t_lst_intp.append(t_CL)

    a_OL_idx, u_OL_idx = interpolate_sim(a_OL, u_OL, x, t_OL - t_OL[ini_idx],
                                         new_x=x_intp, new_t=t_CL)
    a_OL_lst.append(a_OL_idx)
    u_OL_lst.append(u_OL_idx)

# Spatio-temporal solution
plt.figure(figsize=cm2inch(width, 4 * len(eq_lst)))
for eq_idx in range(len(eq_lst)):
    plt.subplot(len(eq_lst), 1, eq_idx + 1)

    x_r = x_intp
    u_r = u_lst_intp[eq_idx]
    t_r = t_lst_intp[eq_idx]

    X, T = np.meshgrid(x_r, t_r)
    c_map = 'jet'
    # c_map = 'Blues'
    # c_map = 'bone'
    # c_map = 'gray'
    plt.contourf(T, X, u_r, cmap=c_map)
    cb = plt.colorbar()
    plt.xlabel(r'$time$')
    plt.ylabel(r'$x$')
    plt.yticks(np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.]),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$'])

plt.tight_layout()
# plt.subplots_adjust(left=0.08, right=1.05, top=0.95, bottom=0.17)

plt.savefig('Dissertation/' + fig_name + '_u', dpi=dpi,
            transparent=transp, bbox_inches='tight', pad_inches=0.1)
plt.show()

# Fourier decomposition
fig = plt.figure(figsize=cm2inch(width, 4))
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    odd_cf = 2 * p + 1
    even_cf = 2 * p + 2
    y_str = r'$a_{' + str(even_cf) + '}$'
    x_str = r'$a_{' + str(odd_cf) + '}$'
    plt.ylabel(y_str)
    plt.xlabel(x_str)
    plt.axis('equal')
    if not enabled:
        plt.plot(a_OL_intp[:, odd_cf], a_OL_intp[:, even_cf], c=grey,
                 alpha=alpha, linewidth=lw/1.8, zorder=0)
for eq_idx in range(len(eq_lst)):
    eq = eq_lst[eq_idx]
    a_eq = roots[eq]
    a = a_lst_intp[eq_idx]
    enabled_idx = enabled_lst_intp[eq_idx]
    a_enabled = a[enabled_idx:, :]
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
#        plt.plot(a_eq[odd_cf], a_eq[even_cf], 'x', color=col2[eq_idx],
#                 ms=ms/1.5)
        plt.plot(a_enabled[0, odd_cf], a_enabled[0, even_cf], '.',
                 color=col2[eq_idx], ms=ms/1.8)
        plt.plot(a_enabled[:, odd_cf], a_enabled[:, even_cf], '-',
                 color=col2[eq_idx], lw=lw/1.8, zorder=10)

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
ax_lst = []
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    # plt.grid()
    # plt.axis('equal')
    ax = np.array(plt.axis())
    ax[0] -= ax_k * (ax[1] - ax[0])
    ax[1] += ax_k * (ax[1] - ax[0])
    ax[2] -= ax_k * (ax[3] - ax[2])
    ax[3] += ax_k * (ax[3] - ax[2])
    plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
    plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
    # ax = [-max_ax, max_ax, -max_ax, max_ax]
    ax_lst.append(ax)
    plt.axis(tuple(ax))

plt.savefig('Dissertation/' + fig_name + '_a',
            dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# Other plots
stab_time = []

fig = plt.figure(figsize=cm2inch(width, 8))
for eq_idx in range(len(eq_lst)):
    eq = eq_lst[eq_idx]
    a_eq = roots[eq]
    r_enable = r_enable_lst[eq_idx]
    enabled_idx = enabled_lst_intp[eq_idx]
    a = a_lst_intp[eq_idx]
    t = t_lst_intp[eq_idx]
    d, r = disturbance(a, a_eq)
    a_OL = a_OL_lst[eq_idx]
    d_OL, r_OL = disturbance(a_OL, a_eq)

    Mu = actuators[eq]['Mu']
    Sigma = actuators[eq]['Sigma']
    Q = np.diagflat(np.ones_like(a_eq))
    R = rho * np.diagflat(np.ones(len(Mu)))
    A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R)
    h = sim_actuators(d, K, idx=enabled_lst_intp[eq_idx])

    plt.subplot(2, len(eq_lst), eq_idx + 1)
    if not enabled:
        plt.plot([t[0], t[-1]], [r_enable, r_enable], '--k', alpha=alpha,
                 lw=lw/2)
#       plt.plot([t[enabled_idx], t[enabled_idx]], [0, r_enable], '--k',
#                alpha=alpha, lw=lw/2)
        plt.plot(t, r_OL, grey, alpha=alpha, linewidth=lw/1.8, zorder=0)
#    plt.plot(t, r, color=col2[eq_idx], lw=lw/1.5)
    plt.plot(t[enabled_idx:], r[enabled_idx:], color=col2[eq_idx],
             lw=lw/1.5, zorder=10)
    stab_idx = np.argmin(np.abs(r - stab_tol))
    stab_time.append(t[stab_idx] - t[enabled_idx])

    plt.ylim([0, 4.8])
    plt.xlim([t[0], t[-1]])

    plt.subplot(2, len(eq_lst), eq_idx + 3)
    for idx in range(h.shape[1]):
        plt.plot(t, h[:, idx], label='act '+str(idx+1), lw=lw/1.5)
    plt.xlim([t[0], t[-1]])
    if enabled:
        plt.ylim([-15, 15])
    else:
        plt.ylim([-40, 40])

plt.subplot(2, len(eq_lst), 1)
plt.title('16')
plt.ylabel(r'$\|\|\tilde{a}(t)\|\|$')
plt.subplot(2, len(eq_lst), 2)
plt.title('18')
plt.subplot(2, len(eq_lst), 3)
plt.xlabel('time')
plt.ylabel(r'$h_{act}(t)$')
plt.legend(loc='lower right')
plt.subplot(2, len(eq_lst), 4)
plt.xlabel('time')
plt.legend(loc='lower right')

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
plt.savefig('Dissertation/' + fig_name + '_magnitude',
            dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

print()
print('Stabilisation times, tol = %.3e' % (stab_tol))
for eq_idx in range(len(eq_lst)):
    print('  Equilibrium %d, stabilisation time = %.3f'
          % (eq_lst[eq_idx], stab_time[eq_idx]))
print()

# Solution in a fixed time t
xticks = np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.])
xticks_lab = ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
              r'$5\pi/6$', r'$\pi$']

plt.figure(figsize=cm2inch(width/1.5, 8))
for eq_idx in range(len(eq_lst)):
    plt.subplot(len(eq_lst), 1, eq_idx + 1)

    eq = eq_lst[eq_idx]
    a_eq = roots[eq]
    u_eq = a2u(roots[eq], N)
    u_eq_intp = interpolate(x_intp, x, u_eq)

    enabled_idx = enabled_lst_intp[eq_idx]
    u = u_lst_intp[eq_idx]
    a = a_lst_intp[eq_idx]
    d, r = disturbance(a, a_eq)

    Mu = actuators[eq]['Mu']
    Sigma = actuators[eq]['Sigma']
    Q = np.diagflat(np.ones_like(a_eq))
    R = rho * np.diagflat(np.ones(len(Mu)))
    A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R)
    h = sim_actuators(d, K, idx=enabled_lst_intp[eq_idx])

    u_enable = u[enabled_idx, :]
    h_enable = h[enabled_idx, :]

    for act_idx in range(len(h_enable)):
        h_act = h_enable[act_idx]
        B_act = B[:, act_idx]
        a_act = h_act * B_act
        u_act = a2u(a_act, N)
        u_act_intp = interpolate(x_intp, x, u_act)
        plt.plot(x_intp, u_act_intp, label='act '+str(act_idx+1),
                 linewidth=lw/1.5)
    plt.plot(x_intp, u_enable, '-', label='solution',
             color=col2[eq_idx], linewidth=lw/1.5)
    plt.plot(x_intp, u_eq_intp, '--', label='equilibrium',
             color=col2[eq_idx], linewidth=lw/1.5)

    plt.ylabel('u')
    plt.xlim([0, np.pi])
    plt.legend(loc='lower left', ncol=2)

plt.subplot(len(eq_lst), 1, 1)
if enabled:
    plt.ylim([-15, 15])
else:
    plt.ylim([-30, 30])
plt.xticks(xticks, [''] * len(xticks))
plt.subplot(len(eq_lst), 1, 2)
if enabled:
    plt.ylim([-5, 5])
else:
    plt.ylim([-40, 40])
plt.xlabel('x')
plt.xticks(xticks, xticks_lab)

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
# plt.grid()

plt.savefig('Dissertation/' + fig_name + '_u_T', dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()

# %% Closed-loop simulations
"""# Example of zoomed plot of the coefficients
N = 96
method = 2
tol = 1e-10
rho = 1e-5
eq = 3
n_act = 1
search = 'unique'
ctrl_method = 'lqr_test'
stab_method = 'search'
sim_T = 24
zoomed = True
fig_name = 'CL_simulation_test'

# Required files
eq_file = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(eq_file)
a_eq = roots[eq]
OL_file = 'sim_N' + str(N) + '_method' + str(method) + '_nonlin_OL'
a_OL, t_OL = load_simulation(OL_file)
act_file = ('actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol))) +
            '_' + search + '_' + ctrl_method + '_act' + str(n_act))
actuators = load_actuators(act_file)
stab_name = ('stabilisation_range_N' + str(N) + '_eq' + str(eq) +
             '_' + stab_method + '_T' + str(sim_T))
r0, rT, a0, sim_T, dt = load_stabilisation(stab_name, a_eq)

param = {'v': v, 'N': N, 'method': method}
t = np.arange(0, sim_T, dt)

Mu = actuators[eq]['Mu']
Sigma = actuators[eq]['Sigma']
Q = np.diagflat(np.ones_like(a_eq))
R = rho * np.diagflat(np.ones(len(Mu)))
A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R)
BK = np.dot(B, K)

r0_max, rT_max, success = edge_detection(r0, rT)
r0_max = 0.010
idx = np.argmin(np.abs(r0_max - r0))
a_ini = a0[idx]
u0 = a2u(a_ini, N)
a, u = sim_CL_nonlin_KS(x, t, u0, param, BK, a_eq)

# Coefficients
ax_k = 0.05

fig = plt.figure(figsize=cm2inch(width, 4))
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    odd_cf = 2 * p + 1
    even_cf = 2 * p + 2
    y_str = r'$a_{' + str(even_cf) + '}$'
    x_str = r'$a_{' + str(odd_cf) + '}$'
    plt.ylabel(y_str)
    plt.xlabel(x_str)
    plt.axis('equal')
    if zoomed:
        OL_odd, OL_even = disturbance_zoom(a_OL[:, odd_cf], a_OL[:, even_cf],
                                           a_eq[odd_cf], a_eq[even_cf])
        CL_odd, CL_even = disturbance_zoom(a[:, odd_cf], a[:, even_cf],
                                           a_eq[odd_cf], a_eq[even_cf])
    else:
        OL_odd = a_OL[:, odd_cf]
        OL_even = a_OL[:, even_cf]
        CL_odd = a[:, odd_cf]
        CL_even = a[:, even_cf]

    plt.plot(OL_odd, OL_even, color='k', alpha=alpha, linewidth=lw)
    plt.plot(CL_odd, CL_even, color='r', alpha=alpha, linewidth=lw)

plt.tight_layout(pad=pad, w_pad=w_pad, h_pad=h_pad)
for p in range(cf[0] * cf[1]):
    plt.subplot(cf[0], cf[1], p + 1)
    # plt.grid()
    # plt.axis('equal')
    ax = np.array(plt.axis())
    ax[0] -= ax_k * (ax[1] - ax[0])
    ax[1] += ax_k * (ax[1] - ax[0])
    ax[2] -= ax_k * (ax[3] - ax[2])
    ax[3] += ax_k * (ax[3] - ax[2])
    plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.4)
    plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.4)
    plt.axis(tuple(ax))

plt.savefig('Dissertation/' + fig_name + '_a', dpi=dpi, transparent=transp,
            bbox_inches='tight', pad_inches=0.1)
plt.show()"""

# %% DEFINITIONS
