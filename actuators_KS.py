# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 15:03:20 2017

@author: Gengiro
"""
import shelve


def create_actuators(filename, total_eq):
    # Creates a new file to save the actuators of each equilibrium point
    fold = 'Equilibria' + '/'
    d = shelve.open(fold + filename, 'n')
    actuators = []
    for k in range(total_eq):
        actuators.append({'is_controllable': False, 'Mu': [], 'Sigma': []})
    d['actuators'] = actuators
    print('File created (%s) for %d equilibria' %
          (fold + filename, total_eq))
    d.close()


def load_actuators(filename):
    # Loads the actuators
    print('Loading actuators variable from file: ' + filename)
    fold = 'Equilibria' + '/'
    d = shelve.open(fold + filename)
    if 'actuators' in d:
        actuators = d['actuators']
    else:
        klist = d.keys()
        print()
        print('Error trying to load the actuators, available keys:')
        print()
        for key in klist:
            print(key)
        d.close()
        assert()
    d.close()
    print('Number of actuators loaded: %d' % (len(actuators)))
    print()
    return actuators


def save_actuators(filename, new_actuators, roots, x, param):
    # Opens the actuators file and modifies the actuators
    from lin_KS import lin_KS
    from controllability_KS import Gramian_KS, controllability
    from force_KS import B_KS
    from pathlib import Path

    fold = 'Equilibria' + '/'
    file = Path(fold + filename + '.dat')
    try:
        abs_path = file.resolve()
    except FileNotFoundError:
        print('File not found, creating a new one\n')
        create_actuators(filename, len(roots))
    d = shelve.open(fold + filename)
    old_actuators = d['actuators']
    print('Saving new actuators in file: ' + filename)
    print()
    if len(new_actuators) != len(old_actuators):
        d.close()
        raise AttributeError('Length mismatch in the new_actuators list')

    for k in range(len(old_actuators)):
        new_ctrl = new_actuators[k]['is_controllable']
        new_Mu = new_actuators[k]['Mu']
        new_Sigma = new_actuators[k]['Sigma']
        old_ctrl = old_actuators[k]['is_controllable']
        old_Mu = old_actuators[k]['Mu']
        old_Sigma = old_actuators[k]['Sigma']

        replace = new_ctrl and not(old_ctrl)
        both_ctrl = new_ctrl and old_ctrl
        if replace:
            old_actuators[k] = new_actuators[k]
            print('Equilibrium point num %d is now controllable' % (k))
        elif both_ctrl and (new_actuators[k] != old_actuators[k]):
            print('Equilibrium point num %d has new possible actuators' % (k))

            A = lin_KS(roots[k], param['v'])
            new_B, tmp_F, tmp_f = B_KS(x, new_Mu, new_Sigma, param['N'])
            new_Wc = Gramian_KS(A[1:, 1:], new_B[1:, :])
            new_rank, new_det, new_min_eig = controllability(new_Wc)
            print('   New Wc min eigenvalue = %.3g' % (new_min_eig))

            old_B, tmp_F, tmp_f = B_KS(x, old_Mu, old_Sigma, param['N'])
            old_Wc = Gramian_KS(A[1:, 1:], old_B[1:, :])
            old_rank, old_det, old_min_eig = controllability(old_Wc)
            print('   Old Wc min eigenvalue = %.3g' % (old_min_eig))

            if new_min_eig > old_min_eig:
                print('  New actuators are better')
                old_actuators[k] = new_actuators[k]
            else:
                print('  Old actuators are better')

    d['actuators'] = old_actuators
    d.close()
    print()
    print('Actuators file updated!')
    print()
    return old_actuators


def plot_check_actuators(actuators, n_actuators, n_pos_eig):
    import matplotlib.pyplot as plt
    import numpy as np
    font_size = 13
    K = np.arange(len(actuators))

    plt.figure(figsize=(10, 15))
    plt.subplot(3, 1, 1)
    plt.plot(K, n_actuators, 'xb', label='Actuators')
    plt.plot(K, n_pos_eig, 'xr', label='Unstable directions')
    plt.xlabel('Equilibria num', fontsize=font_size)
    plt.ylabel('Number', fontsize=font_size)
    plt.title('Actuators and unstable directions',
              fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.grid()
    plt.legend(fontsize=font_size)
    ax = list(plt.axis())
    ax[0] = 0
    ax[1] = K[-1]
    plt.axis(tuple(ax))

    plt.subplot(3, 1, 2)
    for k in K:
        act = actuators[k]
        plt.plot(k * np.ones_like(act['Mu']), act['Mu'], 'xb')
    plt.xlabel('Equilibria num', fontsize=font_size)
    plt.ylabel(r'$\mu$ [rad]', fontsize=font_size)
    plt.title('Mu value of the actuators', fontsize=font_size)
    plt.yticks(np.pi / 6. * np.array(range(7)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$'], fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.grid()
    ax = list(plt.axis())
    ax[0] = 0
    ax[1] = K[-1]
    plt.axis(tuple(ax))

    plt.subplot(3, 1, 3)
    for k in K:
        act = actuators[k]
        plt.plot(k * np.ones_like(act['Sigma']), act['Sigma'], 'xb')
    plt.xlabel('Equilibria num', fontsize=font_size)
    plt.ylabel(r'$\sigma$ [rad]', fontsize=font_size)
    plt.title('Sigma value of the actuators', fontsize=font_size)
    """plt.yticks(np.pi / 6. * np.array(range(7)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$'], fontsize=font_size)"""
    plt.xticks(fontsize=font_size)
    plt.grid()
    ax = list(plt.axis())
    ax[0] = 0
    ax[1] = K[-1]
    plt.axis(tuple(ax))

    plt.tight_layout()


def check_actuators(actuators, roots, param):
    from lin_KS import lin_KS, eig_lin_KS

    print('Checking the current actuators')
    print()
    n_ctrl = 0
    n_unctrl = 0
    n_actuators = []
    n_pos_eig = []

    for k in range(len(actuators)):
        if actuators[k]['is_controllable']:
            n_ctrl += 1
        else:
            print('Equilibria num %d is not controllable' % (k))
            n_unctrl += 1

        n_actuators.append(len(actuators[k]['Mu']))
        A = lin_KS(roots[k], param['v'])
        eig, pos_eig = eig_lin_KS(A, plot=False)
        n_pos_eig.append(pos_eig['n_pos'])

    plot_check_actuators(actuators, n_actuators, n_pos_eig)
    print()
    print('Total number of controllable equilibria: %d' % (n_ctrl))
    print('Total number of uncontrollable equilibria: %d' % (n_unctrl))
    print()
