# -*- coding: utf-8 -*-
"""
Script to solve/simulate the closed-loop system originated by the
Kuramoto-Sivashinsky equation
Created on Thu Jun  8 17:29:58 2017

@author: Jorge Vidal-Ribas
University of Southampton
"""

import numpy as np
from equilibria_KS import load_equilibria
from simulations import create_simulation, load_simulation, compare_sim
from lin_KS import disturbance
from nonlin_KS import sim_CL_nonlin_KS, plot_coeff_KS, plot_sol_KS
from actuators_KS import load_actuators
from lqr import sim_actuators, plot_lsim, lqr_matrices_KS
from rfft_norm import a2u
from time import time


v = (2. * np.pi / 39.) ** 2
N = 96
method = 2
tol = 1e-10
sim_T = 10
dt = 0.01
seed = 1
ovrwrt = True
rand_u0 = False

n_act = 2
search = 'unique'
ctrl_method = 'lqr_test'
rho = 1e-1

eq = 3
r_enable = 1.3
r_enable = False
ini_idx = 0

sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'

x = np.linspace(0, 2 * np.pi, N, endpoint=False)
t = np.arange(0, sim_T, dt)

param = {'v': v, 'N': N, 'method': method}
roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)
actuators_name = ('actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol))) +
                  '_' + search + '_' + ctrl_method + '_act' + str(n_act))
actuators = load_actuators(actuators_name)

if actuators[eq]['is_controllable']:
    print('Stabilising equilibria num %d' % (eq))
    a_eq = roots[eq]
    Mu = actuators[eq]['Mu']
    Sigma = actuators[eq]['Sigma']
    Q = np.diagflat(np.ones_like(a_eq))
    R = rho * np.diagflat(np.ones(len(Mu)))
    A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R)

    if rand_u0:
        u0 = np.zeros(x.size)
        np.random.seed(seed)
        u0[1:(x.size-1)] = (np.random.random_sample(x.size-2) -
                            0.5) * 1e-06
    else:
        sim_file = 'sim_N' + str(N) + '_method1_nonlin_OL'
        a_sim, t_sim = load_simulation(sim_file)
        u_sim = a2u(a_sim, N)
        u0 = u_sim[ini_idx, :]

    BK = np.dot(B, K)

    print('Simulating ...')
    t_0 = time()
    a, u = sim_CL_nonlin_KS(x, t, u0, param, BK, a_eq, r=r_enable)
    t_end = time()
    d, d_norm = disturbance(a, a_eq)
    enabled_idx = compare_sim(a, a_sim[ini_idx:, :])
    f = sim_actuators(d, K, idx=enabled_idx)
    plot_sol_KS(u, x, t)
    plot_coeff_KS(a, x, t)
    plot_coeff_KS(d, x, t)
    plot_lsim(a, t, u=f, only_u=True)
    a_err = np.sqrt(np.square(a[-1, :] - a_eq).sum())
    print('Final error = %.5g' % (a_err))
    print('Elapsed time: %.1fs' % (t_end - t_0))
    print()
    CL_sim_file = ('sim_N' + str(N) + '_method' + str(method) +
                   '_nonlin_CL_no_rand_T' + str(sim_T) + '_eq' + str(eq))
    if not r_enable:
        CL_sim_file += '_enabled'
    create_simulation(CL_sim_file, a, dt, overwrite=ovrwrt)
    print('CL enabled at index %d, t = %.2f' % (enabled_idx, t[enabled_idx]))
    print()

else:
    print('Equilibria num %d is uncontrollable' % (eq))
    print()
