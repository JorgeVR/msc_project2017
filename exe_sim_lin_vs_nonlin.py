# -*- coding: utf-8 -*-
"""
Created on Tue Aug  8 12:11:19 2017

@author: Gengiro
"""

import numpy as np
from equilibria_KS import load_equilibria
from controllability_KS import place_actuators, plot_actuators
from lin_KS import lin_KS, sim_lin_KS, sim_CL_lin_KS
from nonlin_KS import sim_nonlin_KS, sim_CL_nonlin_KS, plot_coeff_KS, plot_sol_KS
from lqr import lqr_cont_inf
from rfft_norm import irfft_norm
from time import time


def compare_a(a_lin, a_nlin, a_eq, t):
    print('Comparison of the a_k coefficients in a time simulation\n')
    r = np.linspace(0, len(t) - 1, num=5)
    for idx_t in r:
        idx_t = int(idx_t)
        print('t = %.3f' % (t[idx_t]), '\t a_eq\t\t a_nlin\t\t a_lin',
              '\t\t a_diff\t\t d_nlin\t\t d_lin'
              )
        for idx_k in range(1, 7):
            print('\t\t%7.4f' % (a_eq[idx_k]), '\t%7.4f\t\t%7.4f'
                  % (a_nlin[idx_t, idx_k], a_lin[idx_t, idx_k]),
                  '\t%7.4f' % (a_nlin[idx_t, idx_k] - a_lin[idx_t, idx_k]),
                  '\t%7.4f\t\t%7.4f'
                  % (a_nlin[idx_t, idx_k] - a_eq[idx_k],
                     a_lin[idx_t, idx_k] - a_eq[idx_k]))
        print()


v = (2. * np.pi / 39.) ** 2
N = 96
method = 1
param = {'v': v, 'N': N, 'method': method}
tol = 1e-10
x = np.linspace(0, 2 * np.pi, N, endpoint=False)
t = np.linspace(0, 1, num=101)

roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)

n_eq = 1
a_eq = roots[n_eq]
print('Equilibrium point number %d' % (n_eq))
print('a_eq =')
for k in range(6):
    print('  %7.4f' % (a_eq[k]))
print('   ...')
print()

u0 = irfft_norm(a_eq * 1j, N)
u0[1:(x.size - 1)] += (np.random.random_sample(x.size - 2) - 0.5) * 1e-5

print('SIMULATING NONLINEAR OPEN LOOP SYSTEM ...')
print()
t_0 = time()
a_OL_nlin, u_OL_nlin = sim_nonlin_KS(x, t, u0, param)
t_end = time()
plot_sol_KS(u_OL_nlin, x, t)
plot_coeff_KS(a_OL_nlin, x, t)
print()
print("Total time = %.2f s" % (t_end - t_0))
print()
print('SIMULATING LINEAR OPEN LOOP SYSTEM ...')
print()
t_0 = time()
a_OL_lin, u_OL_lin = sim_lin_KS(x, t, u0, a_eq, param)
t_end = time()
plot_sol_KS(u_OL_lin, x, t)
plot_coeff_KS(a_OL_lin, x, t)
print()
print("Total time = %.2f s" % (t_end - t_0))
print()
compare_a(a_OL_lin, a_OL_nlin, a_eq, t)
