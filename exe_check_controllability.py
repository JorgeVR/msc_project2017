# -*- coding: utf-8 -*-
"""
Created on Fri Aug 11 12:54:32 2017

@author: Gengiro
"""


import numpy as np
from lin_KS import lin_KS
from equilibria_KS import load_equilibria
from controllability_KS import controllable
from actuators_KS import load_actuators


v = (2. * np.pi / 39.) ** 2
N = 96
method = 1
tol = 1e-10
x = np.linspace(0, 2 * np.pi, N, endpoint=False)

param = {'v': v, 'N': N, 'method': method}
roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)
actuators_name = 'actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
actuators = load_actuators(actuators_name)

# list([7])
ctrl_methods = ['ctrb', 'Gramian', 'pos_eig', 'lqr_test']
for eq in range(len(roots)):
    if actuators[eq]['is_controllable']:
        print('EQUILIBRIA num %d ACTUATORS AVAILABLE' % (eq))
        print()
        A = lin_KS(roots[eq], v)
        Mu = actuators[eq]['Mu']
        Sigma = actuators[eq]['Sigma']
        for ctrl_m in ctrl_methods:
            print('Control method: ', ctrl_m)
            ctrl, B, F = controllable(x, Mu, Sigma, N, A, method=method)
            print('CONTROLLABILITY: ' + str(ctrl))
            print()
    else:
        print('EQUILIBRIA num %d ACTUATORS NOT AVAILABLE' % (eq))
        print()
