# -*- coding: utf-8 -*-
"""
Created on Mon May 15 18:01:45 2017

@author: Jorge Vidal-Ribas
"""
import numpy as np
import scipy.integrate as sp
import matplotlib.pyplot as plt
from nonlin_KS import nonlin_KS_ode, sim_nonlin_KS
from rfft_norm import rfft_norm, irfft_norm


def lin_KS(a_eq, v):
    M = a_eq.size
    A = np.zeros((M, M))

    for k in range(M):
        A[k, k] = (k ** 2 - v * (k ** 4))
        for p in range(1, M):
            if (k - p) < 0:
                a1 = - a_eq[p - k]
            else:
                a1 = a_eq[k - p]
            if (k + p) < M:
                a2 = a_eq[k + p]
            else:
                a2 = 0
            # A[k, p] += -2 * p * (a1 + a2)
            A[k, p] += -2 * k * (a1 - a2)
    return A


def lin_KS_ode(d, t, A):    # lambda function when calling odeint!
    if d.shape != (len(A),):
        raise ValueError('Dimension mismatch, d.shape (%d) != A.shape[0] (%d)'
                         % (d.shape[0], A.shape[0]))
    return np.dot(A, np.array(d).T)


def sim_lin_KS(x, t, u0, a_eq, param):
    atol = 1e-12
    v = param['v']
    N = param['N']
    x_step = x[1] - x[0]
    u = np.zeros((len(t), N))
    u[0, :] = u0
    A = lin_KS(a_eq, v)

    # Find the amplitudes of the Fourier decomposition
    a0 = np.imag(rfft_norm(u[0, :], N))
    a0[0] = 0
    d0 = a0 - a_eq
    freq = np.fft.rfftfreq(N, x_step)

    # d_t_k = np.zeros((t.size, freq.size))
    d_t_k = sp.odeint(lin_KS_ode, d0, t, args=(A,), atol=atol)
    assert(d_t_k.shape == (t.size, freq.size))

    a_t_k = np.zeros_like(d_t_k)
    for i in range(t.size):
        a_t_k[i, :] = d_t_k[i, :] + a_eq

    for i in range(1, t.size):
        u[i, :] = irfft_norm(a_t_k[i, :] * 1j, N)

    return a_t_k, u


def sim_CL_lin_KS(x, t, u0, param, BK, a_eq):
    atol = 1e-12
    v = param['v']
    N = param['N']
    x_step = x[1] - x[0]
    u = np.zeros((len(t), N))
    u[0, :] = u0
    A = lin_KS(a_eq, v)

    # Find the amplitudes of the Fourier decomposition
    a0 = np.imag(rfft_norm(u[0, :], N))
    a0[0] = 0
    d0 = a0 - a_eq
    freq = np.fft.rfftfreq(N, x_step)

    # d_t_k = np.zeros((t.size, freq.size))
    d_t_k = sp.odeint(lin_KS_ode, d0, t, args=(A - BK,), atol=atol)
    assert(d_t_k.shape == (t.size, freq.size))

    a_t_k = np.zeros_like(d_t_k)
    for i in range(t.size):
        a_t_k[i, :] = d_t_k[i, :] + a_eq

    for i in range(1, t.size):
        u[i, :] = irfft_norm(a_t_k[i, :] * 1j, N)
    return a_t_k, u


def plot_eigenvalues(val):
    font_size = 13

    print('Eigenvalues of the linearised system:')
    plt.figure(figsize=(10, 5))
    plt.plot(np.real(val), np.imag(val), 'o')
    plt.xlabel(r'$Re(\lambda)$', fontsize=font_size)
    plt.ylabel(r'$Im(\lambda)$', fontsize=font_size)
    plt.title('Eigenvalues of the linearised system', fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    # file_name = 'Eigenvalues'
    # plt.savefig(file_name, dpi=600)
    plt.show()

    pos = {'idx': [], 'val': []}
    neg = {'idx': [], 'val': []}
    for k in range(len(val)):
        if val[k] > 0:
            pos['idx'].append(k)
            pos['val'].append(val[k])
        else:
            neg['idx'].append(k)
            neg['val'].append(val[k])

    plt.figure(figsize=(10, 6))
    plt.semilogy(pos['idx'], np.real(pos['val']), 'o', label='Pos')
    plt.semilogy(neg['idx'], - np.real(neg['val']), 'x', label='Neg')
    plt.legend()
    plt.xlabel('Index', fontsize=font_size)
    plt.ylabel(r'$\log(Re(\lambda))$', fontsize=font_size)
    plt.title('Eigenvalues of the linearised system', fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.show()


def plot_pos_eigenvec(pos_eig):
    font_size = 13
    vec = pos_eig['vec']
    val = pos_eig['val']

    for k in range(len(vec)):
        real_vec = np.real(vec[k])
        imag_vec = np.imag(vec[k])
        print()
        print('Eigenvector coefficients, eig %d: (%.3g, %.3gj)'
              % (k + 1, np.real(val[k]), np.imag(val[k])))
        plt.figure(figsize=(10, 6))
        # plt.subplot(1, 2, 1)
        # plt.plot(vec[k], 'x')
        plt.stem(imag_vec, linefmt='r', markerfmt='ro', basefmt='k--',
                 label='Imag')
        plt.stem(real_vec, linefmt='b', markerfmt='bo', basefmt='k--',
                 label='Real')
        plt.xlabel('k', fontsize=font_size)
        plt.ylabel(r'Eigenvector coeffs: $v_k$',
                   fontsize=font_size)
        plt.title('Eigenvector coefficients, eig %d: (%.3g, %.3gj)'
                  % (k + 1, np.real(val[k]), np.imag(val[k])),
                  fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.legend()
        plt.grid()

#        plt.subplot(1, 2, 2)
#        plt.stem(real_vec)
#        plt.xlabel('k', fontsize=font_size)
#        plt.ylabel(r'Eigenvector coeffs: $\vert v_k \vert$',
#                   fontsize=font_size)
#        plt.yticks(fontsize=font_size)
#        plt.xticks(fontsize=font_size)

        plt.tight_layout()
        plt.show()


def sort_eig(val, vec):
    order = np.argsort(np.real(val))
    val = val[order]
    vec = vec[:, order]
    return val, vec


def eig_lin_KS(A, side='right', plot=True):
    val, vec = np.linalg.eig(A)
    val, vec = sort_eig(val, vec)
    if side == 'right':
        pass
    elif side == 'left':
        vec = np.linalg.inv(vec).T
    else:
        raise ValueError('Side input wrong, choose right or left')
    eig = {'val': val, 'vec': []}
    pos_eig = {'val': [], 'vec': [], 'n_pos': 0}

    for k in range(len(vec[0, :])):
        eig['vec'].append(vec[:, k])

    for k in range(len(val)):
        if np.real(val[k]) > 1e-12:
            if pos_eig['val'] == []:
                pos_eig['val'].append(val[k])
                pos_eig['vec'].append(vec[:, k])
            else:
                for p in range(len(pos_eig['val'])):
                    real_cond = np.abs(np.real(val[k]) -
                                       np.real(pos_eig['val'][p])) < 1e-9
                    imag_cond = np.abs(np.imag(val[k]) +
                                       np.imag(pos_eig['val'][p])) < 1e-9
                    if real_cond and imag_cond:
                        break
                else:
                    pos_eig['val'].append(val[k])
                    pos_eig['vec'].append(vec[:, k])
    pos_eig['n_pos'] = len(pos_eig['val'])

    # Plotting the eigenvalues
    if plot:
        plot_eigenvalues(val)

        print('Eigenvalues with positive real part:')
        if pos_eig['val'] == []:
            print('  None')
        for k in range(len(pos_eig['val'])):
            item = pos_eig['val'][k]
            print('  %d: (%.8g, %.8gj)'
                  % (k + 1, np.real(item), np.imag(item)))

        plot_pos_eigenvec(pos_eig)

    return eig, pos_eig


def complex2real(eigenval, eigenvec):
    val = np.real(eigenval)
    vec = np.real(eigenvec)
    vec /= np.linalg.norm(vec)
    return val, vec


def abs_error(a, b):
    return np.sqrt(np.square(a - b).sum())


def rel_error(old, new):
    return(abs_error(old, new) / abs_error(old, 0))


def plot_eps_dist(epsilon, Er):
    font_size = 13
    print()
    print('Relative error in the direction of positive eigenvalues:')

    plt.figure(figsize=(10, 6))
    for k in range(len(Er[:, 1])):
        label_str = 'eig ' + str(k + 1)
        plt.loglog(epsilon, Er[k, :], label=label_str)
    plt.ylabel(r'Absolute error, E = $\Vert \.a_l - \.a_{nl}\Vert$',
               fontsize=font_size)
    plt.xlabel(r'Disturbance ($\epsilon$), $a = a_{eq} + \epsilon * v$',
               fontsize=font_size)
    plt.title('Absolute error in the direction of positive eigenvalues',
              fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.legend()
    plt.grid()
    plt.show()


def epsilon_disturbance(a_eq, pos_eig, A, p):
    # Study of the error between the linear and nonlinear systems under
    # an epsilon disturbance in the direction of the max eigenvalue.
    pos_val = pos_eig['val']
    pos_vec = pos_eig['vec']

    epsilon_range = np.logspace(-15, 1, 33)
    Er = np.zeros((len(pos_val), len(epsilon_range)))

    for k in range(len(pos_val)):
        val, vec = complex2real(pos_val[k], pos_vec[k])
        for m in range(len(epsilon_range)):
            epsilon = epsilon_range[m]
            d = epsilon * vec
            a = a_eq + d
            dadt_l = lin_KS_ode(d, 1, A)
            dadt_nl = nonlin_KS_ode(a, 1, p['method'], p['N'], p['v'])
            Er[k, m] = abs_error(dadt_nl, dadt_l)
    plot_eps_dist(epsilon_range, Er)


def plot_eig_field(XX, YY, da_l, da_nl, lin_d, nlin_d, real_vec, V):
    font_size = 13
    q_units = 'xy'
    q_angles = 'xy'
    q_scale = 100  # 15
    real_vec = real_vec[::-1]
    max_x = np.max(XX)
    max_y = np.max(YY)

    print()
    print('Fourier coefficients vector field:')
    print('  linear (red) / nonlinear (blue)')

    plt.figure(figsize=(9, 8))
    plt.title('Fourier coefficients vector field', fontsize=font_size)
    for k in range(len(real_vec)):
            v_xy = np.dot(V, real_vec[k])
            px = max_x / (v_xy[0] + 1e-8)
            py = max_y / (v_xy[1] + 1e-8)
            p = np.min([px, py])

            eig_x = np.array([0, v_xy[0] * p])
            eig_y = np.array([0, v_xy[1] * p])
            plt.plot(eig_x, eig_y, 'k--', alpha=0.7)
            plt.plot(- eig_x, - eig_y, 'k--', alpha=0.7)

    plt.quiver(XX, YY, da_l[:, :, 0], da_l[:, :, 1], units=q_units,
               angles=q_angles, scale=q_scale, color='r', alpha=1)
    plt.quiver(XX, YY, da_nl[:, :, 0], da_l[:, :, 1], units=q_units,
               angles=q_angles, scale=q_scale, color='b', alpha=0.7)
    v = plt.axis()

    for s in range(len(lin_d)):
            plt.plot(lin_d[s][:, 0], lin_d[s][:, 1], 'r--',
                     linewidth=0.7)
            plt.plot(nlin_d[s][:, 0], nlin_d[s][:, 1], 'b--',
                     linewidth=0.7, alpha=0.7)

    y_str = r'$v_2*$'
    x_str = r'$v_1$'
    plt.ylabel(y_str, fontsize=font_size)
    plt.xlabel(x_str, fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.grid()
    plt.axis(v)
    plt.show()


def plot_error_field(XX, YY, Er):
    font_size = 13
    print()
    print('Absolute error surface:')

    plt.figure(figsize=(11, 5))
    levels = np.linspace(np.min(Er), np.max(Er), 10)
    plt.contourf(XX, YY, Er, levels, cmap='jet')
    cb = plt.colorbar(extend='both')
    cb.ax.tick_params(labelsize=font_size)
    plt.xlabel(r'$v_1$', fontsize=font_size)
    plt.ylabel(r'$v_2*$', fontsize=font_size)
    plt.title(r'Absolute error surface, E = $\Vert \.a_l - \.a_{nl}\Vert$',
              fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.grid()
    # plt.tight_layout()
    plt.show()


def ortho_vec(v1, v2):
    v = np.array([v1, v2]).T
    q, r = np.linalg.qr(v, mode='reduced')
    o1 = q[:, 0]
    o2 = q[:, 1]
    return o1, o2


def eig_CS(pos_vec):
    pos_vec = pos_vec[::-1]
    v1 = pos_vec[0]
    v3 = np.zeros_like(v1)
    v3[0] = 1
    if len(pos_vec) < 2:
        v2 = np.ones_like(v1)
        v2[0] = 0
    else:
        v2 = pos_vec[1]
    diag = np.ones_like(v1)
    V = np.diagflat(diag)
    V[:, 0] = v1
    V[:, 1] = v2
    V[:, 2] = v3

    v_o1, v_o2 = ortho_vec(v1, v2)
    V_o = V.copy()
    V_o[:, 1] = v_o2
    V_inv = np.linalg.inv(V_o)

    V_d = V_inv[0:2, :]
    V_xy = np.dot(V_inv, V)[0:2, 0:2]
    V = V[:, 0:2]

    return V, V_d, V_xy


def a0_eig(a_eq, XX, YY, V):
    AA = np.zeros((len(XX[:, 0]), len(XX[0, :]), len(a_eq)))
    DD = np.zeros_like(AA)
    for idx1 in range(len(XX[:, 0])):
        for idx2 in range(len(XX[0, :])):
            XY = np.array([[XX[idx1, idx2]], [YY[idx1, idx2]]])
            d0 = np.dot(V, XY).flatten()
            DD[idx1, idx2, :] = d0
            AA[idx1, idx2, :] = d0 + a_eq
    return AA, DD


def eig_mesh(XX, YY, Vxy):
    XXv = np.zeros_like(XX)
    YYv = np.zeros_like(YY)
    for idx1 in range(0, len(XX[:, 0])):
        for idx2 in range(0, len(XX[0, :])):
            x = XX[idx1, idx2]
            y = YY[idx1, idx2]
            xy = np.array([[x, y]]).T
            xyv = np.dot(Vxy, xy)

            XXv[idx1, idx2] = xyv[0, :]
            YYv[idx1, idx2] = xyv[1, :]
    return XXv, YYv


def disturbance(a, a_eq):
    d = np.zeros_like(a)
    if a.ndim == 1:
        d = a - a_eq
        d_norm = np.linalg.norm(d)
    else:
        for m in range(len(a_eq)):
            d[:, m] = a[:, m] - a_eq[m]
        d_norm = np.linalg.norm(d, axis=1)
    return d, d_norm


def plot_growth_rate(lin_d, nlin_d, t, eigval, k):
    font_size = 13
    c0 = np.log(lin_d[0])
    cmax = np.log(lin_d[-1])
    c = np.linspace(2 * c0 - cmax, cmax, num=7)
    print()
    print('Growth rate,  eig %d (%.3f, %.3fj)'
          % (k, np.real(eigval), np.imag(eigval)))

    plt.figure(figsize=(10, 5))
    for c_val in c:
        plt.plot(t, np.real(eigval) * t + c_val, 'k--')
    plt.plot([], [], 'k--', label=r'$Re(\lambda) + C$')
    plt.plot(t, np.log(lin_d), 'r', label='lin')
    plt.plot(t, np.log(nlin_d), 'b', label='nonlin', alpha=0.5)

    plt.ylabel(r'$\lg(\Vert a(t) - a_{eq}\Vert)$', fontsize=font_size)
    plt.xlabel('time', fontsize=font_size)
    plt.title('Growth rate,  eig %d (%.3f, %.3fj)'
              % (k, np.real(eigval), np.imag(eigval)), fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.legend()
    # plt.grid()
    # plt.tight_layout()
    plt.show()


def growth_rate(a_eq, pos_eig, A, p):
    eig_val = pos_eig['val']
    eig_vec = pos_eig['vec']
    epsilon = 1e-6
    t = np.linspace(0, 3, num=301)
    x = np.linspace(0, 2 * np.pi, num=p['N'], endpoint=False)

    for k in range(len(eig_val)):
        val, vec = complex2real(eig_val[k], eig_vec[k])
        a0 = a_eq + epsilon * vec
        u0 = irfft_norm(a0 * 1j, p['N'])

        lin_a, lin_u = sim_lin_KS(x, t, u0, a_eq, p)
        lin_d, lin_d_norm = disturbance(lin_a, a_eq)

        nlin_a, nlin_u = sim_nonlin_KS(x, t, u0, p)
        nlin_d, nlin_d_norm = disturbance(nlin_a, a_eq)

        plot_growth_rate(lin_d_norm, nlin_d_norm, t, eig_val[k], k + 1)


def real_eig(cmplx_val, cmplx_vec):
    real_val = []
    real_vec = []
    for k in range(len(cmplx_val)):
        real_val.append(np.real(cmplx_val[k]))
        vec = np.real(cmplx_vec[k])
        vec /= np.linalg.norm(vec)
        real_vec.append(vec)
    return real_val, real_vec


def eig_vector_field(a_eq, pos_eig, A, p):
    na = 3
    delta = 10e-2
    dsim = 2
    t = np.linspace(0, 0.5, num=101)
    x = np.linspace(0, 2 * np.pi, num=p['N'], endpoint=False)

    real_val, real_vec = real_eig(pos_eig['val'], pos_eig['vec'])
    V, V_d, V_xy = eig_CS(real_vec)

    X = np.linspace(-na * delta, na * delta, 2 * na + 1)
    Y = np.linspace(-na * delta, na * delta, 2 * na + 1)
    XX, YY = np.meshgrid(X, Y)
    XXv, YYv = eig_mesh(XX, YY, V_xy)

    dadt_l_v = np.zeros((len(XX[:, 0]), len(XX[0, :]), 2))
    dadt_nl_v = np.zeros_like(dadt_l_v)
    Er_v = np.zeros_like(XX)
    sim_l = []
    sim_nl = []

    AA, DD = a0_eig(a_eq, XX, YY, V)

    for idx1 in range(len(AA[:, 0])):
        for idx2 in range(len(AA[0, :])):
            a = AA[idx1, idx2, :]
            d = DD[idx1, idx2, :]

            dadt_l = lin_KS_ode(d, 1, A)
            dadt_nl = nonlin_KS_ode(a, 1, p['method'], p['N'], p['v'])

            dadt_l_v[idx1, idx2, :] = np.dot(V_d, dadt_l).flatten()
            dadt_nl_v[idx1, idx2, :] = np.dot(V_d, dadt_nl).flatten()
            Er_v[idx1, idx2] = abs_error(dadt_l, dadt_nl)

    # for sim in range(nsim):
    for idx1 in range(0, len(XX[:, 0]), dsim):
        for idx2 in range(0, len(XX[0, :]), dsim):

            a0 = AA[idx1, idx2, :]
            u0 = irfft_norm(a0 * 1j, p['N'])

            lin_a, lin_u = sim_lin_KS(x, t, u0, a_eq, p)
            lin_d, lin_d_norm = disturbance(lin_a, a_eq)

            nlin_a, nlin_u = sim_nonlin_KS(x, t, u0, p)
            nlin_d, nlin_d_norm = disturbance(nlin_a, a_eq)

            lin_d_v = np.zeros((len(t), 2))
            nlin_d_v = np.zeros_like(lin_d_v)
            for idx_t in range(len(t)):
                lin_d_v[idx_t, :] = np.dot(V_d, lin_d[idx_t, :])
                nlin_d_v[idx_t, :] = np.dot(V_d, nlin_d[idx_t, :])

            sim_l.append(lin_d_v)
            sim_nl.append(nlin_d_v)

    # Plotting figures
    plot_eig_field(XXv, YYv, dadt_l_v, dadt_nl_v, sim_l, sim_nl, real_vec, V_d)
    plot_error_field(XXv, YYv, Er_v)


def lin_checks(roots, filt, param):
    if filt == []:
        r_range = range(len(roots))
    else:
        r_range = filt

    # real_eig = np.zeros((len(r_range), 1))
    # imag_eig = np.zeros((len(r_range), 1))

    for k in range(len(r_range)):
        print()
        print('LINEARISATION OF THE ROOT %d' % (r_range[k]))
        print()
        a_eq = roots[r_range[k]]
        A = lin_KS(a_eq, param['v'])
        eig, pos_eig = eig_lin_KS(A)

        if len(pos_eig['val']) > 0:
            growth_rate(a_eq, pos_eig, A, param)
            # epsilon_disturbance(a_eq, pos_eig, A, param)
            eig_vector_field(a_eq, pos_eig, A, param)

        """
    plt.figure(figsize=(9, 4))
    plt.plot(real_eig, 'o', label='Real')
    plt.plot(imag_eig, 'x', label='Imag')
    plt.plot([0, len(real_eig) - 1], [0, 0], 'k--')
    plt.legend()
    r_str = [str(item) for item in r_range]
    plt.xticks(np.arange(len(r_range)), r_str, fontsize=font_size)
    plt.xlabel('Equilibrium point number', fontsize=font_size)
    plt.ylabel('Most positive eigenvalue', fontsize=font_size)
    file_name = 'Eigenvalues'
    plt.savefig(file_name, dpi=600)
    plt.show()
"""


def test_eig_lin_KS():
    eigenval0 = np.round(np.random.random((1, 2)) * 20 - 10)
    T = np.round(np.random.random((2, 2)) * 10 + 1)
    B = np.diagflat(eigenval0)
    print('Eigenvalues:')
    print(eigenval0)
    print('Diagonal matrix:')
    print(B)
    print('Eigenvectors matrix:')
    print(T)
    A = np.dot(np.dot(T, B), np.linalg.inv(T))

    val, vec, pos_val, pos_vec = eig_lin_KS(A)
    # pos_vec = np.array(pos_vec)
    eigenval, eigenvec = np.linalg.eig(B)
    print('Calculated eigenvalues:')
    print(val)
    print('Calculated eigenvectors:')
    print(vec)
    print('Positive eigenvalues:')
    print(pos_val)
    print('Positive eigenvalues eigenvectors:')
    print(pos_vec)
    print('First eigenval and eigenvector')
    print(val[0], ' ', vec[:, 0])
    print('  A*v = lambda*v')
    print(' ', np.dot(A, np.transpose(vec[:, 0])), '=',
          np.dot(val[0], vec[:, 0]))
    if len(pos_val) > 0:
        print('First positive eigenval and eigenvector')
        print(pos_val[0], ' ', pos_vec[0])
        print('  A*v = lambda*v')
        print(' ', np.dot(A, np.transpose(pos_vec[0])), '=',
              np.dot(pos_val[0], pos_vec[0]))


def test_eig_CS():
    eigenvalues = np.array([2 + 1j, 2 - 1j, 5, 0])
    V = np.array([[0, 0, 1 + 1j, 1],
                  [0, 0, 1 - 1j, 1],
                  [0, 1, 0, 0],
                  [1, 0, 0, 0]]).T
    A_diag = np.diagflat(eigenvalues)
    A = np.dot(np.dot(V, A_diag), np.linalg.inv(V))
    # A = np.array([[0, 0, 0], [0, 0.8, -0.8], [0, 0.4, -1]])
    a_eq = np.zeros((A.shape[0],))

    eig, pos_eig = eig_lin_KS(A)
    pos_vec = pos_eig['vec']

    real_val, real_vec = real_eig(pos_eig['val'], pos_eig['vec'])
    V, V_d, V_xy = eig_CS(real_vec)

    na = 5
    delta = 5e-2
    dsim = 2
    t = np.linspace(0, 3, num=301)
    dt = t[1] - t[0]

    X = np.linspace(-na * delta, na * delta, 2 * na + 1)
    Y = np.linspace(-na * delta, na * delta, 2 * na + 1)
    XX, YY = np.meshgrid(X, Y)
    XXv, YYv = eig_mesh(XX, YY, V_xy)

    dadt_l_v = np.zeros((len(XX[:, 0]), len(XX[0, :]), 2))
    lin_d_sim = []

    AA, DD = a0_eig(a_eq, XX, YY, V)

    for idx1 in range(len(AA[:, 0])):
        for idx2 in range(len(AA[0, :])):
            a = AA[idx1, idx2, :]
            dadt_l = np.dot(A, a)
            # print(' dadt = ', dadt_l)
            # print(dadt_l.shape)

            dadt_l_v[idx1, idx2, :] = np.dot(V_d, dadt_l).flatten()

    # for sim in range(nsim):
    for idx1 in range(0, len(XX[:, 0]), dsim):
        for idx2 in range(0, len(XX[0, :]), dsim):

            a = np.zeros((len(t), len(a_eq)))
            a_v = np.zeros((len(t), 2))
            a[0, :] = DD[idx1, idx2, :]
            a_v[0, :] = np.dot(V_d, a[0, :])
            for idx_t in range(1, len(t)):
                da = np.dot(A, a[idx_t - 1, :])
                a[idx_t, :] = da * dt + a[idx_t - 1, :]
                a_v[idx_t, :] = np.dot(V_d, a[idx_t, :])

            lin_d_sim.append(a_v)

    # Plotting figures
    plot_eig_field(XXv, YYv, dadt_l_v, dadt_l_v, lin_d_sim, lin_d_sim,
                   pos_vec, V_d)


def test_sim_disturbance():
    from equilibria_KS import load_equilibria
    from nonlin_KS import plot_coeff_KS, plot_sol_KS
    from time import time

    v = (2. * np.pi / 39.) ** 2.
    N = 64
    method = 1
    tol = 1e-10
    param = {'v': v, 'N': N, 'method': method}
    t = np.linspace(0, 1, num=101)
    x = np.linspace(0, 2 * np.pi, num=N, endpoint=False)
    roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
    roots = load_equilibria(roots_name)
    a_eq = roots[4]
    epsilon = 1e-1

    A = lin_KS(a_eq, v)
    eigval, eigvec, pos_eigval, pos_eigvec = eig_lin_KS(A)
    val, vec = complex2real(pos_eigval[0], pos_eigvec[0])
    a0 = a_eq + epsilon * vec
    u0 = irfft_norm(a0 * 1j, N)
    a0alt = np.imag(rfft_norm(u0, N))

    t_0 = time()
    a_t_k, u = sim_nonlin_KS(x, t, u0, param)
    t_end = time()
    plot_sol_KS(u, x, t)
    plot_coeff_KS(a_t_k, x, t)
    print()
    print("Total time = %.2f s" % (t_end - t_0))
    print('Eigenvalue: ', val)
    print('Eigenvector size: ', vec.size)
    print('Eigenvector module: ', np.linalg.norm(vec))
    for k in range(8):
        print(k, ': ', a_eq[k], '\t', vec[k], '\t', a0[k], '\t', a0alt[k])
