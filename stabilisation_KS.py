# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 18:41:28 2017

@author: Gengiro
"""

import numpy as np
import shelve


def create_stabilisation(filename, a0, rT, sim_T, dt):
    from simulations import exists_simulation
    # Creates a new file with the simulation conditions
    if exists_simulation(filename):
        ans = input('File already exists, overwrite it? (Y/N) ')
        if ans != 'Y':
            print('File not saved')
            return
    fold = 'Simulations' + '/'
    d = shelve.open(fold + filename, 'n')
    d['a0'] = a0
    d['rT'] = rT
    d['sim_T'] = sim_T
    d['dt'] = dt
    print('File created: %s' % (fold + filename))
    d.close()


def load_stabilisation(filename, a_eq):
    from lin_KS import disturbance
    from simulations import exists_simulation
    # Loads the simulations used in stabilisation analysis
    if not exists_simulation(filename):
        error_msg = 'File ' + filename + ' not found, impossible to load'
        raise ValueError(error_msg)

    fold = 'Simulations' + '/'
    d = shelve.open(fold + filename)
    a0 = d['a0']
    rT = d['rT']
    sim_T = d['sim_T']
    dt = d['dt']
    d.close()
    print()
    print('Simulations loaded from file: ' + filename)
    print('Number of simualtions loaded: %d' % (len(rT)))
    print()
    r0 = np.zeros_like(rT)
    for k in range(len(rT)):
        d0, r0[k] = disturbance(a0[k, :], a_eq)  # Not working
    return r0, rT, a0, sim_T, dt


def random_disturbance(N, r, m=None, vec=None):
    M = N // 2 + 1
    if m is None:
        m = M
    if m > N:
        m = M
    if vec is None:
        d = np.zeros((M,))
        d[1:(m)] = np.random.random((m - 1)) - .5
        d_norm = d / np.linalg.norm(d)
    else:
        d = vec.copy()
        d[1:(m)] += (np.random.random((m - 1)) - .5) * 5e-2
        d_norm = d / np.linalg.norm(d)
        error = np.sqrt(np.square(vec - d_norm).sum())
        print('  vec difference = %.3e' % (error))
    return d_norm * r


def u_init(d, a_eq, N):
    from rfft_norm import irfft_norm
    a = a_eq + d
    return irfft_norm(a * 1j, N)


def eval_sim_KS(a, a_eq):
    # Indicate if the system has been stabilised or not
    from lin_KS import disturbance
    d, r = disturbance(a, a_eq)
    r_end = r[-1]  # Check what happens with inf values
    r_ini = r[0]
    if r_end > 1.5 * r_ini:
        grows = True
        min_r = np.min(r)
    else:
        grows = False
        min_r = 9e9
    return grows, r_end, min_r


def edge_detection(r0, rT):
    tol = 3.5
    p_range = np.linspace(1/len(r0), 100 - 1/len(r0), num=len(r0))
    p_range = np.linspace(5, 95, num=20)
    avg_top = np.zeros_like(p_range)
    avg_bottom = np.zeros_like(p_range)
    for idx in range(len(p_range)):
        p = p_range[idx]
        avg_top[idx] = np.average([rT[k] for k in range(len(rT))
                                  if r0[k] > np.percentile(r0, p)])
        avg_bottom[idx] = np.average([rT[k] for k in range(len(rT))
                                     if r0[k] < np.percentile(r0, p)])
    max_idx = np.argmax(np.log10(avg_top) - np.log10(avg_bottom))
    top_height = avg_top[max_idx]
    bottom_height = avg_bottom[max_idx]
    mean_height = 10 ** (0.5 * (np.log10(top_height) +
                                np.log10(bottom_height)))
    min_idx = np.argmax(r0)
    for r_idx in range(len(r0)):
        if rT[r_idx] > mean_height and r0[min_idx] > r0[r_idx]:
            min_idx = r_idx
    height_diff = np.log10(top_height) - np.log10(bottom_height)
    success = height_diff > tol
    if np.max(rT) < 1e-6:
        min_idx = np.argmax(rT)
        success = True
    r0_max = r0[min_idx]
    rT_max = rT[min_idx]
    return r0_max, rT_max, success


def plot_stabilisation_range(r0, rT, eq, method='search', save=False):
    import matplotlib.pyplot as plt
    font_size = 13
    p = 0

    r0_max, rT_max, success = edge_detection(r0, rT)
    plt.figure(figsize=(10, 4))
    if method in ['search', 'E_dot', 'min_d']:
        plt.semilogy(r0, rT, 'b.')
    elif method == 'naive':
        plt.loglog(r0, rT, 'b.')
    else:
        raise ValueError('Not available method, try search, naive or E_dot')
    ax = np.array(plt.axis())
    ax[0] = np.percentile(r0, p)
    ax[1] = np.percentile(r0, 100 - p)
    ax[0] -= 0.2 * (ax[1] - ax[0])
    if success:
        if method in ['search', 'E_dot', 'min_d']:
            plt.semilogy([r0_max, r0_max], ax[2:4], 'r--', linewidth=0.7)
        elif method == 'naive':
            plt.loglog([r0_max, r0_max], ax[2:4], 'r--', linewidth=0.7)
    plt.axis(tuple(ax))
    plt.title('Stabilisation range of the equilibria %d' % (eq),
              fontsize=font_size)
    plt.xlabel(r'$r(0) = \|\|\delta(0)\|\|$', fontsize=font_size)
    plt.ylabel(r'$r(T) = \|\|\delta(T)\|\|$', fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)

    if save is not False:
        file_name = 'Results/fig_' + save
        plt.savefig(file_name, dpi=600)
    plt.show()
    return r0_max, rT_max


def find_rmax_idx(R0, r_max):
    idx = np.argmin(np.abs(R0 - r_max))
    error = np.abs(R0[idx] - r_max)
#    print('r_max difference = %.3e' % (error))
    if error > 1e-6:
        assert(0)
    return idx


def plot_stabilisation_traj(A0, a_eq, param, eq, BK, r_max, t, cf=(1, 3),
                            a=None):
    import matplotlib.pyplot as plt
    from equilibria_KS import plot_circle
    from nonlin_KS import sim_CL_nonlin_KS
    from rfft_norm import irfft_norm
    font_size = 13
    dtxt = 0.03
    c_sim = [
             # '#00009bff',  # dark blue
             '#0062ffff',  # blue
             # '#00d4ffff',  # light blue
             # '#f0ff0eff',  # yellow
             # '#f00000ff',  # red
             '#ff9900',  # orange
             '#66ff66',  # green
             ]
    x = np.linspace(0, 2 * np.pi, param['N'], endpoint=False)
    ax_k = 0
#    max_ax = 0
    ax_lst = []

    fig = plt.figure(figsize=(5 * cf[1], 5 * cf[0]))
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        y_str = r'$a_{' + str(even_cf) + '}$'
        x_str = r'$a_{' + str(odd_cf) + '}$'
        plt.ylabel(y_str, fontsize=font_size)
        plt.xlabel(x_str, fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.axis('equal')
        if a is not None:
            plt.plot(a[:, odd_cf], a[:, even_cf], '#A9A9A9')  # '#00cc00')
        plot_circle(a_eq[odd_cf], a_eq[even_cf], r_max,
                    color='r--', linewidth=0.7)
        ax_lst.append(np.array(plt.axis()))
    for k in range(A0.shape[0]):
        u0 = irfft_norm(A0[k, :] * 1j, param['N'])
        a_k, u_k = sim_CL_nonlin_KS(x, t, u0, param, BK, a_eq)
        for p in range(cf[0] * cf[1]):
            plt.subplot(cf[0], cf[1], p + 1)
            odd_cf = 2 * p + 1
            even_cf = 2 * p + 2
            plt.plot(a_k[:, odd_cf], a_k[:, even_cf], color=c_sim[k])

            if a_k[0, odd_cf] >= 0:
                h_a = 'left'
                dx = dtxt
            else:
                h_a = 'right'
                dx = -dtxt
            if a_k[0, even_cf] >= 0:
                v_a = 'bottom'
                dy = dtxt
            else:
                v_a = 'top'
                dy = -dtxt
            plt.text(a_k[0, odd_cf] + dx, a_k[0, even_cf] + dy, str(k + 1),
                     color='k', fontsize=font_size, ha=h_a, va=v_a)
            plt.plot(a_k[0, odd_cf], a_k[0, even_cf], 'x', color=c_sim[k],
                     label='T = 0')
#            plt.plot(a_k[int(2 / dt), odd_cf], a_k[int(2 / dt), even_cf], 'x',
#                     color=c_sim[k],
#                     label='T = 2')
#            plt.plot(a_k[int(10 / dt), odd_cf], a_k[int(10 / dt), even_cf],
#                     '^', color=c_sim[k] label='T = 10')
#            plt.plot(a_k[-1, odd_cf], a_k[-1, even_cf], 's',
#                     color=c_sim[k],
#                     label='T = 25')

    plt.tight_layout()
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        # plt.grid()
        # plt.axis('equal')
        ax = ax_lst[p]
        ax[0] -= ax_k * (ax[1] - ax[0])
        ax[1] += ax_k * (ax[1] - ax[0])
        ax[2] -= ax_k * (ax[3] - ax[2])
        ax[3] += ax_k * (ax[3] - ax[2])
#        plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.7)
#        plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.7)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
        # ax = [-max_ax, max_ax, -max_ax, max_ax]
#        plt.axis(tuple(ax))
    artist = []
    label = []
    for k in range(A0.shape[0]):
        artist.append(plt.Line2D((.5, .5), (0, 0), color=c_sim[k],
                                 marker='', linestyle='-'))
        label.append('sim ' + str(k + 1))
    fig.legend(artist, label, loc='upper center', ncol=A0.shape[0],
               fontsize=font_size, bbox_to_anchor=(0.5, 1.05))
    plt.show()
    print()


def calculate_next_r(r0, r_max, k_r, lim):
    rand_up = 1.03
    rand_low = 0.90
    d_r = (r_max / r0) ** k_r
    print('d_r* = %.3e' % (d_r))
    d_r = np.max([np.min([d_r, 5]), 0.2])
    print('d_r = %.3e' % (d_r))
    r = r0 * d_r
    print('r* = %.3e' % (r))
    r = np.max([np.min([r, lim[1]]), lim[0]])
    r *= rand_low + np.random.random() * (rand_up - rand_low)
    print('r = %.3e' % (r))
    print()
    return r


def min_distance(sim_file, a_eq, dist=False, max_dist=False):
    from simulations import load_simulation
    from lin_KS import disturbance

    a, t = load_simulation(sim_file)
    d, r = disturbance(a, a_eq)

    min_r_idx = np.argmin(r)
    min_d = d[min_r_idx, :]
    min_d_mag = np.linalg.norm(min_d)
    min_d_norm = min_d / min_d_mag

    max_r_idx = np.argmax(r)
    max_d = d[max_r_idx, :]
    max_d_mag = np.linalg.norm(max_d)
    max_d_norm = max_d / max_d_mag

    if dist:
        if max_dist:
            return min_d_norm, min_d_mag, max_d_norm, max_d_mag
        else:
            return min_d_norm, min_d_mag
    else:
        if max_dist:
            return min_d_norm, max_d_norm
        else:
            return min_d_norm


def stabilisation_range(a_eq, param, eq, BK, r0, t, n_sim=5, N_sim=100,
                        vec=None, method='search'):
    from nonlin_KS import sim_CL_nonlin_KS
    from lin_KS import lin_KS
    # from nonlin_KS import plot_coeff_KS, plot_sol_KS
    from simulations import estimated_time
    from time import time
    modes = 10
    r_max = 4.5
    sim_file = 'sim_N' + str(param['N']) + '_method1_nonlin_OL'

    if method in ['search', 'E_dot', 'min_d']:
        k_r = 1.25
        lim = [1e-7, 5]
        r = r0
        if method == 'search':
            vec = None
        elif method == 'E_dot':
            A = lin_KS(a_eq, param['v'])
            vec = max_energy_gradient(A - BK)
        elif method == 'min_d':
            vec = min_distance(sim_file, a_eq)
    elif method == 'naive':
        r_logspace = np.logspace(-6, 0.4, num=N_sim)
        vec = None
    else:
        raise ValueError('Not valid method, try search, naive, E_dot or min_d')

    N = param['N']
    x = np.linspace(0, 2 * np.pi, N, endpoint=False)
    a0_lst = np.zeros((N_sim, len(a_eq)))
    r0_x = np.zeros((N_sim,))
    rT_y = np.zeros((N_sim,))
    rT_n = np.zeros((n_sim,))
    a0_n = np.zeros((n_sim, len(a_eq)))

    print('STABILISATION RANGE OF THE EQUILIBRIA %d' % (eq))
    print()
    t0 = time()
    for k in range(N_sim):
        t_end = time()
        if method == 'naive':
            r = r_logspace[k]
        for n in range(n_sim):
            print('Equilibria num %d (T = %d, method %d, %s)'
                  % (eq, np.round(t[-1]), param['method'], method))
            print('Iteration %d out of %d (%.1f%%)' % (k, N_sim,
                  (k) * 100 / N_sim))
            print(estimated_time(k / N_sim, t_end - t0))
            print('Simulation %d out of %d' % (n + 1, n_sim))
            print('r0 = %.3e' % (r))
            d0 = random_disturbance(N, r, m=modes, vec=vec)
            u0 = u_init(d0, a_eq, N)
            a, u = sim_CL_nonlin_KS(x, t, u0, param, BK, a_eq)
            a0_n[n, :] = a[0, :]
            # plot_sol_KS(u, x, t)
            # plot_coeff_KS(a, x, t)
            grows, r_end, min_r = eval_sim_KS(a, a_eq)
            r_max = np.min([r_max, min_r])
            rT_n[n] = r_end
            print('rT = %.3e' % (r_end))
            if grows:
                print('Closed-loop system UNSTABLE!')
                # plot_coeff_KS(a, x, t)
                print('r_max = %.3e' % (r_max))
                print()
                break
            else:
                print('Closed-loop system STABLE!')
            print('r_max = %.3e' % (r_max))
            print()

        max_idx = np.argmax(rT_n)
        a0_lst[k, :] = a0_n[max_idx, :]
        rT_y[k] = rT_n[max_idx]
        r0_x[k] = r
        # plot_stabilisation_range(r0_x[:(k + 1)], rT_y[:(k + 1)], eq)
        if method in ['search', 'E_dot', 'min_d']:
            r = calculate_next_r(r, r_max, k_r, lim)
    print('Simulation ended!')
    print('Current stabilisation ball r = %.3e' % (r_max))
    print()
    return r0_x, rT_y, a0_lst


def energy_gradient(A, d):
    # d_arr = np.array(d)
    return 0.5 * np.dot(d.T, np.dot(A + A.T, d))


def max_energy_gradient(A):
    from lin_KS import eig_lin_KS
    A_tilde = A + A.T
    eig, pos_eig = eig_lin_KS(A_tilde, plot=False)
    # val = np.real(pos_eig['val'][-1])
    vec = np.real(pos_eig['vec'][-1])
    # print('Max eigenvalue = %.3f' % (val))
    return vec


def plot_energy_stability_check(A_tilde, N, val, vec, n=1000, save=False):
    import matplotlib.pyplot as plt
    font_size = 13
    m = 11
    # plot a comparative graph with the energy gradient for every
    # eigenvector of the matrix A_tilde

    if n != 0:
        plt.figure(figsize=(10, 10))
        plt.subplot(2, 1, 1)
    else:
        plt.figure(figsize=(10, 5))
    Edot = np.zeros_like(val)
    for l in range(len(val) - 1, -1, -1):
        Edot[l] = energy_gradient(A_tilde, vec[l])
        plt.plot([0., len(val) - 1], [val[l], val[l]], '--',
                 label=r'$\lambda$ = ' + ('%.2f' % (val[l])), linewidth=0.7)
    plt.plot(Edot, 'bx')
    plt.title('Energy gradient in the unstable directions', fontsize=font_size)
    plt.xlabel('Eigenvector index', fontsize=font_size)
    plt.ylabel(r'$\dot{E}$', fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.legend(loc='lower right')

    # plot a comparative graph with the energy gradient for different
    # initial perturbations (norm = 1) randomly generated
    if n != 0:
        Edot = np.zeros((n,))
        for k in range(n):
            rand_vec = random_disturbance(N, 1., m=m)
            Edot[k] = energy_gradient(A_tilde, rand_vec)

        plt.subplot(2, 1, 2)
        for l in range(len(val) - 1, -1, -1):
            plt.plot([0., n - 1], [val[l], val[l]], '--',
                     label=r'$\lambda$ = ' + ('%.2f' % (val[l])),
                     linewidth=0.7)
        plt.plot(Edot, 'b.')
        plt.title('Energy gradient in random directions', fontsize=font_size)
        plt.ylabel(r'$\dot{E}$', fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.legend(loc='lower right')

        plt.tight_layout()
    if save is not False:
        file_name = 'Results/fig_' + save
        plt.savefig(file_name, dpi=600)
    plt.show()
    print()


def energy_stability(a_eq, param, eq, n=int(1e3), save=False):
    from lin_KS import eig_lin_KS, lin_KS

    A = lin_KS(a_eq, param['v'])
    # print('Unstable directions of the A matrix')
    _, __ = eig_lin_KS(A, plot=False)
    A_tilde = A + A.T

    # print()
    # print('Unstable directions of the A_tilde matrix')
    eig, pos_eig = eig_lin_KS(A_tilde, plot=False)
    if save is not False:
        save += '_eq' + str(eq) + '_energy_gradient'
    plot_energy_stability_check(A_tilde, param['N'], pos_eig['val'],
                                pos_eig['vec'], n=n, save=save)
    return pos_eig


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return rho, phi


def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y


def disturbance_zoom(a_x, a_y, a_eq_x, a_eq_y):
    k = 1 / 4
    a = 1.2
    z_x = np.zeros_like(a_x)
    z_y = np.zeros_like(a_y)
    if isinstance(a_x, list):
        for k in range(len(a_x)):
            rho, phi = cart2pol(a_x[k] - a_eq_x, a_y[k] - a_eq_y)
            z_x[k], z_y[k] = pol2cart(np.log10(rho), phi)
            z_x[k], z_y[k] = pol2cart((rho * a) ** k, phi)
    else:
        rho, phi = cart2pol(a_x - a_eq_x, a_y - a_eq_y)
        z_x, z_y = pol2cart(np.log10(rho), phi)
        z_x, z_y = pol2cart((rho * a) ** k, phi)
    return z_x, z_y


def plot_stabilisation_time(A0, param, x, t, A, BK, a_eq, plot='r',
                            save=False):
    import matplotlib.pyplot as plt
    from nonlin_KS import sim_CL_nonlin_KS
    from lin_KS import disturbance
    from rfft_norm import irfft_norm
    font_size = 13
    c_sim = [
             # '#00009bff',  # dark blue
             '#0062ffff',  # blue
             # '#00d4ffff',  # light blue
             # '#f0ff0eff',  # yellow
             # '#f00000ff',  # red
             '#ff9900',  # orange
             '#66ff66',  # green
             ]

    plt.figure(figsize=(10, 5))
    for k in range(A0.shape[0]):
        u0 = irfft_norm(A0[k, :] * 1j, param['N'])
        a, u = sim_CL_nonlin_KS(x, t, u0, param, BK, a_eq)
        d, r = disturbance(a, a_eq)
        if plot == 'r':
            plt.semilogy(t, r, color=c_sim[k], label='sim '+str(k + 1))
            plt.ylabel(r'$r = \|\|\delta\|\|$', fontsize=font_size)
        elif plot == 'E':
            E = np.zeros_like(r)
            for idx_t in range(len(t)):
                E[idx_t] = energy_gradient(A - BK, d[idx_t, :])
            plt.semilogy(t, E, color=c_sim[k], label='sim '+str(k + 1))
            plt.ylabel(r'$E = \delta^T(A + A^T$', fontsize=font_size)

    plt.xlabel('time', fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.legend()
    if save is not False:
        file_name = 'Results/fig_' + save
        plt.savefig(file_name, dpi=600)
    plt.show()
    print()


def test_random_disturbance():
    N = 4
    m = 4
    r = 5
    d = random_disturbance(N, r, m=m)
    print('d =\n', d)
    if np.abs(r - np.linalg.norm(d)) > 1e-10:
        print('Radius is not being well assigned')
        print('r = %.4g')
        print('norm(d) = %.4g')
        print()


def test_disturbance_zoom():
    import matplotlib.pyplot as plt
    r_range = np.linspace(1e-5, 2, 10)
    x_0 = 0
    y_0 = 0
    a = 1.2
    k = 2
    b = 1 / 4
    phi = np.linspace(0, 2 * np.pi, 512)
    circles_x = []
    circles_y = []
    for idx in range(len(r_range)):
        r = r_range[idx]
        circles_x.append(r * np.cos(phi) + x_0)
        circles_y.append(r * np.sin(phi) + y_0)

    plt.figure(figsize=(10, 4))
    r = [np.linspace(1e-5, 2, 2000)]
    r.append(r[0])
    r.append(r[0] - (r[0] / a) ** k)
    r.append((r[0] * a) ** b)
    for idx in range(1, len(r)):
        plt.plot(r[0], r[idx])
    plt.show()

    plt.figure(figsize=(10, 5))
    plt.subplot(1, 2, 1)
    for idx in range(len(r_range)):
        plt.plot(circles_x[idx], circles_y[idx])

    plt.subplot(1, 2, 2)
    for idx in range(len(r_range)):
        x, y = disturbance_zoom(circles_x[idx], circles_y[idx], x_0, y_0)
        plt.plot(x, y)

    plt.show()
