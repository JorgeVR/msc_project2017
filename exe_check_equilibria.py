# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 19:40:07 2017

@author: Gengiro
"""

import numpy as np
from equilibria_KS import (load_equilibria, check_equilibria,
                           check_paired_equilibria, plot_equilibria)
from simulations import load_simulations
from stabilisation_KS import (load_stabilisation, plot_stabilisation_range,
                              plot_stabilisation_traj, find_rmax_idx,
                              energy_stability, plot_stabilisation_time)
from actuators_KS import load_actuators
from lqr import lqr_matrices_KS

v = (2. * np.pi / 39.) ** 2
N = 96
method = 1
tol = 1e-10
x = np.linspace(0, 2 * np.pi, N, endpoint=False)


param = {'v': v, 'N': N, 'method': method}
roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)
sim_name = 'sim_N' + str(N) + '_nonlin_OL'
a_list = load_simulations(sim_name)
a = a_list[-1]

do = 10

if do == 0:
    only = 'red_paired'
#    only = 'single'
#    only = [3, 4, 15, 16, 17, 18, 21, 22]

    # check_equilibria(roots)
    # check_paired_equilibria(roots, v)
    plot_equilibria(roots, param, only=only, a=a, save=roots_name)

elif do == 10:
    check_equilibria(roots)
    a0_name = roots_name + '_a0'
    a_name = roots_name + '_sim'
    a0 = load_equilibria(a0_name)
    a = load_simulations(a_name)
    plot_equilibria(roots, a0, a[-1], x, N)

elif do == 1:
    eq = 3
    n = int(1e4)
    pos_eig = energy_stability(roots[eq], param, eq, n=n, save=roots_name)

elif do == 2:
    search = 'unique'
    ctrl_method = 'lqr_test'
    stab_method = 'search'
    rho = 1e-1
    eq = 16
    sim_T_r = [2, 10, 25]
    sim_T_r = [25]
    actuators_name = ('actuators_N' + str(N) +
                      '_tol' + str(-int(np.log10(tol))) +
                      '_' + search + '_' + ctrl_method)
    actuators = load_actuators(actuators_name)
    a_eq = roots[eq]
    Mu = actuators[eq]['Mu']
    Sigma = actuators[eq]['Sigma']

    Q = np.diagflat(np.ones_like(a_eq))
    R = rho * np.diagflat(np.ones(len(Mu)))
    A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R)
    BK = np.dot(B, K)

    for sim_T in sim_T_r:
        stab_name = ('stabilisation_range_N' + str(N) + '_eq' + str(eq) +
                     '_' + stab_method + '_T' + str(sim_T))
        r0, rT, a0, sim_T, dt = load_stabilisation(stab_name, a_eq)
        r_stab, rT_max = plot_stabilisation_range(r0, rT, eq,
                                                  method=stab_method)
        print('r_max = %.3e' % (r_stab))

    t = np.arange(0, sim_T, dt)
    idx = find_rmax_idx(r0, r_stab)
#    idx = 250
    a0_plot = a0[idx - 1: idx + 2, :]
    plot_stabilisation_traj(a0_plot, a_eq, param, eq, BK,
                            r_stab, t, a=a)
    plot_stabilisation_time(a0_plot, param, x, t, A, BK, a_eq)
