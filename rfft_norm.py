# -*- coding: utf-8 -*-
"""
My FFT functions, to standarise the normalisation
Created on Wed Mar  1 23:28:50 2017

@author: Jorge Vidal-Ribas
University of Southampton
"""
import numpy as np


def rfft_norm(a, N):
    return np.fft.rfft(a) / N


def irfft_norm(a, N):
    return np.fft.irfft(a * N)


def interpolate(x_new, x, f):
    from scipy.interpolate import splrep, splev
    tck = splrep(x, f, s=0)
    return splev(x_new, tck, der=0)


def a2u(a, N):
    if a.ndim == 1:
        u = irfft_norm(a * 1j, N)
    elif a.ndim == 2:
        u = np.zeros((len(a[:, 0]), N))
        for t_idx in range(len(a[:, 0])):
            u[t_idx, :] = irfft_norm(a[t_idx, :] * 1j, N)
    return u


def my_rfft(a, N):
    M = int(N / 2) + 1
    output = np.zeros(M, dtype=complex)
    for k in range(M):
        output[k] = (a * np.exp(-2.0 * np.pi * 1j * k * np.arange(N)/N)).sum()
    return output


# Example of an equivalent of the fft.rfft function (same output values)
def my_rfft2(a):
    N = a.shape[-1]
    M = int(N / 2) + 1
    output = np.zeros(M, dtype=complex)
    for k in range(M):
        output[k] = (a * np.exp(-2.0 * np.pi * 1j * k * np.arange(N)/N)).sum()
    return output


def test_rfft_norm():
    import matplotlib.pyplot as plt
    # N always even!
    N = 8 * 1024
    N = 48
    T = 2 * np.pi / N
    x = np.linspace(0., N * T, N, endpoint=False)
#    x = np.linspace(0., 2 * np.pi, num=N)
    A = (np.random.rand() * 10 + 1)
    A0 = (np.random.rand() * 8 + 1)
    A0 = 0.
    f = int(np.random.rand() * 7 + 1)

    y = A * np.sin(f * x) + A0

    fourier_norm = rfft_norm(y, N)
    freq = np.fft.rfftfreq(N, T) * 2 * np.pi

#    error0 = (np.abs(fourier_norm[0]) - A0) / A0
    errorA = (np.abs(fourier_norm[f]) - 0.5 * A) / A

    for i in range(10):
        if i == 0:
            a_th = A0
            b_th = 0
        elif i == f:
            # is the next statement right?
            a_th = A / 2.0
            b_th = - A / 2.0
        else:
            a_th = 0
            b_th = 0
        print("freq = %d \t fft[%d] = (%.3f, %.3fj)" % (freq[i], i, a_th,
              b_th))
        print("\t\trfft[%d] = (%.3f, %.3fj)" % (i, np.real(fourier_norm[i]),
              np.imag(fourier_norm[i])))
        print()

    print()
    print("Frequency = %d" % (f))
    print("Amplitude = %f,  error = %.3g%%" % (A, errorA * 100))
#    print("Constant  = %f,  error = %.3g%%" % (A0, error0 * 100))

    y_new = irfft_norm(np.imag(fourier_norm) * 1j, N)
    error = np.square(y - y_new).sum() / (0.5 * (A + A0))
    max_err = max(np.abs(y - y_new)) / (0.5 * (A + A0))

    rfourier = np.fft.rfft(y, norm=None)
    y_dir = np.fft.irfft(rfourier, norm=None, n=N)
    error_dir = np.square(y - y_dir).sum() / (0.5 * (A + A0))
    max_err_dir = max(np.abs(y - y_dir)) / (0.5 * (A + A0))

    print("Max absolute error   (norm) = %.3g%%" % (max_err * 100))
    print("Max absolute error   (dir)  = %.3g%%" % (max_err_dir * 100))
    print("Sum of squares error (norm) = %.3g%%" % (error * 100))
    print("Sum of squares error (dir)  = %.3g%%" % (error_dir * 100))

    plt.figure(figsize=(10, 10))
    plt.subplot(3, 1, 1)
    plt.plot(x, A0 * np.ones_like(x), 'k--')
    plt.plot(x, y, '.', label="Original")
    plt.plot(x, y_new, label="New")
    plt.plot(x, y_dir, label="Direct")
    plt.grid()
    plt.legend()
    plt.ylabel('Amplitude')
    plt.xlabel("x [rad]")
    plt.xticks(np.pi / 6. * np.array(range(13)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$', r'$3\pi/2$',
                r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'])

    plt.subplot(3, 1, 3)
    plt.stem(freq[:10], np.real(fourier_norm)[:10], label="Real")
    plt.stem(freq[:10], np.imag(fourier_norm)[:10], 'g', label="Imag")
    plt.grid()
    plt.legend()
    plt.ylabel('Amplitude')
    plt.xlabel('Frequency [Hz]')

    plt.subplot(3, 1, 2)
    plt.plot(x, y - y_new, label="New")
    plt.plot(x, y - y_dir, label="Direct")
    plt.ylabel('Error')
    plt.xlabel("x [rad]")
    plt.xticks(np.pi / 6. * np.array(range(13)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$', r'$3\pi/2$',
                r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'])
    plt.legend()
    plt.tight_layout()
    plt.show()
