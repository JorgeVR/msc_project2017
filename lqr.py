# -*- coding: utf-8 -*-
"""
Created on Sat Aug  5 19:43:08 2017

@author: Gengiro
"""

import numpy as np


def lqr_cont_inf(A, B, Q, R):
    from scipy.linalg import solve_continuous_are
    if isinstance(R, np.ndarray):
        R_inv = np.linalg.inv(R)
    else:
        R_inv = 1 / R
    # RN = np.dot(R_inv, N.T)
    A_tilde = A  # - np.dot(B, RN)
    Q_tilde = Q  # - np.dot(N, RN)
    X = solve_continuous_are(A_tilde, B, Q_tilde, R)
    K = np.dot(R_inv, np.dot(B.T, X))  # + N.T)
    e, v = np.linalg.eig(A - np.dot(B, K))
    return K, e


def lqr_matrices_KS(a_eq, param, x, Mu, Sigma, Q, R):
    from lin_KS import lin_KS
    from force_KS import B_KS
    A = lin_KS(a_eq, param['v'])
    B, F, f = B_KS(x, Mu, Sigma, param['N'])
    K, e = lqr_KS(A, B, Q, R)
    return A, B, F, K, e


def lqr_KS(A, B, Q, R):
    from controllability_KS import cmplt2red, K_red2cmplt
    A_red, B_red = cmplt2red(A, B)
    Q_red, _ = cmplt2red(Q, B)
    K_red, e_red = lqr_cont_inf(A_red, B_red, Q_red, R)
    K = K_red2cmplt(K_red)
    return K, e_red


def sim_actuators(x, K, idx=0):
    u = np.zeros((x.shape[0], K.shape[0]))
    for t in range(idx, x.shape[0]):
        u[t, :] = - np.dot(K, x[t, :])
    return u


def lsim(x0, t, A):
    from scipy.integrate import odeint
    x = odeint(lambda x, t: np.dot(A, np.array(x).T), x0, t)
    return x


def plot_lsim(x, t, u=None, only_u=False):
    import matplotlib.pyplot as plt
    font_size = 13
    l = len(x[0, :])
    if u is not None:
        b_u = 1
    else:
        b_u = 0
    if only_u:
        plt.figure(figsize=(10, 4))
        for k in range(u.shape[1]):
            plt.plot(t, u[:, k], label='Actuator ' + str(k + 1))
        plt.xlabel('time', fontsize=font_size)
        plt.ylabel('u', fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.legend()
        plt.grid()
    else:
        plt.figure(figsize=(10, (l + b_u) * 4))
        for k in range(l):
            plt.subplot(l + b_u, 1, k + 1)
            plt.plot(t, x[:, k])
            plt.ylabel(r'$x_' + str(k + 1) + '$', fontsize=font_size)
            plt.xlabel('time', fontsize=font_size)
            plt.xticks(fontsize=font_size)
            plt.yticks(fontsize=font_size)
            plt.grid()
        if u is not None:
            plt.subplot(l + 1, 1, l + 1)
            for k in range(u.shape[1]):
                plt.plot(t, u[:, k], label='Actuator ' + str(k + 1))
            plt.xlabel('time', fontsize=font_size)
            plt.ylabel('u', fontsize=font_size)
            plt.xticks(fontsize=font_size)
            plt.yticks(fontsize=font_size)
            plt.legend()
            plt.grid()
        plt.tight_layout()
    plt.show()


def test_lqr():
    M = 0.5
    m = 0.2
    b = 0.1
    I = 0.006
    g = 9.8
    l = 0.3
    x0 = [0.1, 0, 0.1, 0]
    t = np.arange(0, 5, 0.01)

    p = I * (M + m) + M * m * l ** 2

    A = np.array([[0, 1, 0, 0],
                  [0, -(I + m * l ** 2) * b / p, (m ** 2 * g * l ** 2)/p, 0],
                  [0, 0, 0, 1],
                  [0, -(m * l * b) / p, m * g * l * (M + m) / p, 0]])
    B = np.array([[0],
                  [(I + m * l ** 2) / p],
                  [0],
                  [m * l / p]])
    print('A = \n', A)
    print()
    print('B = \n', B)
    print()

    Q = np.diagflat([1, 0, 1, 0])
    R = 1
    K, e = lqr_cont_inf(A, B, Q, R)

    print('K =\n', K)
    print()
    print('Should be K = -1.0000   -1.6567   18.6854    3.4594\n')
    print('e =\n', e)
    print()

    x = lsim(x0, t, A - np.dot(B, K))
    u = sim_actuators(x, K)
    plot_lsim(x, t, u=u)
