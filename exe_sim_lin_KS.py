# -*- coding: utf-8 -*-
"""
Solving Kuramoto-Sivashinsky equation: u_t = (u^2)_x - v * u_xxxx - u_xx + f
Created on Mon Apr  3 08:35:24 2017

@author: Gengiro
"""
import numpy as np
from lin_KS import sim_lin_KS
from nonlin_KS import plot_sol_KS, plot_coeff_KS
from rfft_norm import irfft_norm
from equilibria_KS import load_equilibria
from simulations import save_simulation
from time import time

v = (2. * np.pi / 39.) ** 2.
N = 96
method = 1
tol = 1e-10
param = {'v': v, 'N': N, 'method': method}
t = np.linspace(0, 1.2, num=101)
x = np.linspace(0, 2 * np.pi, N, endpoint=False)
eq = 2

roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)
a_eq = roots[eq]
u0 = irfft_norm(a_eq * 1j, N)
u0[1:(x.size-1)] += (np.random.random_sample(x.size-2) - 0.5) * 1e-5

t_0 = time()
a_t_k, u = sim_lin_KS(x, t, u0, a_eq, param)
t_end = time()
plot_sol_KS(u, x, t)
plot_coeff_KS(a_t_k, x, t)
print()
print("Total time = %.2f s" % (t_end - t_0))
ans = input('Do you want to save the simulation? (Y/N) ')
if ans == 'Y':
    print()
    sim_filename = 'sim_N' + str(N) + '_lin_OL_eq' + str(eq)
    save_simulation(sim_filename, a_t_k)
    print()
else:
    print()
    print('Simulation not saved')
    print()
