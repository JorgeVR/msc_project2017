# README #

MSc Project

Stabilisation of a chaotic 1-D PDE with linear feedback control  
By Jorge Vidal-Ribas  
University of Southampton  
Faculty of Engineering and Environment  
September 2017  

All the files and data of the project are included in this repository.  
Python version 3.5.2  
Packages used Numpy 2.0.0, Scipy 0.19.0 and Matplotlib 2.0.0

Contact  
Jorge Vidal-Ribas  
jvr1g16@soton.ac.uk  