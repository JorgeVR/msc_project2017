# -*- coding: utf-8 -*-
"""
Created on Sun May  7 22:51:28 2017

@author: Gengiro
"""
import shelve


def create_equilibria(filename, roots):
    # Creates a new file with the roots given
    fold = 'Equilibria' + '/'
    d = shelve.open(fold + filename, 'n')
    d['roots'] = roots
    print('File created (%s) including %d roots' %
          (fold + filename, len(roots)))
    d.close()


def load_equilibria(filename):
    # Loads the equilibria from the specific filename

    fold = 'Equilibria' + '/'

    d = shelve.open(fold + filename)
    if 'roots' in d:
        roots = d['roots']
    else:
        klist = d.keys()
        print()
        print('Error trying to load the roots, available keys:')
        print()
        for key in klist:
            print(key)
        d.close()
        assert()
    d.close()
    print()
    print('Roots loaded from file: ' + filename)
    print('Number of roots loaded: %d' % (len(roots)))
    print()
    return roots


def save_equilibria(filename, roots):
    # Opens the roots file and adds the possible new roots
    import numpy as np
    from pathlib import Path

    fold = 'Equilibria' + '/'
    file = Path(fold + filename + '.dat')
    try:
        _ = file.resolve()
    except FileNotFoundError:
        print('File not found, creating a new one\n')
        create_equilibria(filename, roots)
        return roots
    n_not_new = 0
    n_new = 0
    d = shelve.open(fold + filename)
    original_roots = d['roots']
    print()
    print('Original number of roots: %d' % (len(original_roots)))
    print('New available roots: %d' % (len(roots)))
    print()
    for r in roots:
        for r_original in original_roots:
            if np.square(r - r_original).sum() < 1e-8:
                n_not_new += 1
                break
        else:
            # If the root is new, add it to roots
            original_roots.append(r)
            n_new += 1

    d['roots'] = original_roots
    d.close()
    print('Roots already included: %d' % (n_not_new))
    print('New roots added: %d' % (n_new))
    print('New number of roots: %d' % len(original_roots))
    return original_roots


def search_equilibria(a0, t, roots, tol, param):
    import numpy as np
    from scipy.optimize.nonlin import nonlin_solve
    from nonlin_KS import nonlin_KS_ode

    method = param['method']
    N = param['N']
    v = param['v']

    # a_eq = newton_krylov(lambda a: KS_ode(a, t, method, N, v), a0,
    #                     method='lgmres')  # , maxiter=2000)
    a_eq, out_eq = nonlin_solve(lambda a: nonlin_KS_ode(a, t, method, N, v),
                                a0, jacobian='krylov', full_output=True,
                                raise_exception=False, f_tol=tol)
    # Check if the root is valid F(root) = square(root).sum < tolerance
    if out_eq['success']:
        print('Successful search!')
        print('Valid root found: ', a_eq)
        # Compare the found root with the previous roots
        for r in roots:
            if np.sqrt(np.square(r - a_eq).sum()) < 1e-8:
                print('This root was already found')
                break
        else:
            # If the root is new, add it to roots
            print('This root is new! Added')
            roots.append(a_eq)
            a_eq_odd = [a_eq[k] for k in range(1, len(a_eq), 2)]
            if np.square(a_eq).sum() < 1e-8:
                print('Trivial solution, no asymmetric root.')
            elif np.square(a_eq_odd).sum() < 1e-8:
                print('Solution with null odd coeffs, no asymmetric root.')
            else:
                for k in range(1, a_eq.size, 2):
                    a_eq[k] *= -1
                print('Checking asymmetric root ...')
                a_eq, out_eq = nonlin_solve(lambda a:
                                            nonlin_KS_ode(a, t, method, N, v),
                                            a0, jacobian='krylov',
                                            full_output=True,
                                            raise_exception=False, f_tol=tol)
                if out_eq['success']:
                    for r in roots:
                        if np.square(r - a_eq).sum() < 1e-8:
                            print('Asymmetric root already found!')
                            raise ValueError("Can this be possible?")
                            break
                    else:
                        print('Asymmetric root found, added too')
                        roots.append(a_eq)
                else:
                    print('No convergence. Next search!')
    else:
        print('No convergence. Next search!')
    print('Current number of roots: %d' % (len(roots)))
    print()
    return roots


"""def plot_equilibria(roots, a_ini, a_t_k, x, N):
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.interpolate import splrep, splev
    from rfft_norm import irfft_norm
    font_size = 13
    cf = (2, 2)

    r = (x.size // 2) + 1
    x_r = x[:r]
    x_interp = np.linspace(0, np.pi, num=480)
    u_r = np.zeros((x_r.size, len(roots)))
    for k in range(len(roots)):
        u = irfft_norm(roots[k] * 1j, N)
        u_r[:, k] = u[:r]

    if a_ini == []:
        b_a_0 = False
    else:
        b_a_0 = True
    if a_t_k == []:
        b_a = False
    else:
        b_a = True
    # Print all the roots and plot the related solution u
    t_cf = 2 * cf[0] * cf[1] + 1
    a_r = np.zeros((len(roots), t_cf))
    if b_a_0:
        a_0 = np.zeros((len(a_ini), t_cf))
    if b_a:
        a = np.zeros((len(a_t_k[:, 0]), t_cf))
    for k in range(t_cf):
        a_r[:, k] = [roots[r][k] for r in range(len(roots))]
        if b_a_0:
            a_0[:, k] = [a_ini[r][k] for r in range(len(a_ini))]
        if b_a:
            a[:, k] = a_t_k[:, k]

    plt.figure(1, figsize=(12, 9))
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        if b_a:
            plt.plot(a[:, odd_cf], a[:, even_cf], 'b')
        if b_a_0:
            plt.plot(a_0[:, odd_cf], a_0[:, even_cf], '.k')
        plt.plot(a_r[:, odd_cf], a_r[:, even_cf], '.r')
        for k in range(1, len(roots)):
            if a_r[k, odd_cf] >= 0:
                h_a = 'left'
            else:
                h_a = 'right'
            if a_r[k, even_cf] >= 0:
                v_a = 'bottom'
            else:
                v_a = 'top'
            plt.text(a_r[k, odd_cf], a_r[k, even_cf], str(k), color='r',
                     fontsize=font_size, ha=h_a, va=v_a)

        y_str = r'$a_{' + str(even_cf) + '}$'
        x_str = r'$a_{' + str(odd_cf) + '}$'
        plt.ylabel(y_str, fontsize=font_size)
        plt.xlabel(x_str, fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.grid()

    plt.tight_layout()

    print('Ploting every equilibria ...')
    print()

    plt.figure(2, figsize=(12, 6))
    for k in range(len(roots)):
        print('Solution num %d = ' % (k), roots[k])
        tck = splrep(x_r, u_r[:, k], s=0)
        u_interp = splev(x_interp, tck, der=0)
        plt.plot(x_interp, u_interp, label='num '+str(k))

    plt.ylabel("u", fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xlabel("x [rad]", fontsize=font_size)
    plt.xticks(np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.]),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$'], fontsize=font_size)
    plt.legend(loc="upper right")
    plt.tight_layout()
    plt.show() """


def sim_search_equilibria(n_eq, t_eq, u0, x, dt, tol, param):
    import numpy as np
    from rfft_norm import rfft_norm
    from nonlin_KS import sim_nonlin_KS
    from simulations import estimated_time
    from time import time

    N = x.size
    a0 = np.imag(rfft_norm(u0, N))
    a0[0] = 0
    t = np.arange(0, n_eq * t_eq + dt, dt)
    k_0 = 0
    K = int(t_eq/dt)
    k_end = K + 1
    a_t_k = np.zeros((t.size, a0.size))
    u = np.zeros((t.size, u0.size))
    roots = []
    a_ini = []
    a_ini.append(a0)
    # Parameters that define the iteration: num of search (n_eq) and time
    # between each one (t_eq). Considering the next strucutre:
    # Search -> Time iteration -> Search -> ... -> Time iteration -> Search
    print('Starting initial root search ...')
    a0, t, roots, tol, param
    roots = search_equilibria(a0, 1, roots, tol, param)  # Time iteration
    # Use the fun KS_method to find the initial values of the search
    t0 = time()
    for n in range(0, n_eq):
        k_0 = K * n
        k_end = K * (n + 1) + 1
        # print(k_0, k_end)
        t_k = t[k_0:k_end]
        # print(t_k)
        # print(t)
        # Use the fun KS_method to find the next initial value
        a_tmp, u_tmp = sim_nonlin_KS(x, t_k, u0, param)
        # print(u_tmp[:, 1])
        # print(u[:, 1])
        a_t_k[k_0:k_end, :] = a_tmp
        u[k_0:k_end, :] = u_tmp
        # print(u[:, 1])
        # plot_KS(a_t_k, u, x, t)
        u0 = u[k_end - 1, :]
        a0 = a_t_k[k_end - 1, :]
        a_ini.append(a0)
        # Use the fun search_KSroot to find the equilibria points
        print('Starting new root search (%d, %.1f%%) ...'
              % (n, n / n_eq * 100))
        t_end = time()
        print(estimated_time(n / n_eq, t_end - t0))
        roots = search_equilibria(a0, t_k, roots, tol, param)

    return roots, a_ini, a_t_k, u


def asymmetric_equilibria(eq):
    eq_asym = eq.copy()
    for m in range(1, len(eq), 2):
        eq_asym[m] *= -1
    return eq_asym


def are_paired_equilibria(eq1, eq2):
    import numpy as np
    tol = 1e-6
    eq1_asym = asymmetric_equilibria(eq1)
    dif = np.square(eq1_asym - eq2).sum()
    if dif < tol:
        return True
    else:
        return False


def is_single_equilibria(eq):
    return are_paired_equilibria(eq, eq)


def paired_equilibria(roots):
    single = []
    paired = []
    missing = []
    k = 0
    while (k < len(roots)):
        eq1 = roots[k]
        if is_single_equilibria(eq1):
            single.append(k)
            k += 1
        else:
            for m in range(k + 1, len(roots)):
                eq2 = roots[m]
                if are_paired_equilibria(eq1, eq2):
                    paired.append([k, m])
                    k += 2
                    break
            else:
                missing.append(k)
                k += 1
    return single, paired, missing


def check_equilibria(roots):
    import numpy as np

    # Checking differences
    print('CHECKING DIFFERENCES BETWEEN EQUILIBRIA')
    print()
    min_val = 9e9
    min_k = 0
    min_m = 0
    for k in range(len(roots) - 1):
        for m in range(k + 1, len(roots)):
            val = np.square(roots[k] - roots[m]).sum()
            print('Difference between root %2d & %2d = %.5g' % (k, m, val))
            if min_val > val:
                min_val = val
                min_k = k
                min_m = m
        print()
    print()

    # Checking asymmetric pairs
    print('FINDING ASYMMETRIC PAIRS')
    print()
    S, P, M = paired_equilibria(roots)
    print('Equilibria with null odd Fourier components:')
    print(S)
    print()
    print('Paired equilibria:')
    print()
    for k in range(len(P)):
        a_idx = P[k][0]
        b_idx = P[k][1]
        print('Root %2d is the asymmetric of root %2d' % (a_idx, b_idx))
        a_root = asymmetric_equilibria(roots[a_idx])
        b_root = roots[b_idx]
        val = np.sqrt(np.square(a_root - b_root).sum())
        print('Difference between root %2d & %2d = %3.5g'
              % (a_idx, b_idx, val))
        print()

    print('SUMMARY')
    print()
    print('Minimum difference (between %d & %d) = %.5g'
          % (min_k, min_m, min_val))
    print()
    if len(M) > 0:
        print('%d missing asymmetric pairs!' % (len(M)))
        print('Missing asymmetric pairs: ', M)
    else:
        print('All asymmetric pairs have been found!')
    print('%d roots: %d paired and %d with null odd components'
          % (len(roots), 2 * len(P), len(S)))
    if len(roots) != 2 * len(P) + len(S) + len(M):
        raise ValueError('Mismatch between list dimensions')


def guess_asym_A(A):
    A_asym = A.copy()
    for k in range(A.shape[0]):
        for p in range(A.shape[1]):
            if k != p and (k + p) % 2 == 1:
                A_asym[k, p] *= -1
    return A_asym


def guess_asym_eigvec(vec):
    v_asym = vec.copy()
    for l in range(1, len(vec), 2):
        v_asym[l] *= -1
    return v_asym


def plot_matrix_values(A):
    import matplotlib.pyplot as plt

    plt.figure(figsize=(12, 12))
    plt.matshow(A, fignum=False, cmap='seismic')
    plt.grid()
    plt.show()


def plot_eigenvec_dif(vec1, vec2, n):
    import numpy as np
    import matplotlib.pyplot as plt
    font_size = 13

    for l in range(0, len(vec1), 2):
        if np.abs(np.real(vec1[l]) * np.real(vec2[l])) > 1e-2:
            break
    if np.real(vec1[l]) * np.real(vec2[l]) < 0:
        vec2 *= -1
    vec2_alt = guess_asym_eigvec(vec2)
    dif = vec1 - vec2_alt

    plt.figure(figsize=(10, 8))
    plt.subplot(2, 1, 1)
    plt.stem(np.real(vec1), linefmt='b', markerfmt='bo', basefmt='w',
             label='vec 1')
    plt.stem(np.real(vec2_alt), linefmt='k', markerfmt='ko', basefmt='w',
             label='vec 2*')
    plt.stem(np.real(dif), linefmt='r', markerfmt='ro', basefmt='w',
             label='diff')
    plt.xlabel('k', fontsize=font_size)
    plt.ylabel(r'Real coeffs, $Re(v_k)$', fontsize=font_size)
    plt.title('Real coefficients, eig %d' % (n + 1), fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.grid()
    plt.legend()

    plt.subplot(2, 1, 2)
    plt.stem(np.imag(vec1), linefmt='b', markerfmt='bo', basefmt='w',
             label='vec 1')
    plt.stem(np.imag(vec2_alt), linefmt='k', markerfmt='ko', basefmt='w',
             label='vec 2*')
    plt.stem(np.imag(dif), linefmt='r', markerfmt='ro', basefmt='w',
             label='diff')
    plt.xlabel('k', fontsize=font_size)
    plt.ylabel(r'Imaginary coeffs, $Im(v_k)$', fontsize=font_size)
    plt.title('Imaginary coefficients, eig %d' % (n + 1), fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xticks(fontsize=font_size)
    plt.grid()
    plt.legend()

    plt.tight_layout()
    plt.show()


def check_paired_equilibria(roots, v):
    from lin_KS import lin_KS, eig_lin_KS
    import numpy as np

    S, P, M = paired_equilibria(roots)
    for k in range(len(P)):
        eq1 = P[k][0]
        eq2 = P[k][1]
        print('PAIRED EQUILIBRIA num %d & %d' % (eq1, eq2))
        print()
        a_eq1 = roots[eq1]
        A1 = lin_KS(a_eq1, v)
        eig1, pos_eig1 = eig_lin_KS(A1, plot=False)
        a_eq2 = roots[eq2]
        A2 = lin_KS(a_eq2, v)
        eig2, pos_eig2 = eig_lin_KS(A2, plot=False)
        print('Unstable directions eigenvalues:')
        for n in range(pos_eig1['n_pos']):
            val1 = pos_eig1['val'][n]
            val2 = pos_eig2['val'][n]
            print('%d. (%.3f, %.3fj) & (%.3f, %.3fj)'
                  % (n + 1, np.real(val1), np.imag(val1),
                     np.real(val2), np.imag(val2)))
        print()
        print('Unstable directions eigenvalues:')
        for n in range(pos_eig1['n_pos']):
            vec1 = pos_eig1['vec'][n]
            vec2 = pos_eig2['vec'][n]
            plot_eigenvec_dif(vec1, vec2, n)
        print()

        """A1_asym = guess_asym_A(A1)
        print('Paired matrices difference:')
        # plot_matrix_values(A1 - A2)
        print('Max value: %.4g' % (np.max(np.abs(A1 - A2))))
        print()
        print('Error of the guessed paired matrix:')
        # plot_matrix_values(A1_asym - A2)
        print('Max value: %.4g' % (np.max(np.abs(A1_asym - A2))))
        print()"""


def plot_steady_states(roots, eq_filt, param, save=False):
    import numpy as np
    import matplotlib.pyplot as plt
    from rfft_norm import irfft_norm, interpolate
    font_size = 13
    N = param['N']

    x = np.linspace(0, 2 * np.pi, param['N'], endpoint=False)
    x_interp = np.linspace(0, np.pi, 480)

    plt.figure(figsize=(10, 5))
    for eq in eq_filt:
        u = irfft_norm(roots[eq] * 1j, N)
#        u_r = u[:r]
#        u_r[-1] = 0
        u_interp = interpolate(x_interp, x, u)
        plt.plot(x_interp, u_interp, label='eq ' + str(eq))
#        plt.plot(x, u, label='eq ' + str(eq))

    plt.ylabel("u", fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xlabel("x [rad]", fontsize=font_size)
    plt.xticks(np.pi / 6. * np.array([0., 1., 2., 3., 4., 5., 6.]),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$'], fontsize=font_size)
#    plt.xlim(0, 1.05 * np.pi)
    plt.legend(loc="upper right", bbox_to_anchor=(1.15, 1))
    plt.grid()
    if save is not False:
        file_name = 'Results/fig_' + save + '_steady_states'
        plt.savefig(file_name, dpi=600)
    plt.show()
    print()


def plot_eigval_traj(roots, eq_filt, param, eq_filt_alpha=[], cf=(1, 3),
                     a=None, save=False):
    import numpy as np
    import matplotlib.pyplot as plt
    from lin_KS import lin_KS, eig_lin_KS
    font_size = 13
    v = param['v']
    r_k = 7e-1
    ax_k = 0.05
    dtxt = 0.03
    n_pos_c = ['w', '#00009bff', '#0062ffff', '#00d4ffff',  # '#54ffa9ff',
               '#f0ff0eff', '#ff5400ff', '#f00000ff']
#    max_ax = 0

    fig = plt.figure(figsize=(5 * cf[1], 5 * cf[0]))
    plt.title('Equilibria', y=1.08)
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        y_str = r'$a_{' + str(even_cf) + '}$'
        x_str = r'$a_{' + str(odd_cf) + '}$'
        plt.ylabel(y_str, fontsize=font_size)
        plt.xlabel(x_str, fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.axis('equal')
        if a is not None:
            plt.plot(a[:, odd_cf], a[:, even_cf], '#A9A9A9')
    for eq in eq_filt:
        a_eq = roots[eq]
        A = lin_KS(a_eq, v)
        eig, pos_eig = eig_lin_KS(A, plot=False)
        n_pos = pos_eig['n_pos']
        sum_pos = np.real(pos_eig['val']).sum()
        r = r_k * sum_pos ** 2
        for p in range(cf[0] * cf[1]):
            plt.subplot(cf[0], cf[1], p + 1)
            odd_cf = 2 * p + 1
            even_cf = 2 * p + 2
            plt.scatter(a_eq[odd_cf], a_eq[even_cf], s=r, c=n_pos_c[n_pos],
                        label='N = ' + str(n_pos))
            # plt.plot(a_eq[odd_cf], a_eq[even_cf], '.r')
            if a_eq[odd_cf] >= 0:
                h_a = 'left'
                dx = dtxt
            else:
                h_a = 'right'
                dx = -dtxt
            if a_eq[even_cf] >= 0:
                v_a = 'bottom'
                dy = dtxt
            else:
                v_a = 'top'
                dy = -dtxt
            plt.text(a_eq[odd_cf] + dx, a_eq[even_cf] + dy, str(eq), color='k',
                     fontsize=font_size, ha=h_a, va=v_a)
#            max_ax = np.max([np.max(np.abs(np.array(plt.axis()))), max_ax])
    for eq in eq_filt_alpha:
        a_eq = roots[eq]
        A = lin_KS(a_eq, v)
        eig, pos_eig = eig_lin_KS(A, plot=False)
        n_pos = pos_eig['n_pos']
        sum_pos = np.real(pos_eig['val']).sum()
        r = r_k * sum_pos ** 2
        for p in range(cf[0] * cf[1]):
            plt.subplot(cf[0], cf[1], p + 1)
            odd_cf = 2 * p + 1
            even_cf = 2 * p + 2
            plt.scatter(a_eq[odd_cf], a_eq[even_cf], s=r, c=n_pos_c[n_pos],
                        label='N = ' + str(n_pos), alpha=0.2)

    artist = []
    label = []
    for c in range(1, len(n_pos_c)):
        artist.append(plt.Line2D((.5, .5), (0, 0), color=n_pos_c[c],
                                 marker='o', linestyle=''))
        label.append('N = ' + str(c))
    fig.legend(artist, label, loc='upper center', ncol=len(n_pos_c[1:]),
               fontsize=font_size, bbox_to_anchor=(0.5, 1.05))

    plt.tight_layout()
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        # plt.grid()
        # plt.axis('equal')
        ax = np.array(plt.axis())
        ax[0] -= ax_k * (ax[1] - ax[0])
        ax[1] += ax_k * (ax[1] - ax[0])
        ax[2] -= ax_k * (ax[3] - ax[2])
        ax[3] += ax_k * (ax[3] - ax[2])
        plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.7)
        plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.7)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
        # ax = [-max_ax, max_ax, -max_ax, max_ax]
        plt.axis(tuple(ax))
    if save is not False:
        file_name = 'Results/fig_' + save + '_eigenvalues'
        plt.savefig(file_name, dpi=600)
    plt.show()
    print()


def plot_circle(x, y, radius, color='k--', linewidth=1.0):
    import numpy as np
    import matplotlib.pyplot as plt
    a = np.linspace(0, 2 * np.pi, num=128)
    X = x + radius * np.cos(a)
    Y = y + radius * np.sin(a)
    plt.plot(X, Y, color, linewidth=linewidth)


def plot_arrow(x, y, a_x, a_y, radius, color='k--'):
    import matplotlib.pyplot as plt
    w = 0.03
    a_x *= radius
    a_y *= radius
    plt.quiver(x, y, a_x, a_y, units='xy', angles='xy', scale=1, color=color,
               width=w, alpha=1)
    plt.quiver(x, y, -a_x, -a_y, units='xy', angles='xy', scale=1, color=color,
               width=w, alpha=1)


def plot_eigvec_traj(roots, eq_filt, param, cf=(1, 3), a=None, save=False):
    import numpy as np
    import matplotlib.pyplot as plt
    from lin_KS import lin_KS, eig_lin_KS, real_eig
    font_size = 13
    v = param['v']
    radius = .5
    ax_k = 0.05
    dtxt = 0.03
#    max_ax = 0

    plt.figure(figsize=(5 * cf[1], 5 * cf[0]))
    plt.title('Equilibria', y=1.08)
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        y_str = r'$a_{' + str(even_cf) + '}$'
        x_str = r'$a_{' + str(odd_cf) + '}$'
        plt.ylabel(y_str, fontsize=font_size)
        plt.xlabel(x_str, fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.axis('equal')
        if a is not None:
            plt.plot(a[:, odd_cf], a[:, even_cf], '#A9A9A9')  # '#00cc00')
    for eq in eq_filt:
        a_eq = roots[eq]
        A = lin_KS(a_eq, v)
        eig, pos_eig = eig_lin_KS(A, plot=False)
        r_val, r_vec = real_eig(pos_eig['val'], pos_eig['vec'])
        for p in range(cf[0] * cf[1]):
            plt.subplot(cf[0], cf[1], p + 1)
            odd_cf = 2 * p + 1
            even_cf = 2 * p + 2
            plot_circle(a_eq[odd_cf], a_eq[even_cf], radius,
                        color='b', linewidth=0.7)
            for m in range(len(r_vec)):
                plot_arrow(a_eq[odd_cf], a_eq[even_cf], r_vec[m][odd_cf],
                           r_vec[m][even_cf], radius, color='b')
            # plt.plot(a_eq[odd_cf], a_eq[even_cf], '.r')
            if a_eq[odd_cf] >= 0:
                h_a = 'left'
                dx = dtxt
            else:
                h_a = 'right'
                dx = -dtxt
            if a_eq[even_cf] >= 0:
                v_a = 'bottom'
                dy = dtxt
            else:
                v_a = 'top'
                dy = -dtxt
            plt.text(a_eq[odd_cf] + dx, a_eq[even_cf] + dy, str(eq), color='k',
                     fontsize=font_size, ha=h_a, va=v_a)
#            max_ax = np.max([np.max(np.abs(np.array(plt.axis()))), max_ax])

    plt.tight_layout()
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        # plt.grid()
        # plt.axis('equal')
        ax = np.array(plt.axis())
        ax[0] -= ax_k * (ax[1] - ax[0])
        ax[1] += ax_k * (ax[1] - ax[0])
        ax[2] -= ax_k * (ax[3] - ax[2])
        ax[3] += ax_k * (ax[3] - ax[2])
        plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.7)
        plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.7)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
        # ax = [-max_ax, max_ax, -max_ax, max_ax]
        plt.axis(tuple(ax))
    if save is not False:
        file_name = 'Results/fig_' + save + '_eigenvectors'
        plt.savefig(file_name, dpi=600)
    plt.show()
    print()


def plot_eq_search(roots, a, a0, cf=(1, 3), save=False):
    import numpy as np
    import matplotlib.pyplot as plt

    font_size = 13
    ax_k = 0.05
#    max_ax = 0

    fig = plt.figure(figsize=(5 * cf[1], 5 * cf[0]))
    plt.title('Equilibria', y=1.08)
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        odd_cf = 2 * p + 1
        even_cf = 2 * p + 2
        y_str = r'$a_{' + str(even_cf) + '}$'
        x_str = r'$a_{' + str(odd_cf) + '}$'
        plt.ylabel(y_str, fontsize=font_size)
        plt.xlabel(x_str, fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.xticks(fontsize=font_size)
        plt.axis('equal')
        plt.plot(a[:, odd_cf], a[:, even_cf], '#A9A9A9')
        for m in range(len(a0)):
            a_ini = a0[m]
            plt.plot(a_ini[odd_cf], a_ini[even_cf], '.', color='#0062ffff')
        for eq in range(len(roots)):
            a_eq = roots[eq]
            plt.plot(a_eq[odd_cf], a_eq[even_cf], 'rx')
#            max_ax = np.max([np.max(np.abs(np.array(plt.axis()))), max_ax])

    artist = []
    label = []
    artist.append(plt.Line2D((.5, .5), (0, 0), color='#A9A9A9', marker='',
                             linestyle='-'))
    label.append('simulation')
    artist.append(plt.Line2D((.5, .5), (0, 0), color='#0062ffff', marker='.',
                             linestyle=''))
    label.append('search start')
    artist.append(plt.Line2D((.5, .5), (0, 0), color='r', marker='x',
                             linestyle=''))
    label.append('equilibria')
    fig.legend(artist, label, loc='upper center', ncol=3,
               fontsize=font_size, bbox_to_anchor=(0.5, 1.05))

    plt.tight_layout()
    for p in range(cf[0] * cf[1]):
        plt.subplot(cf[0], cf[1], p + 1)
        # plt.grid()
        # plt.axis('equal')
        ax = np.array(plt.axis())
        ax[0] -= ax_k * (ax[1] - ax[0])
        ax[1] += ax_k * (ax[1] - ax[0])
        ax[2] -= ax_k * (ax[3] - ax[2])
        ax[3] += ax_k * (ax[3] - ax[2])
        plt.plot([ax[0] * 1.1, ax[1] * 1.1], [0, 0], 'k--', linewidth=0.7)
        plt.plot([0, 0], [ax[2] * 1.1, ax[3] * 1.1], 'k--', linewidth=0.7)
#        plt.plot([-max_ax, max_ax], [0, 0], 'k', linewidth=0.7)
#        plt.plot([0, 0], [-max_ax, max_ax], 'k', linewidth=0.7)
        # ax = [-max_ax, max_ax, -max_ax, max_ax]
        plt.axis(tuple(ax))
    if save is not False:
        file_name = 'Results/fig_' + save + '_search'
        plt.savefig(file_name, dpi=600)
    plt.show()
    print()


def equilibria_selection(roots, only=None):
    import numpy as np
    eq_filt = []
    alpha = []
    S, P, M = paired_equilibria(roots)
    if only is None:
        eq_filt = list(np.arange(len(roots)))
        add_save = 'all'
    elif only == 'single':
        eq_filt = list(S)
        add_save = only
    elif only == 'paired':
        for n in range(len(P)):
            eq_filt.append(P[n][0])
            eq_filt.append(P[n][1])
            add_save = only
    elif only == 'red_paired':
        for n in range(len(P)):
            eq_filt.append(P[n][0])
            alpha.append(P[n][1])
        add_save = only
    elif isinstance(only, list):
        eq_filt = list(only)
        add_save = str(only).replace(' ', '')
    else:
        raise AttributeError('Not valid entry for only input')
    return eq_filt, alpha, add_save


def plot_equilibria(roots, param, only=None, a=None, a0=None, save=False):
    cf = (1, 3)

    eq_filt, alpha, add_save = equilibria_selection(roots, only=only)

    if save is not False:
        save += '_' + add_save

    if (a0 is not None) and (a is not None):
        print('Equilibria search simulation:')
        plot_eq_search(roots, a, a0, cf=cf, save=save)
    print('Steady state solutions:')
    plot_steady_states(roots, eq_filt, param, save=save)
    print('Unstable eigenvalues of the linearised systems:')
    plot_eigval_traj(roots, eq_filt, param, eq_filt_alpha=alpha, cf=cf, a=a,
                     save=save)
    print('Unstable directions of the linearised systems:')
    plot_eigvec_traj(roots, eq_filt, param, cf=cf, a=a, save=save)
    print()


def test_pair_fun():
    import numpy as np
    eq1 = np.array([0, 1, 2])
    eq1_asym = np.array([0, -1, 2])
    eq_single = np.array([0, 0, 4])

    if not is_single_equilibria(eq_single):
        raise ValueError('Single equilibria is not detected')
    if not are_paired_equilibria(eq1, eq1_asym):
        raise ValueError('Paired equilibria are not detected')
