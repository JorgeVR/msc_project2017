# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 23:42:05 2017

@author: Gengiro
"""
import shelve


def exists_simulation(filename):
    from pathlib import Path

    fold = 'Simulations' + '/'
    file = Path(fold + filename + '.dat')
    try:
        __ = file.resolve()
    except FileNotFoundError:
        # print(fold + filename + '.dat')
        return False
    else:
        return True


def is_int(c):
    try:
        int(c)
        return True
    except ValueError:
        return False


def new_name(filename):
    c = filename[-1]
    if not is_int(c):
        return filename + '_1'
    for k in range(2, 5):
        c = filename[-k]
        if c == '_':
            name = filename[:-(k - 1)]
            print(name)
            new_int = int(filename[-(k - 1):]) + 1
            return name + str(new_int)
    else:
        assert(0)


def create_simulation(filename, a, dt, B=None, overwrite=False):
    # Creates a new simulation file
    print('Creating a new simulation file ...')
    fold = 'Simulations' + '/'
    if exists_simulation(filename) and not overwrite:
        filename = new_name(filename)
        print('Filename already exists!')
        print('New filename: ' + filename)
    d = shelve.open(fold + filename, 'n')
    d['a'] = a
    d['dt'] = dt
    if B is None:
        stype = 'OL'
    else:
        stype = 'CL'
        d['B'] = B
    d['stype'] = stype
    d.close()
    print('File created (%s)' % (fold + filename,))
    print()


def load_simulation(filename):
    import numpy as np
    # Loads the simulations from the specific filename
    print('Loading simulation file ...')
    if not exists_simulation(filename):
        error_msg = 'File ' + filename + ' not found, impossible to load'
        raise ValueError(error_msg)

    fold = 'Simulations' + '/'
    d = shelve.open(fold + filename)
    a = d['a']
    dt = d['dt']
    stype = d['stype']
    if stype == 'CL':
        B = d['B']
        print('Closed-loop simulation')
    else:
        print('Open loop simulation')
    d.close()
    print('Simulation loaded from file: ' + filename)
    print()
    t = np.arange(0, len(a[:, 0]) * dt, dt)
    if stype == 'CL':
        return a, t, B
    else:
        return a, t


def estimated_time(percent, time):
    if percent != 0.:
        left_time = time / percent * (1 - percent)
        hours = left_time // 3600
        left_time -= 3600 * hours
        minutes = left_time // 60
        left_time -= minutes * 60
        seconds = left_time
        time_str = ('Estimated left time %dh, %dmin and %.1fs'
                    % (hours, minutes, seconds))
    else:
        time_str = 'Estimated left time not available'
    return time_str


def interpolate_sim(a, u, x, t, new_x=None, new_t=None):
    import numpy as np
    from rfft_norm import interpolate

    if new_t is not None:
        new_a = np.zeros((len(new_t), len(a[0, :])))
        for k in range(len(a[0, :])):
            new_a[:, k] = interpolate(new_t, t, a[:, k])
        new_u = np.zeros((len(new_t), len(x)))
        for idx_x in range(len(x)):
            new_u[:, idx_x] = interpolate(new_t, t, u[:, idx_x])
    else:
        new_a = a.copy()
        new_u = u.copy()
    if new_x is not None:
        newnew_u = np.zeros((len(new_u[:, 0]), len(new_x)))
        for idx_t in range(len(new_u[:, 0])):
            newnew_u[idx_t, :] = interpolate(new_x, x, new_u[idx_t, :])
    else:
        newnew_u = new_u
    return new_a, newnew_u


def compare_sim(a1, a2):
    import numpy as np

    if np.square(a1[0, :] - a2[0, :]).sum() > 1e-10:
        raise ValueError('Simulation files do not start in the same point')

    min_len = np.min([len(a1[:, 0]), len(a2[:, 0])])
    if min_len <= 1:
        return 0

    for idx in range(min_len):
        if np.square(a1[idx, :] - a2[idx, :]).sum() > 1e-10:
            return idx - 1
    else:
        raise Warning(('Not valid index found, both simulations were equal for'
                       ' the range studied.'))
        return 0


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)
