# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 23:09:05 2017

@author: Gengiro
"""

import numpy as np
from equilibria_KS import load_equilibria
from actuators_KS import load_actuators
from lqr import lqr_matrices_KS
from stabilisation_KS import stabilisation_range, plot_stabilisation_range
from stabilisation_KS import create_stabilisation
from simulations import exists_simulation
from time import time


v = (2. * np.pi / 39.) ** 2
N = 96
method = 2
tol = 1e-10
x = np.linspace(0, 2 * np.pi, N, endpoint=False)
search = 'unique'
ctrl_method = 'lqr_test'
rho = 1e-1
r0 = 1e-5
n_act = 2
n_sim = 40
N_sim = 400
# sim_T = 50
t_step = 1e-2
sim_T_r = [2, 8, 24]
# sim_T_r = [10]
stab_method_lst = ['naive', 'search', 'E_dot']
stab_method_lst = ['min_d']

param = {'v': v, 'N': N, 'method': method}
roots_name = ('equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol))))
roots = load_equilibria(roots_name)
actuators_name = ('actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol))) +
                  '_' + search + '_' + ctrl_method + '_act' + str(n_act))
actuators = load_actuators(actuators_name)

# for eq in range(0, len(roots)):
eq_r = [3, 16, 18, 21]
eq_r = [21]
for eq in eq_r:
    for stab_method in stab_method_lst:
        for sim_T in sim_T_r:
            t = np.arange(0, sim_T, t_step)
            stab_name = ('stabilisation_range_N' + str(N) + '_eq' + str(eq) +
                         '_' + stab_method + '_T' + str(sim_T) + '_act' +
                         str(n_act))
            is_ctrl = actuators[eq]['is_controllable']
            exists = exists_simulation(stab_name)
#            if is_ctrl and not exists:
            if is_ctrl:
                a_eq = roots[eq]
                Mu = actuators[eq]['Mu']
                Sigma = actuators[eq]['Sigma']
                Q = np.diagflat(np.ones_like(a_eq))
                R = rho * np.diagflat(np.ones(len(Mu)))
                A, B, F, K, e = lqr_matrices_KS(a_eq, param, x, Mu, Sigma,
                                                Q, R)
                BK = np.dot(B, K)
                t_0 = time()
                R0, RT, a0 = stabilisation_range(a_eq, param, eq, BK, r0, t,
                                                 n_sim=n_sim, N_sim=N_sim,
                                                 method=stab_method)
                t_end = time()
                r_stab, rT_stab = plot_stabilisation_range(R0, RT, eq,
                                                           method=stab_method,
                                                           save=stab_name)
                print('Stabilisation radius = %.4e' % (r_stab))
                print("Total time = %.2f s" % (t_end - t_0))
                print()
                create_stabilisation(stab_name, a0, RT, sim_T, t_step)
