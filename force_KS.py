# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 22:24:01 2017

@author: Gengiro
"""
import numpy as np
import matplotlib.pyplot as plt
from rfft_norm import rfft_norm, irfft_norm, interpolate
from scipy.linalg import solve_lyapunov
from scipy.optimize import curve_fit


def f_KS(x, mu, sigma):
    return np.exp(- (x - mu) ** 2 / sigma)


def create_F_KS(mu_0, sigma_0, n, d):
    m = 3
    Mu = []
    Sigma = []
    if m == 0:
        Mu = [mu_0]
    elif m == 1:
        dist = np.linspace(0, np.pi, num=n)
        for d_m in dist:
            Mu.append(mu_0 + d_m)
    elif m == 2:
        dist = np.linspace(0, 2 * np.pi, num=n)
        for d_m in dist:
            Mu.append(mu_0 + d_m)
    elif m == 3:
        dist = np.linspace(0, (n - 1) * d, num=n)
        for d_m in dist:
            Mu.append(mu_0 + d_m)
            Sigma.append(sigma_0)
    # Mu = np.array(Mu)
    # Sigma = np.ones_like(mu) * sigma_0
    return Mu, Sigma


def F_KS(x, mu, sigma):
    f1 = np.array([f_KS(x_k, mu, sigma) for x_k in x])
    f2 = np.array([f_KS(x_k, mu + 2 * np.pi, sigma) for x_k in x])
    f3 = np.array([-f_KS(x_k, 2 * np.pi - mu, sigma) for x_k in x])
    f4 = np.array([-f_KS(x_k, - mu, sigma) for x_k in x])
    """plt.figure(figsize=(12, 6))
    plt.plot(x, f1, 'b')
    plt.plot(x, f2, 'b--')
    plt.plot(x, f3, 'r')
    plt.plot(x, f4, 'r--')
    plt.plot(x, f1 + f2 + f3 + f4, 'k')
    plt.show()"""
    return f1 + f2 + f3 + f4


def b_KS(f, N):
    return np.array([np.imag(rfft_norm(f, N))])


def B_KS(x, mu, sigma, N):
    # print('N = ', N)
    F = np.zeros((N, len(mu)))
    B = np.zeros((N // 2 + 1, len(mu)))
    for k in range(len(mu)):
        f = F_KS(x, mu[k], sigma[k])

        # a = np.imag(rfft_norm(f.flatten(), N))
        # f = irfft_norm(a * 1j, N)

        F[:, k] = f.flatten()
        b = b_KS(f.flatten(), N)
        B[:, k] = b.flatten()

    return B, F, f


def plot_F_KS(x, F, N):
    font_size = 13
    x_interp = np.linspace(0, 2 * np.pi, num=512, endpoint=False)

    F_fft = np.imag(rfft_norm(F, N))
    F_ifft = irfft_norm(F_fft * 1j, N)

    F_interp = interpolate(x_interp, x, F)
    F_ifft_interp = interpolate(x_interp, x, F_ifft)

    plt.figure(figsize=(8, 3))
    plt.plot([0, 2 * np.pi], [1, 1], 'k--', linewidth=1)
    plt.plot([0, 2 * np.pi], [-1, -1], 'k--', linewidth=1)
    plt.plot([0, 2 * np.pi], [0, 0], 'k', linewidth=1)
    plt.plot(x_interp, F_interp, 'b', label='Original')
    plt.plot(x, F, 'b.')
    plt.plot(x_interp, F_ifft_interp, 'r', alpha=0.7, label='Rebuilded')
    plt.plot(x, F_ifft, 'r.', alpha=0.7)
    plt.xlabel("x [rad]", fontsize=font_size)
    plt.xticks(np.pi / 6. * np.array(range(13)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$', r'$3\pi/2$',
                r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'], fontsize=font_size)
    plt.ylabel('Force')
    plt.yticks(fontsize=font_size)
    plt.legend()
    plt.show


def f_fitting(x, mu, sigma):
    f = F_KS(x, mu, sigma)
    # N = len(x)
    # a = np.imag(rfft_norm(f, N))
    # f = irfft_norm(a * 1j, N)
    return f


def fitting_error(x, U, Mu, Sigma):
    E = U.copy()
    F_fit = np.zeros_like(E)
    for k in range(len(U[0, :])):
        for m in range(len(Mu)):
            mu = Mu[m]
            sigma = Sigma[m]
            mu_idx = np.argmin(np.abs(x - mu))
            f = F_KS(x, mu, sigma).flatten()
            F_fit[:, k] += E[mu_idx, k] / f[mu_idx] * f
            E[:, k] -= E[mu_idx, k] / f[mu_idx] * f
    return E, F_fit


def F_fitting(x, u, F0, B0, N):
    err = u - F0
    l = (len(x) // 2) + 1
    # l = len(x)
    norm = err[np.argmax(np.abs(err[:l]))]
    err /= norm

    mu_idx = np.argmax(err[:l])
    mu_0 = x[mu_idx]
    max_idx = mu_idx
    min_idx = mu_idx
    for idx in range(mu_idx, len(err)):
        cond = np.abs(err[idx]) < 1e-2 or (err[mu_idx] * err[idx]) <= 0
        if cond:
            max_idx = idx
            break
    for idx in range(mu_idx, -1, -1):
        cond = np.abs(err[idx]) < 1e-2 or (err[mu_idx] * err[idx]) <= 0
        if cond:
            min_idx = idx
            break
    max_amp = np.max([x[max_idx] - x[mu_idx], x[mu_idx] - x[min_idx]])
    min_amp = np.min([x[max_idx] - x[mu_idx], x[mu_idx] - x[min_idx]])
    max_sigma = (max_amp ** 2) * 3 / 5
    min_sigma = (min_amp ** 2) / 5
    max_mu = mu_0 + 0.5 * max_amp
    min_mu = mu_0 - 0.5 * min_amp
    bounds = ([min_mu, min_sigma], [max_mu, max_sigma])

    popt, pcov = curve_fit(f_fitting, x, err, bounds=bounds)
    mu = np.array([popt[0]])
    sigma = np.array([popt[1]])
    f = F_KS(x, mu, sigma)
    b, F, f = B_KS(x, mu, sigma, N)
    F = f * norm + F0
    B = np.zeros((B0.shape[0], B0.shape[1] + 1))
    B[:, :-1] = B0
    B[:, -1] = b.flatten()

    print('mu = %.4f /u03c0 (%.4f /u03c0, %.4f /u03c0)'
          % (mu / np.pi, min_mu / np.pi, max_mu / np.pi))
    print('sigma = %.4f /u03c0 (%.4f /u03c0, %.4f /u03c0)'
          % (sigma / np.pi, min_sigma / np.pi, max_sigma / np.pi))
    print('Initial error = %.5g' % (np.sqrt(np.square(u - F0).sum())))
    print('Fitting error = %.5g' % (np.sqrt(np.square(u - F).sum())))
    return B, F, mu, sigma


def test_F():
    mu_0 = 0.2 / 2 * np.pi
    sigma_0 = 2 * np.pi / 100
    sigma_0 = np.sqrt(np.pi / 8)
    # mu = 2 * np.sqrt(sigma)
    n = 1
    d = 4 * np.sqrt(sigma_0)

    N = 64
    # N = 512
    x = np.linspace(0, 2 * np.pi, num=N, endpoint=False)
    mu, sigma = create_F_KS(mu_0, sigma_0, n, d)
    B, F, f = B_KS(x, mu, sigma, N)
    print(B.shape)
    # Q = - np.dot(B, B.T)
    # print(Q.shape)
    plot_F_KS(x, f, N)


def test_lyapunov():

    A = np.array([[-2, 1], [-1, 0]])
    Q = np.array([[1, 0], [0, 1]])
    A = np.array([[1, 2], [-3, -4]])
    Q = np.array([[3, 1], [1, 1]])
    print(A)
    print(Q)
    X = solve_lyapunov(A.T, -Q)
    print(X)
    print(np.linalg.matrix_rank(X))


def test_f_fitting():
    N = 64
    x = np.linspace(0, 2 * np.pi, num=N, endpoint=False)

    mu_0 = 1.9 / 2 * np.pi
    sigma_0 = np.sqrt(np.pi / 32)
    n = 1
    d = 4 * np.sqrt(sigma_0)
    mu, sigma = create_F_KS(mu_0, sigma_0, n, d)

    B, F = B_KS(x, mu, sigma, N)
    popt, pcov = curve_fit(f_fitting, x, F, bounds=(0., np.pi))

    print('Original mu = %.4f, calculated mu %.4f (error = %.3g%%)'
          % (mu_0, popt[0], np.abs(popt[0] - mu_0) * 100 / mu_0))
    print('Original sigma = %.4f, calculated sigma %.4f (error = %.3g%%)'
          % (sigma_0, popt[1], np.abs(popt[1] - sigma_0) * 100 / sigma_0))
    plot_F_KS(x, F - f_fitting(x, popt[0], popt[1]), N)

# test_F()
# test_f_fitting()
