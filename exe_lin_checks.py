# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 17:29:58 2017

@author: Gengiro
"""

from lin_KS import lin_checks
from equilibria_KS import load_equilibria
import numpy as np

v = (2. * np.pi / 39.) ** 2
N = 64
N = 96
method = 2
tol = 1e-10

param = {'v': v, 'N': N, 'method': method}
roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)

lin_checks(roots, list([1, 2, 4, 6, 7]), param)
# lin_checks(roots, [], param)
