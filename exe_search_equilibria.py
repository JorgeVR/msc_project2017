# -*- coding: utf-8 -*-
"""
Solving Kuramoto-Sivashinsky equation: u_t = (u^2)_x - v * u_xxxx - u_xx + f
Created on Sun Apr 23 17:42:56 2017

@author: Gengiro
"""
import numpy as np
from equilibria_KS import sim_search_equilibria, check_equilibria
from equilibria_KS import plot_equilibria, save_equilibria, create_equilibria
from simulations import create_simulation
from time import time

v = (2. * np.pi / 39.) ** 2.
N = 96
method = 2
param = {'v': v, 'N': N, 'method': method}

x = np.linspace(0, 2 * np.pi, N, endpoint=False)
n_eq = 700
t_eq = 0.02
dt = 0.005
tol = 1e-10

filename = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
a0_name = filename + '_a0'
a_name = filename + '_sim'

for k in range(1):
    u0 = np.zeros(x.size)
#    np.random.seed(1)
    np.random.seed(10)
    u0[1:(x.size-1)] = (np.random.random_sample(x.size-2) - 0.5) * 1e-04

    t_0 = time()
    roots, a0, a, u = sim_search_equilibria(n_eq, t_eq, u0, x, dt, tol, param)
    t_end = time()
    print("Total time = %.2f s" % (t_end - t_0))
    check_equilibria(roots)
    roots = save_equilibria(filename, roots)
    plot_equilibria(roots, param, a=a, a0=a0)
    create_equilibria(a0_name, a0)
    create_simulation(a_name, a, dt)
