# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 17:29:58 2017

@author: Gengiro
"""

import numpy as np
from equilibria_KS import load_equilibria
from controllability_KS import place_actuators, plot_actuators
from actuators_KS import load_actuators, save_actuators, check_actuators
from actuators_KS import create_actuators
from time import time


v = (2. * np.pi / 39.) ** 2
N = 96
method = 1
tol = 1e-10
x = np.linspace(0, 2 * np.pi, N, endpoint=False)
n_act = 5
search = 'unique'
ctrl_method = 'lqr_test'

param = {'v': v, 'N': N, 'method': method}
roots_name = 'equilibria_N' + str(N) + '_tol' + str(-int(np.log10(tol)))
roots = load_equilibria(roots_name)
actuators_name = 'actuators_N' + str(N) + '_tol' + str(-int(np.log10(tol))) \
                 + '_' + search + '_' + ctrl_method + '_act' + str(n_act)
create_actuators(actuators_name, len(roots))
actuators = load_actuators(actuators_name)
eq_lst = [3, 4, 16, 17, 18, 21, 22]

# for eq in range(0, len(roots)):
for eq in eq_lst:
    if not actuators[eq]['is_controllable']:
        t_0 = time()
        B, F, act = place_actuators(n_act, roots, [eq], param, search=search,
                                    ctrl=ctrl_method)
        t_end = time()
        print("Total time = %.2f s" % (t_end - t_0))
        if act['is_controllable']:
            plot_actuators(x, F, eq, search=search)
            actuators[eq] = act
            actuators = save_actuators(actuators_name, actuators, roots, x,
                                       param)

check_actuators(actuators, roots, param)
