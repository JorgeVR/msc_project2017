# -*- coding: utf-8 -*-
"""
Created on Fri Jun 23 12:21:46 2017

@author: Gengiro
"""

import numpy as np
import matplotlib.pyplot as plt
from lin_KS import lin_KS, eig_lin_KS, complex2real, rel_error
from force_KS import B_KS, F_KS, create_F_KS, fitting_error, curve_fit
from force_KS import f_fitting
from rfft_norm import irfft_norm, interpolate


def cmplt2red(A, B):
    A_red = A[1:, 1:]
    B_red = B[1:, :]
    return A_red, B_red


def red2cmplt(A_red, B_red):
    A = np.zeros((A_red.shape[0] + 1, A_red.shape[1] + 1))
    A[1:, 1:] = A_red
    B = np.zeros((B_red.shape[0] + 1, B_red.shape[1]))
    B[1:, :] = B_red
    return A, B


def K_red2cmplt(K_red):
    K = np.zeros((K_red.shape[0], K_red.shape[1] + 1))
    K[:, 1:] = K_red
    return K


def Gramian_KS(A, B):
    from scipy.linalg import solve_lyapunov

    Q = - np.dot(B, B.T)
    Wc = solve_lyapunov(A, Q)
    return Wc


def ctrb(A, B):
    # Convert input parameters to matrices (if they aren't already)
    amat = np.mat(A)
    bmat = np.mat(B)
    n = np.shape(amat)[0]
    # Construct the controllability matrix
    Wc = bmat
    for i in range(1, n):
        Wc = np.hstack((Wc, amat**i*bmat))
    return Wc


def controllability(Wc):
    if Wc.shape[0] == Wc.shape[1]:
        det_Wc = np.linalg.det(Wc)
        eigval, eigvec = np.linalg.eig(Wc)
        min_eig = np.min(np.abs(eigval))
    else:
        det_Wc = 0.
        min_eig = 0.
    rank_Wc = np.linalg.matrix_rank(Wc)
    return rank_Wc, det_Wc, min_eig


def plot_F_u(x, u, F):
    font_size = 13
    plt.figure(figsize=(10, 6))
    # plt.plot(x_interp, u_v, 'r', label='Original')
    plt.plot(x, u, 'b.', label='Perturbation', alpha=0.6)
    plt.plot(x, F, 'k', label='Force')
    plt.plot(x, u - F, 'r', label='Error')
    plt.xlabel("x [rad]", fontsize=font_size)
    plt.xticks(np.pi / 6. * np.array(range(13)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$', r'$3\pi/2$',
                r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'], fontsize=font_size)
    plt.ylabel('Force', fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.grid()
    plt.legend()
    plt.show()


def plot_greedy_actuator(x, U, F):
    font_size = 13
    len_v = len(U[0, :])
    plt.figure(figsize=(10, 3 * len_v))
    for k in range(len_v):
        u = U[:, k]
        f = F[:, k]
        plt.subplot(len_v, 1, k + 1)
        plt.plot(x[::6], u[::6], 'b.', label='Perturbation', alpha=0.6)
        plt.plot(x, f, 'k', label='Force')
        plt.plot(x, u - f, 'r', label='Error')
        plt.xlabel("x [rad]", fontsize=font_size)
        plt.xticks(np.pi / 6. * np.array(range(13)),
                   ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                    r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$',
                    r'$3\pi/2$', r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'],
                   fontsize=font_size)
        plt.ylabel('Force', fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.grid()
        plt.legend()
    plt.show()


def plot_actuators(x, F, eq, search):
    font_size = 13
    N_interp = 512
    n_act = len(F[0, :])
    print('Actuators alocation (%d actuators):' % (n_act))

    x_interp = np.linspace(0, 2 * np.pi, N_interp, endpoint=False)

    plt.figure(figsize=(10, 5))
    for p in range(n_act):
        plt.plot(x_interp, interpolate(x_interp, x, F[:, p]))
        # plt.plot(x, F[:, p])

    plt.xlabel("x [rad]", fontsize=font_size)
    plt.axis([0, 2 * np.pi, -1.1, 1.1])
    plt.xticks(np.pi / 6. * np.array(range(13)),
               ['0', r'$\pi/6$', r'$\pi/3$', r'$\pi/2$', r'$2\pi/3$',
                r'$5\pi/6$', r'$\pi$', r'$7\pi/6$', r'$4\pi/3$', r'$3\pi/2$',
                r'$5\pi/3$', r'$11\pi/6$', r'$2\pi$'], fontsize=font_size)
    plt.ylabel('Force', fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.grid()
    file_name = 'Results/fig_actuators_eq' + str(int(eq)) + '_' + search
    plt.savefig(file_name, dpi=600)
    plt.show()


def refine_vec(N_h, vec):
    vec_h = np.zeros((N_h // 2 + 1, ))
    for k in range(len(vec)):
        vec_h[k] = vec[k]
    return vec_h


def ini_uF(N, pos_eig):
    len_v = len(pos_eig['val'])
    vec = pos_eig['vec']
    U = np.zeros((N, len_v))
    for l in range(len_v - 1, -1, -1):
        tmp, v = complex2real(1, vec[l])
        v_ref = refine_vec(N, v)
        U[:, l] = irfft_norm(v_ref * 1j, N)
    return U


def greedy_actuator(x, E):
    max_Error = []
    max_Idx = []
    lim_idx = len(E[:, 0]) // 2

    for k in range(len(E[0, :])):
        max_Error.append(np.max(np.abs(E[:lim_idx, k])))
        max_Idx.append(np.argmax(np.abs(E[:lim_idx, k])))
    max_vec = np.argmax(np.abs(max_Error))
    mu_idx = max_Idx[max_vec]
    max_error = E[mu_idx, max_vec]

    e = E[:, max_vec]
    assert (np.square(e[mu_idx] - max_error) < 1e-12), 'Error not equal'
    e /= max_error
    assert (np.abs(e[mu_idx] - 1) < 1e-12), 'Error not normalised'

    mu_0 = x[mu_idx]
    max_idx = mu_idx
    min_idx = mu_idx
    for idx in range(mu_idx, len(e)):
        cond = np.abs(e[idx]) < 1e-3 or (e[mu_idx] * e[idx]) <= 0
        if cond:
            max_idx = idx
            break
    for idx in range(mu_idx, -1, -1):
        cond = np.abs(e[idx]) < 1e-3 or (e[mu_idx] * e[idx]) <= 0
        if cond:
            min_idx = idx
            break
    max_amp = np.max([x[max_idx] - x[mu_idx], x[mu_idx] - x[min_idx]])
    min_amp = np.min([x[max_idx] - x[mu_idx], x[mu_idx] - x[min_idx]])
    max_sigma = (max_amp ** 2) * 3 / 5
    min_sigma = (min_amp ** 2) / 5
    max_mu = mu_0 + 0.5 * max_amp
    min_mu = mu_0 - 0.5 * min_amp
    bounds = ([min_mu, min_sigma], [max_mu, max_sigma])

    popt, pcov = curve_fit(f_fitting, x, e, bounds=bounds)
    mu = np.array([popt[0]])
    sigma = np.array([popt[1]])
    f = F_KS(x, mu, sigma).flatten()
    e_fit = e - f
    e_0 = np.sqrt(np.square(e).sum())
    e_end = np.sqrt(np.square(e_fit).sum())

    print('mu = %.2f\u03c0 (%.2f\u03c0, %.2f\u03c0)'
          % (mu / np.pi, min_mu / np.pi, max_mu / np.pi))
    print('sigma = %.3f\u03c0 (%.3f\u03c0, %.3f\u03c0)'
          % (sigma / np.pi, min_sigma / np.pi, max_sigma / np.pi))
    print('Error reduction = %.3g%%' % (rel_error(e_0, e_end) * 100))
    print()
    return mu, sigma


def Gramian_controllability(A, B):
    is_ctrl = False
    Wc = Gramian_KS(A, B)
    rank_Wc, det_Wc, min_eig = controllability(Wc)
    full_rank = np.min(Wc.shape)
    print('rank Wc = %d, (full rank = %d)' % (rank_Wc, full_rank))
    print('det Wc = %.3g' % (det_Wc))
    print('min eigenvalue = %.3g' % (min_eig))
    if rank_Wc >= full_rank:
        is_ctrl = True
    return is_ctrl


def ctrb_controllability(A, B):
    is_ctrl = False
    Wc = ctrb(A, B)
    rank_Wc, det_Wc, min_eig = controllability(Wc)
    full_rank = np.min(Wc.shape)
    print('rank Wc = %d, (full rank = %d)' % (rank_Wc, full_rank))
    print('det Wc = %.3g' % (det_Wc))
    print('min eigenvalue = %.3g' % (min_eig))
    if rank_Wc >= full_rank:
        is_ctrl = True
    return is_ctrl


def pos_eig_controllability(A, B):
    is_ctrl = False
    tmp, pos_eig = eig_lin_KS(A, side='left', plot=False)
    pos_val = pos_eig['val']
    if len(pos_eig['val']) == 0:
        is_ctrl = True
        return is_ctrl
    pos_vec = pos_eig['vec']
    e_unstable = []
    for k in range(len(pos_val)):
        real_val, real_vec = complex2real(pos_val[k], pos_vec[k])
        e_unstable.append(real_vec)
    E_unstable = np.array(e_unstable)
    EB = np.dot(E_unstable, B)
    if EB.shape[0] != len(pos_val):
        raise AttributeError('EB matrix vertical length is wrong')
    if EB.shape[1] != B.shape[1]:
        raise AttributeError('EB matrix horizontal length is wrong')
    full_rank = len(pos_val)
    rank_EB = np.linalg.matrix_rank(EB)
    print('Unstable real directions = %d' % (len(pos_val)))
    print('Number of actuators = %d' % (B.shape[1]))
    print('rank EB = %d, (full rank = %d)' % (rank_EB, full_rank))
    if rank_EB == full_rank:
        is_ctrl = True
    return is_ctrl


def lqr_ctrl_test(A, B):
    from lqr import lqr_cont_inf
    Q = np.eye(A.shape[0])
    R = np.eye(B.shape[1])
    try:
        K, e = lqr_cont_inf(A, B, Q, R)
    except np.linalg.LinAlgError:
        is_ctrl = False
    else:
        is_ctrl = True
    return is_ctrl


def controllable(x, Mu, Sigma, N, A, method='ctrb'):
    B, F, f = B_KS(x, Mu, Sigma, N)
    A_red = A[1:, 1:]
    B_red = B[1:, :]
    if method == 'Gramian':
        is_controllable = Gramian_controllability(A_red, B_red)
    elif method == 'ctrb':
        is_controllable = ctrb_controllability(A_red, B_red)
    elif method == 'pos_eig':
        is_controllable = pos_eig_controllability(A_red, B_red)
    elif method == 'lqr_test':
        is_controllable = lqr_ctrl_test(A_red, B_red)
    else:
        raise ValueError('Method not valid, use: Gramian, pos_eig, ctrb or ' +
                         'lqr_test')
    return is_controllable, B, F


def best_actuators(B, F, B_best, F_best, A):
    if B_best == []:
        B_best = B
        F_best = F
        rnk_best = 0
    else:
        Wc = Gramian_KS(A[1:, 1:], B[1:, :])
        rnk, det, val = controllability(Wc)
        Wc_best = Gramian_KS(A[1:, 1:], B_best[1:, :])
        rnk_best, det_best, val_best = controllability(Wc_best)
        if rnk > rnk_best:
            B_best = B
            F_best = F
            rnk_best = rnk
        elif rnk == rnk_best:
            if val < val_best:
                B_best = B
                F_best = F
    print('Current best actuators Wc rank = %d' % (rnk_best))
    return B_best, F_best


def place_actuators(n_act, roots, filt, param, search='naive', ctrl='Gramian'):
    N = param['N']
    N_h = 1024
    x = np.linspace(0, 2 * np.pi, N, endpoint=False)
    x_h = np.linspace(0, 2 * np.pi, N_h, endpoint=False)

    if filt == []:
        r_range = range(len(roots))
    else:
        r_range = filt

    for k in range(len(r_range)):
        print()
        print('CONTROLLABILITY OF THE ROOT %d' % (r_range[k]))
        print()

        a_eq = roots[r_range[k]]
        A = lin_KS(a_eq, param['v'])
        eig, pos_eig = eig_lin_KS(A, side='right', plot=False)
        U = ini_uF(N_h, pos_eig)
        Mu = []
        Sigma = []

        print()
        print('ESTIMATION OF THE ACTUATORS POSITION:')
        if search == 'greedy':
            if pos_eig['n_pos'] > 0:
                for n in range(1, n_act + 1):
                    print()
                    print('Actuator number: %d' % (n))
                    E, F_fit = fitting_error(x_h, U, Mu, Sigma)
                    mu, sigma = greedy_actuator(x_h, E)
                    Mu.append(mu)
                    Sigma.append(sigma)
                    E, F_fit = fitting_error(x_h, U, Mu, Sigma)

                    is_ctrl, B, F = controllable(x, Mu, Sigma, N, A,
                                                 method=ctrl)
                    # plot_greedy_actuator(x_h, U, F_fit)

                    if is_ctrl:
                        print()
                        print('CONTROLLABLE SYSTEM!')
                        print()
                        act = {'is_controllable': True, 'Mu': Mu,
                               'Sigma': Sigma}
                        return B, F, act
            else:
                B = []
                F = []

        elif search == 'naive':
            d_min = np.pi / 30
            sigma_r = [d_min ** 2 / 30, (np.pi / 3) ** 2 / 10]
            sigma = np.linspace(sigma_r[1], sigma_r[0], num=10)
            # n_min = np.max([1, pos_eig['n_pos']])
            n_min = 1
            total_idx = (n_act + 1 - n_min) * len(sigma)
            prog_idx = 0
            B_best = []
            F_best = []
            for n in range(n_min, n_act + 1):
                for sg in sigma:
                    if n > 1:
                        mu_r = [d_min, (np.pi - d_min) / (n - 1), d_min / 5]
                    else:
                        mu_r = [d_min, np.pi - d_min, d_min / 10, d_min / 5]
                    mu = np.arange(mu_r[0], mu_r[1], mu_r[2])
                    for mu0 in mu:
                        if n > 1:
                            dist_r = [d_min, (np.pi - d_min - mu0) / (n - 1),
                                      d_min / 5]
                            dist = np.arange(dist_r[0], dist_r[1], dist_r[2])
                        else:
                            dist = [d_min]
                        for d in dist:
                            Mu, Sigma = create_F_KS(mu0, sg, n, d)
                            print()
                            prog = prog_idx / total_idx * 100
                            print('Equilibria num %d' % (r_range[k]))
                            print('Current progress ... %.2f%%' % (prog))
                            print()
                            print('n = %d (%d, %d)' % (n, n_min, n_act))
                            """
                            print('mu_0 = %.2f\u03c0 (%.2f\u03c0, %.2f\u03c0)'
                                  % (mu0 / np.pi, mu[0] / np.pi,
                                     mu[-1] / np.pi))
                            print('sigma = %.3f\u03c0 (%.3f\u03c0, %.3f\u03c0)'
                                  % (sg / np.pi, sigma[0] / np.pi,
                                     sigma[-1] / np.pi))
                            print('d = %.2f\u03c0 (%.2f\u03c0, %.2f\u03c0)'
                                  % (d / np.pi, dist[0] / np.pi,
                                     dist[-1] / np.pi))
                            print()"""

                            is_ctrl, B, F = controllable(x, Mu, Sigma, N, A,
                                                         method=ctrl)
                            # plot_actuators(x, F)

                            if is_ctrl:
                                print()
                                print('CONTROLLABLE SYSTEM!')
                                print()
                                act = {'is_controllable': True, 'Mu': Mu,
                                       'Sigma': Sigma}
                                return B, F, act
                            else:
                                B, F = best_actuators(B, F, B_best, F_best, A)
                                B_best = B.copy()
                                F_best = F.copy()
                    prog_idx += 1
        elif search == 'unique':
            d = np.pi / (n_act + 1)
            mu0 = d
            sg = [(2 * np.pi / 6) ** 2 / 5]
            Mu, Sigma = create_F_KS(mu0, sg, n_act, d)
            is_ctrl, B, F = controllable(x, Mu, Sigma, N, A, method=ctrl)
            # plot_actuators(x, F)

            if is_ctrl:
                print()
                print('CONTROLLABLE SYSTEM!')
                print()
                act = {'is_controllable': True, 'Mu': Mu, 'Sigma': Sigma}
                return B, F, act
        else:
            raise AttributeError('Method not valid, try: greedy or naive.')
        print()
        print('UNCONTROLLABLE SYSTEM')
        print()
        act = {'is_controllable': False, 'Mu': [], 'Sigma': []}
        return B, F, act


def test_Gramian():
    A = np.array([[-2, 0], [0, -1]])
    B1 = np.array([[2], [1]])
    B2 = np.array([[2], [0]])
    print('System 1 (controllable):')
    Wc1 = Gramian_KS(A, B1)
    r1, d1 = controllability(Wc1)
    print(' rank = %d' % (r1))
    print(' det = %.4g' % (d1))

    print('System 2 (uncontrollable):')
    Wc2 = Gramian_KS(A, B2)
    r2, d2 = controllability(Wc2)
    print(' rank = %d' % (r2))
    print(' det = %.4g' % (d2))

    A = np.array([[-.1, -1], [1, 0]])
    B = np.array([[1], [0]])
    print('A = \n', A)
    print('B = \n', B)
    Wc = Gramian_KS(A, B)
    Wc_ideal = np.array([[5, 0], [0, 5]])
    print('Wc = \n', Wc)
    r, d = controllability(Wc)
    print(' rank = %d' % (r))
    print(' det = %.4g' % (d))
    print('Error matrix = \n', Wc - Wc_ideal)


def test_pos_eig_controllability():
    from lqr import lqr_cont_inf, lsim, plot_lsim, sim_actuators
    eigenvalues = np.array([2, -2, 5, -1])
    E = np.array([[1, 0, 1, 0],
                  [0, 0, 1, 1],
                  [0, 1, 0, 0],
                  [1, 0, 0, 1]])
    A_diag = np.diagflat(eigenvalues)
    A = np.dot(np.dot(np.linalg.inv(E), A_diag), E)
    a = np.dot(E[2, :], A)
    print('A =\n', A)
    print('a =\n', a)
    eig, pos_eig = eig_lin_KS(A, side='left', plot=True)
    B = np.array([[0, 1, -1, 0], [1, 0, 1, 0]]).T
    is_ctrl = pos_eig_controllability(A, B)
    is_ctrl = ctrb_controllability(A, B)
    print('Is controllable: ' + str(is_ctrl))
    Q = np.diagflat(np.ones_like(eigenvalues))
    rho = 1e0
    R = rho * np.diagflat(np.ones(B.shape[1]))
    K, e = lqr_cont_inf(A, B, Q, R)
    print('New eigenvalues: \n', e)
    plt.plot(np.real(e))
    t = np.arange(0, 3, 0.01)
    x0 = np.random.random(eigenvalues.shape)
    x = lsim(x0, t, A - np.dot(B, K))
    u = sim_actuators(x, K)
    plot_lsim(x, t, u=u, only_u=False)
